#IMU Saver

##Overview
C\# Graphical user interface developed for streaming data from multiple IMUs simultaneously.
Data streams may be used for:

- Saving digitally marked data in CSV format
- Streaming inertial or ADC data plots
- Displaying a real-time avatar

##Dependencies
- InTheHand
- OpenTk
- OxyPlot

##Important Classes

###GUI
Inherits from Windows Form.
This is the main GUI exposed to the user. Implements button event triggers to connect to IMU devices,
plot data in real-time, and display a real-time human avatar.

###BluetoothScanner
Utilises the InTheHand library to scan for available bluetooth devices in search mode.
List of devices found are returned to the GUI where they populate a listbox exposed to the user.

###BluetoothConnect
Utilises the InTheHand library to connect to a bluetooth device (selected by the user from the listbox).
Updates the user by sending connection status messages which are displayed in the status bar of the GUI.
On successful connection interacts with classes "PacketDecoder" for decoding data and "Device" to send codes to
the connected IMU device.

###PacketDecoder
Contains methods for decoding ADC and IMU data from the packets as well as methods for combining bytes together.
Notifies GUI each time a packet is decoded so that this information can be exposed to the user.

###DataSaver
Responsible for saving the data stream in a CSV format in real time. Runs on a single thread and deals with multiple
threads (one for each connected IMU device) using the "lock" method. Also includes data in the CSV file
provided by the user such as the trial number, and the side of subject weakness.

###Device
Abstract class containing details of the connected IMU device as well as methods abstract methods for sending
commands to the IMU device which are overrided by classes representing the various IMU device types.

###DeviceManager
Container class for "PacketDecoder", "BluetoothConnect", "Device" for every IMU device connected.

###LivePlotClass
Class used to provide live plot of the streamed data.

###AvatarClass
Class used to provide live avatar using quaternions calculated from the streamed data using "QuaternionCalc"

###QuaternionCalc
Class containing a method which implements the Madgwick gradient descent algorithm for calculating quaternions
