﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NU_GUI_Saver
{
    /// <summary>
    /// A collections of extra necessary functions for the project
    /// </summary>

    public static class Extensions
    {
        /// <summary>
        /// Get the array slice between the two indexes.
        /// ... Inclusive for start index, exclusive for end index.
        /// </summary>
        public static T[] Slice<T>(this T[] source, int start, int end)
        {
            // Handles negative ends.
            if (end < 0)
            {
                end = source.Length + end;
            }
            int len = end - start;

            // Return new array.
            T[] res = new T[len];
            for (int i = 0; i < len; i++)
            {
                res[i] = source[i + start];
            }
            return res;
        }

        /// <summary>
        /// Selects all the child controls of a certain type from a parent control
        /// </summary>
        public static IEnumerable<T> GetChildControls<T>(this Control control) where T : Control
        {
            var children = control.Controls.OfType<T>();
            return children.SelectMany(c => GetChildControls<T>(c)).Concat(children);
        }

        public static string GetTimeStamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
    }
}
