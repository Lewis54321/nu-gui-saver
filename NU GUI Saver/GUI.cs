﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using InTheHand.Net.Sockets;
using InTheHand.Net.Bluetooth;
using NU_GUI_Saver.Classes;
using NU_GUI_Saver.Simulation;

namespace NU_GUI_Saver
{

    /// <summary>
    /// This is the code which runs under the main user GUI interface
    /// </summary>
    public partial class GUI : Form
    {
        #region Variables

        /// <summary>
        /// Bluetooth scanning object used to detect nearby bluetooth devices (called with startScan button)
        /// </summary>
        private BluetoothScanner _bluetoothScan;

        /// <summary>
        /// Refers to the currently selected device out of all which were found in the scan
        /// Required in order to individual connect to a device
        /// List of connected devices returned by the bluetooth scan
        /// </summary>
        BluetoothDeviceInfo _selectedDeviceToConnect;
        private Dictionary<string, BluetoothDeviceInfo> _availableDevices = new Dictionary<string, BluetoothDeviceInfo>();

        /// <summary>
        /// Single dataSaver class object is initialised if user wants to save data
        /// This object is run on a background thread and provides updates of lines saved via an event
        /// </summary>
        private DataSaver _dataSaver;

        /// <summary>
        /// Collection of bluetooth names to expose to the user (updated in the bluetooth connected event)
        /// </summary>
        private List<string> _allBluetoothConnectionsNames = new List<string>();

        /// <summary>
        /// Device Manager object for storing Device, Bluetooth Connection, and Packet Decoder objects
        /// </summary>
        private DeviceManager _deviceManager;
        private readonly Dictionary<string, DeviceManager> _allDeviceManagers = new Dictionary<string, DeviceManager>();

        /// <summary>
        /// Task timer
        /// </summary>
        private TimerClass _timerClass;

        /// <summary>
        /// One second timer to measure the data frequency rate
        /// </summary>
        private TimerClass _dataTimerClass;

        /// <summary>
        /// Test details object
        /// </summary>
        private TestDetails _testDetails;

        /// <summary>
        /// Delegates used to update controls on the GUI thread using invoke
        /// </summary>
        /// <param name="text"></param>
        private delegate void DelUpdateString(string text);
        private delegate void DelUpdateInt(int imuNum);
        private delegate void DelEnableWidget();

        /// <summary>
        /// Counters which are triggered by event handlers called by events in the packetDecoder and DataSaver class objects
        /// </summary>
        private int _packetsDecoded;
        private int _packetsDecodedPerSecond;
        private int _linesSaved;
        private int _lineSavedPerSecond;

        /// <summary>
        /// Field variable required to be stored so it can be changed by event trigger (button press)
        /// </summary>
        private int _taskState;
        
        /// <summary>
        /// Panels of pins in GUI whose visability is changed by an event
        /// </summary>
        private Panel[] _allConfigPanel;
        private Label[] _allConfigLabel;
        private readonly Dictionary<string, ConfigDialog> _allConfigDialog = new Dictionary<string, ConfigDialog>();

        /// <summary>
        /// The clock used for incoming packets (for synchronising up the data gathered from different IMUs)
        /// Shared between all packet decoder class instances
        /// </summary>
        private readonly Clock _packetClock = new Clock();

        /// <summary>
        /// The calibration dialog is a new window created in order to allow the user to set custom calibration values for the IMU
        /// </summary>
        private CalibrationDialog _calibrationDialog;

        /// <summary>
        /// Live Plot class manages plotting of real time data in the Simulation tab
        /// </summary>
        private LivePlotClass _livePlot;

        /// <summary>
        /// Cube class manages visualisation of the quaternion manipulated cube in real space
        /// </summary>
        private Cube3dClass _cube;

        /// <summary>
        /// Avatar class manages visualisation of an avatar of the human upper body
        /// </summary>
        private AvatarClass _avatar;

        #endregion

        #region Constructor

        /// <summary>
        /// Class Constructor
        /// </summary>
        public GUI()
        {
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);

            InitializeComponent();

            InitialisePinLists();

            // Create new Timer Object to calulcate data rate and subscribe to its event
            _dataTimerClass = new TimerClass();
            _dataTimerClass.oneSecondTimer();
            _dataTimerClass.timer.Elapsed += new System.Timers.ElapsedEventHandler(OnDataRateTimedEvent);
            _dataTimerClass.timer.Start();

            var thread = Thread.CurrentThread;
            thread.Name = "MainThread";
        }

        /// <summary>
        /// Creates the list of panels, labels, and comboboxes, which will be displayed
        /// on the GUI when new devices are connected
        /// </summary>
        private void InitialisePinLists()
        {
            _allConfigPanel = new[] { tableLayoutPanelPins1, tableLayoutPanelPins2, tableLayoutPanelPins3, tableLayoutPanelPins4 };
            _allConfigLabel = new[] { IMU_1_LABEL, IMU_2_LABEL, IMU_3_LABEL, IMU_4_LABEL };
        }

        #endregion

        #region Bluetooth Connection Management

        /// <summary>
        /// Start scan method triggered by button pressed
        /// Runs single bluetooth scan object on a separate thread and returns available devices using BluetoothScanStatus event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bluetoothScanButton_Click(object sender, EventArgs e)
        {
            // create new bluetooth connection instance
            _bluetoothScan = new BluetoothScanner();

            // Disable the bluetooth scan button
            bluetoothScanButton.Enabled = false;

            // running scan on a seperate background thread to keep the UI responsive
            backgroundWorkerScan.RunWorkerAsync();
        }

        /// <summary>
        /// Background worker event handlers which are called on thread running and ending respectively
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorkerScan_DoWork(object sender, DoWorkEventArgs e)
        {
            DelUpdateString delStringStatus = UpdateStatusText;
            this.statusStrip.BeginInvoke(delStringStatus, "Searching for bluetooth devices...");
            _bluetoothScan.bluetoothSearch();
        }

        /// <summary>
        /// Background worker completed event handler
        /// Returns a list of available devices which are displayed to the user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorkerScan_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripStatusLabel.Text = "Search complete, showing available bluetooth devices";
            bluetoothScanButton.Enabled = true;
            _availableDevices = _bluetoothScan.BluetoothDevices;
            Available_Devices_ListBox.DataSource = _availableDevices.Select(d => d.Value.DeviceName).ToList();
        }

        /// <summary>
        /// Attempt to connect to the device that the user has selected (out of list returned by scan)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bluetoothConnectButton_Click(object sender, EventArgs e)
        {
            DelUpdateString delStringStatus = UpdateStatusText;
            try
            {
                _selectedDeviceToConnect = _availableDevices[Available_Devices_ListBox.SelectedItem.ToString()];
            }
            catch
            {
                this.statusStrip.BeginInvoke(delStringStatus, "Please select a device to connect!");
            }
            if (_selectedDeviceToConnect != null)
            {
                if (DevicePaired())
                {
                    this.statusStrip.BeginInvoke(delStringStatus, "Connecting...");
                    BluetoothStartConnect(_selectedDeviceToConnect);
                }
                else
                {
                    this.statusStrip.BeginInvoke(delStringStatus, "Cannot connect. The device is not paired.");
                }
            }
        }

        /// <summary>
        /// Creates new device, packet decoder, bluetooth connection per device connected
        /// Subscribes to all necessary events
        /// Device object created and passed to BluetoothConnect object to provide methods and for collecting device data (firmware, frequency)
        /// </summary>
        /// <param name="selectedDeviceToConnect"></param>
        private void BluetoothStartConnect(BluetoothDeviceInfo selectedDeviceToConnect)
        {
            // device object made local to avoid risk of being overwritten by subsequent connection
            var device = new DeviceNUIMU(selectedDeviceToConnect, (_allDeviceManagers.Count()));
            var packetDecoder = new PacketDecoder(_packetClock);
            var bluetoothConnection = new BluetoothConnect(device);            

            // Subscribe packet decoder to bluetooth events
            bluetoothConnection.PacketSent += packetDecoder.OnPacketSent;

            // Subscribe the UI to bluetooth events
            bluetoothConnection.BluetoothConnectionStatusChanged += this.OnBluetoothConnectionStatusChanged;
            bluetoothConnection.BluetoothConnectionCompleted += this.OnBluetoothConnectionCompleted;
            bluetoothConnection.BtDisconnected += this.OnBtDisconnected;
            packetDecoder.PacketDecoded += this.OnPacketDecoded;            

            // Run bluetooth connection on seperate thread to keep the UI responsive
            Thread bluetoothConnectThread = new Thread(new ThreadStart(bluetoothConnection.Connect));
            bluetoothConnectThread.Name = "Bluetooth Connection";
            bluetoothConnectThread.Start();

            // Create a new DeviceManager object which will be the only object with class-wide scope to avoid overwritting
            _deviceManager = new DeviceManager(device, packetDecoder, bluetoothConnection);

            // Disable the bluetooth connect button to prevent repeated presses
            this.bluetoothConnectButton.Enabled = false;
        }

        /// <summary>
        /// Method called to find if device is paired, and if not pairs it with code "1234"
        /// </summary>
        /// <returns></returns>
        private bool DevicePaired()
        {
            DelUpdateString del_string_status = UpdateStatusText;
            this.statusStrip.BeginInvoke(del_string_status, "Attempting to Pair...");

            if (!_selectedDeviceToConnect.Authenticated)
            {
                if (!BluetoothSecurity.PairRequest(_selectedDeviceToConnect.DeviceAddress, "1234"))    //Initiates a pair request.
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// User controlled buttom to disconnect IMU device
        /// Set ConnectFlag to false in BluetoothConnect to avoid issue with Disconnect method getting called twice
        /// (once in this method and again in the BluetoothConnect class as a result of the stream not being able to be read)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDisconnectIMUs_Click(object sender, EventArgs e)
        {
            DisconnectImus();
        }

        /// <summary>
        /// Manual method for disconnecting IMUs
        /// Called either to manually connect IMUs or automatically when setting the registers
        /// </summary>
        private void DisconnectImus()
        {
            foreach (DeviceManager DM in _allDeviceManagers.Values)
            {
                DM.BluetoothConnect.Disconnect();
            }
            _allDeviceManagers.Clear();
            _allBluetoothConnectionsNames.Clear();
            Connected_Devices_ListBox.DataSource = null;
        }

        /// <summary>
        /// Event handler for Bluetooth connection class object to return info if connection status changes
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnBluetoothConnectionStatusChanged(object source, UpdatedStatusEventArgs e)
        {
            // Update the Status Strip and re-enable the connect button
            DelUpdateString delStringStatus = UpdateStatusText;
            DelEnableWidget delEnableButton = EnableConnectButton;
            try
            {
                this.statusStrip.BeginInvoke(delStringStatus, e.Status);
                this.bluetoothConnectButton.BeginInvoke(delEnableButton);
            }
            catch (Exception)
            {
                return;
            }
        }

        /// <summary>
        /// Event handler for successful bluetooth connection
        /// This may be triggered by a manual connection attempt by the user or by an automatic reconnection attempy
        /// If triggered by the user then the last created _deviceManager instance will be added to the dict
        /// If triggered by an automatic reconnection then a _deviceManager instance will already exist (although not necessarily be the last created)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnBluetoothConnectionCompleted(object source, BtConnectionEventArgs e)
        {
            // Update the current device mananger by providing the device details
            // These details should have been provided by the "BluetoothConnect" class when it reads the device registers
            _deviceManager.UpdateDeviceDetails();

            // For successful (manual) bluetooth connection then add device manager to dict
            if (!_allDeviceManagers.ContainsKey(e.DeviceName))
            {
                _allDeviceManagers.Add(e.DeviceName, _deviceManager);
            }
            _allBluetoothConnectionsNames = _allDeviceManagers.Values.Select(d => d.DeviceName).ToList();            

            // Update list of connected devices and changes visibility of ADC pin selection
            DelUpdateInt delListConnectedDevices = UpdateConnectedDeviceArray;
            this.Connected_Devices_ListBox.BeginInvoke(delListConnectedDevices, e.UniqueNumber);
        }

        /// <summary>
        /// Event handler for when a bluetooth device is connected (called from BluetoothConnect instance)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnBtDisconnected(object source, BtConnectionEventArgs e)
        {
            // Update list of connected devices and changes visibility of ADC pin selection
            _allBluetoothConnectionsNames.Remove(e.DeviceName);

            // Update list of connected devices
            DelUpdateInt delListConnectedDevices = RemoveConnectedDeviceArray;
            this.Connected_Devices_ListBox.BeginInvoke(delListConnectedDevices, e.UniqueNumber);

            // Stop recording of task data while IMU is disconnected (delegate invoke executes synchronously on same thread)
            // Could change this code to make it more obvious that it is invoking from a delegate
            if (_dataSaver != null)
            {
                this.Invoke(new Action(() => SelectTaskState(false)));
            }
        }

        /// <summary>
        /// Event handler to deal with event from Packet decoder class object (managed by GUI)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnPacketDecoded(object source, PacketDecodedEventArgs e)
        {
            _packetsDecoded++;
            _packetsDecodedPerSecond++;

            if (_packetsDecoded % 1000 == 0)
            {
                DelUpdateString delStringPackets = UpdatePacketsDecodedCount;
                this.decodedPacketsLabel.BeginInvoke(delStringPackets, _packetsDecoded.ToString());
            }
        }

        /// <summary>
        /// Button press event handler to deal with setting the calibration values of the IMU
        /// Creates a new dialog window to enable the user to input custom calibration values which are to be uploaded
        /// Subscribe the button event when the user selects the values so these can be passed back to the main form to be uploaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void setCalibrationButton_Click(object sender, EventArgs e)
        {
            if (Connected_Devices_ListBox.SelectedItem == null)
            {
                return;
            }

            _calibrationDialog = new CalibrationDialog(_deviceManager.Device.InertialCal, _deviceManager.Device.InertialAdv, _deviceManager.Device.SampleSet);
            _calibrationDialog.Show();
            _calibrationDialog.CalValues += this.OnCalValues;
        }

        /// <summary>
        /// Event handler triggered once new user set calibration values 
        /// Handles pausing data from being streamed on the bluetooth thread, writes the registers, then disconnects all connected IMUs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCalValues(object sender, EventArgs e)
        {
            Dictionary<string, int[]> calDict = _calibrationDialog.RegisterValues;
            _calibrationDialog.Hide();
            _deviceManager.BluetoothConnect.PauseStream();
            _deviceManager.BluetoothConnect.ClearStream();
            _deviceManager.BluetoothConnect.WriteRegisters(calDict);
            DisconnectImus();
            const string message = "Register Values Updated. Please Reset Device...";
            toolStripStatusLabel.Text = message;
        }

        #endregion

        #region Save Management

        /// <summary>
        /// User button click event to begin saving data to CSV
        /// If not ready to save then return
        /// Otherwise extract the user set test information from the GUI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveDataButton_Click(object sender, EventArgs e)
        {
            if (!ReadyToSave())
            {
                return;
            }

            // Disables the connect button to prevent user from connecting more IMUs midway through saving
            this.bluetoothConnectButton.Enabled = false;

            // Creates testDetails with the filepath defined, trial type, and the trial number
            _testDetails = new TestDetails(filePathTextBox.Text, trialTypeDropBox.Text, trialNoTextBox.Text, 
                                           comboBoxInstrumSide.Text, comboBoxSittingIncl.Text);

            // Update the task combobox depending on what taskset the user has selected
            taskComboBox.Items.Clear();
            taskComboBox.Items.AddRange(_testDetails.taskList.ToArray());
            _testDetails.TasksToString("Task List");

            // Create a new SaveFileDetails class object and DataSaver class object
            SaveFileDetails saveFileDetails = new SaveFileDetails(_testDetails, _allDeviceManagers);
            _dataSaver = new DataSaver(_deviceManager.BluetoothConnect, saveFileDetails);

            // Subscribe UI to DataSaver LineSaved Event
            _dataSaver.LineSaved += this.OnLineSaved;

            // Reset the packet clock so that data is saved with clock starting at zero
            _packetClock.ResetClock();

            // Subscribe DataSaver instance to packetdecoded events
            // Subscribe PacketDecoder instance to GUI taskChanged event
            foreach (DeviceManager DM in _allDeviceManagers.Values)
            {
                DM.PacketDecoder.PacketDecoded += _dataSaver.OnPacketDecoded;
                this.TaskChanged += DM.PacketDecoder.OnTaskChanged;
            }

            // Subscribe UI to changes in data saver state
            _dataSaver.DataSaverStateChange += this.OnDataSaverStateChange;

            // Create new Timer Object to display task time and Subscribe to its event
            _timerClass = new TimerClass();
            _timerClass.tenthSecondTimer();
            _timerClass.timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);            

            // Run Data Saver on backgroundworker thread to keep UI responsive
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += _dataSaver.worker_DoWork;
            worker.RunWorkerAsync();

            this.saveDataButton.Enabled = false;
        }

        /// <summary>
        /// Method to check that the user has completed all the prerequisites before saving
        /// </summary>
        /// <returns></returns>
        private bool ReadyToSave()
        {
            DelUpdateString delStringStatus = UpdateStatusText;

            if (!_allDeviceManagers.Any())
            {
                this.statusStrip.BeginInvoke(delStringStatus, "Please connect to at least one IMU");
                return false;
            }
            if (filePathTextBox.Text == "" | trialTypeDropBox.Text == "" | trialNoTextBox.Text == "")
            {
                this.statusStrip.BeginInvoke(delStringStatus, "Please set the filepath, test type, and trial number");
                return false;
            }
            if (comboBoxInstrumSide.Text == "" | comboBoxSittingIncl.Text == "")
            {
                this.statusStrip.BeginInvoke(delStringStatus, "Please select the side the instrumentation is attached and sitting inclination");
                return false;
            }
            if (_allDeviceManagers.Values.Select(c => !c.ConfigUpdated).Any(x => x))
            {
                this.statusStrip.BeginInvoke(delStringStatus, "Please set the configuration settings for each connected IMU");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method to stop saving data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stopSaveButton_Click(object sender, EventArgs e)
        {
            if (ReadyToSave() & _dataSaver != null)
            {
                this.bluetoothConnectButton.Enabled = true;
                _dataSaver.SaveDataFlag = false;
                _timerClass.timer.Stop();
                textBoxTaskTimer.Text = "0";
                _timerClass.timeSeconds = 0;
                this.saveDataButton.Enabled = true;
            }
        }

        /// <summary>
        /// Event handler to deal with event from Data Saver class object (managed by GUI)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnLineSaved(object source, EventArgs e)
        {
            _linesSaved++;
            _lineSavedPerSecond++;

            if (_linesSaved % 1000 == 0)
            {
                DelUpdateString delStringPackets = UpdateLinesSavedCount;
                this.LineSavedLabel.BeginInvoke(delStringPackets, _linesSaved.ToString());
            }
        }

        /// <summary>
        /// Event handler to deal with event from Data Saver Class object (managed by GUI)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnDataSaverStateChange(object source, UpdatedStatusEventArgs e)
        {
            DelUpdateString delStringStatus = UpdateStatusText;
            this.statusStrip.BeginInvoke(delStringStatus, e.Status);
        }

        #endregion

        #region GUI Control Event Handlers

        /// <summary>
        /// Event handlers for the "buttonSetPins" button presses to show a list of pins whose value may be set for each connected IMU
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonConfig1_Click(object sender, EventArgs e)
        {
            Config(0);
        }
        private void buttonConfig2_Click(object sender, EventArgs e)
        {
            Config(1);
        }
        private void buttonConfig3_Click(object sender, EventArgs e)
        {
            Config(2);
        }
        private void buttonConfig4_Click(object sender, EventArgs e)
        {
            Config(3); ;
        }
        private void Config(int imuNum)
        {
            // Create a new PinSetDialog form instance with which to all the user to update pin and orientation info for the newly connected IMU
            string imuName = _allBluetoothConnectionsNames[imuNum];            
            if (_allConfigDialog.ContainsKey(imuName))
            {
                _allConfigDialog.Remove(imuName);                
            }
            ConfigDialog configDialog = new ConfigDialog(imuNum, _allDeviceManagers[imuName].Device.PinsActive);
            configDialog.ConfigSet += this.OnConfigSet;
            _allConfigDialog.Add(imuName, configDialog);
            configDialog.Show();
        }

        /// <summary>
        /// Event handler for ConfigSet event originating in the CalibrationDialog
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnConfigSet(object source, ConfigEventArgs e)
        {
            string imuName = _allBluetoothConnectionsNames[e.ImuNum];
            var message = "Updated values for IMU Device: " + imuName;
            toolStripStatusLabel.Text = message;

            _allDeviceManagers[imuName].Device.UpdateConfigSettings(e.PinList, e.Location, e.Orientation);
            _allDeviceManagers[imuName].ConfigUpdated = true;
            _allConfigLabel[e.ImuNum].Font = new Font(_allConfigLabel[e.ImuNum].Font, FontStyle.Bold);
            _allConfigDialog[imuName].Close();
        }

        /// <summary>
        /// Method triggered by event when task start/stop button is clicked (stop button - taskState 0, start button - taskState 1)
        /// Only updates taskNumber (index) every time button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void taskStateButton_Click(object sender, EventArgs e)
        {
            string[] taskStateText = { "Start of Task", "End of Task" };
            object[] backColours = { System.Drawing.Color.Green, System.Drawing.Color.Red };

            // Checks if a task has been selected (index not equal to -1) and that data is currently being saved
            if (taskComboBox.SelectedIndex != -1 && _dataSaver != null)
            {
                int taskNumber = taskComboBox.SelectedIndex + 1;
                _taskState = 1 - _taskState;
                taskStateButton.Text = taskStateText[_taskState];
                taskStateButton.BackColor = (System.Drawing.Color)backColours[_taskState];

                // If stop button pressed increment task selected by one and stop timer (unless have already incremented through all tasks already)
                if (_taskState == 0 & taskComboBox.SelectedIndex < (taskComboBox.Items.Count - 1))
                {
                    taskComboBox.SelectedIndex += 1;
                    _timerClass.timer.Stop();
                }
                else if (_taskState == 0 & taskComboBox.SelectedIndex <= (taskComboBox.Items.Count - 1))
                {
                    _timerClass.timer.Stop();
                }
                else if (_taskState == 1 & taskComboBox.SelectedIndex <= (taskComboBox.Items.Count - 1))
                {
                    _timerClass.timer.Start();
                }

                // Event triggered to send taskState to the packetDecoder class
                OnTaskChanged(new TaskIndexEventArgs(taskNumber, _taskState));
            }
        }

        /// <summary>
        /// Method for changing the task button state
        /// Called when device is disconnected
        /// </summary>
        /// <param name="recordTask"></param>
        private void SelectTaskState(bool recordTask)
        {
            int taskNumber = taskComboBox.SelectedIndex + 1;
            if (recordTask)
            {
                taskStateButton.Text = "End of Task";
                taskStateButton.BackColor = System.Drawing.Color.Red;
                _timerClass.timer.Start();
                _taskState = 1;
            }
            else
            {
                taskStateButton.Text = "Start of Task";
                taskStateButton.BackColor = System.Drawing.Color.Green;
                _timerClass.timer.Stop();
                _taskState = 0;
            }
            OnTaskChanged(new TaskIndexEventArgs(taskNumber, _taskState));
        }

        /// <summary>
        /// Event handler called when Timer Class class object reaches 0.1 seconds
        /// Used to display a clock so the user can know how long a task has taken
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            _timerClass.timeSeconds = _timerClass.timeSeconds + 0.1;
            string timerString = string.Format("{0:0.0}", _timerClass.timeSeconds);
            DelUpdateString delStringTimer = UpdateTimerText;
            this.textBoxTaskTimer.BeginInvoke(delStringTimer, timerString);
        }

        /// <summary>
        /// Event handler called by Timer class object (called once per second and checks value of "_lineSavedPerSecond"
        /// Used for displaying the data save rate output
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnDataRateTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            DelUpdateString delStringPacketRate = UpdateTimerPacketRateText;
            DelUpdateString delStringSaveRate = UpdateTimerSaveRateText;            
            this.packetDecodeRateLabel.BeginInvoke(delStringPacketRate, _packetsDecodedPerSecond.ToString() + " Hz");
            this.SaveDataRateLabel.BeginInvoke(delStringSaveRate, _lineSavedPerSecond.ToString() + " Hz");
            _packetsDecodedPerSecond = 0;
            _lineSavedPerSecond = 0;
        }

        /// <summary>
        /// Event which is called to trigger method in PacketDecoder (managed in GUI)
        /// Used to set the first two elements of the decoded packet (to identify the task being recorded)
        /// </summary>
        public event EventHandler<TaskIndexEventArgs> TaskChanged;
        public virtual void OnTaskChanged(TaskIndexEventArgs e)
        {
            TaskChanged?.Invoke(this, e);
        }

        private void GUI_Resize(object sender, EventArgs e)
        {
            foreach (Control control in this.Controls)
            {

            }
        }

        #endregion

        #region GUI Control Delegates

        /// <summary>
        /// Method for adding names to the list of connected devices displayed to the user
        /// </summary>
        /// <param name="imuNum"></param>
        private void UpdateConnectedDeviceArray(int imuNum)
        {
            Connected_Devices_ListBox.DataSource = _allBluetoothConnectionsNames;
            _allConfigPanel[imuNum].Visible = true;
            _allConfigLabel[imuNum].Text = _allBluetoothConnectionsNames[imuNum];
        }

        /// <summary>
        /// Method for removing names from the list of connected devices displayed to the user
        /// For some reason have to reset the datasource to null before then changing the datasource to reduced array of names
        /// </summary>
        /// <param name="imuNum"></param>
        private void RemoveConnectedDeviceArray(int imuNum)
        {
            Connected_Devices_ListBox.DataSource = null;
            Connected_Devices_ListBox.DataSource = _allBluetoothConnectionsNames;
        }

        private void UpdateStatusText(string status)
        {
            toolStripStatusLabel.Text = status;
        }

        private void EnableConnectButton()
        {
            this.bluetoothConnectButton.Enabled = true;
        }

        private void UpdatePacketsDecodedCount(string count)
        {
            decodedPacketsLabel.Text = count;
        }

        private void UpdateLinesSavedCount(string count)
        {
            LineSavedLabel.Text = count;
        }

        private void UpdateTimerText(string count)
        {
            textBoxTaskTimer.Text = count;
        }

        private void UpdateTimerSaveRateText(string count)
        {
            SaveDataRateLabel.Text = count;
        }
        private void UpdateTimerPacketRateText(string count)
        {
            packetDecodeRateLabel.Text = count;
        }

        #endregion

        #region Simulation

        /// <summary>
        /// Button press event used create new plot class object which gives real time view of data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void adcSimButton_Click(object sender, EventArgs e)
        {
            GenerateLivePlot("ADC");
        }

        private void accSimButton_Click(object sender, EventArgs e)
        {
            GenerateLivePlot("Acc");
        }

        private void gyrSimButton_Click(object sender, EventArgs e)
        {
            GenerateLivePlot("Gyr");
        }

        private void magSimButton_Click(object sender, EventArgs e)
        {
            GenerateLivePlot("Mag");
        }

        private void GenerateLivePlot(string sensorModality)
        {
            if (Connected_Devices_ListBox.SelectedItem == null)
            {
                return;
            }
            StopSimulation();

            _livePlot = new LivePlotClass(this.paintPanel, sensorModality);

            // Subscribe the packet decoded event to the live plot so that new packets will be displayed
            _deviceManager.PacketDecoder.PacketDecoded += _livePlot.OnPacketDecoded;

            // Create new thread to run the plot on
            Thread dataPlotThread = new Thread(_livePlot.GenPlot);
            dataPlotThread.Name = sensorModality + "_PlotThread";
            dataPlotThread.Start();
        }

        /// <summary>
        /// Button press event used to create a new cube simulation object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cubeSimButton_Click(object sender, EventArgs e)
        {
            if (Connected_Devices_ListBox.SelectedItem == null)
            {
                return;
            }
            StopSimulation();

            // Set visibility of the table object
            tableLayoutPanelRot.Visible = true;

            //Create a Quaternion Update Object for updating the cube orientation
            QuaternionCalc quaternionCalc = new QuaternionCalc(_deviceManager.Device.InertialCal, _deviceManager.Device.ImuFreq);

            //Create the new Cube 3D object passing it the quaternion calculation method and the control to paint on
            Dictionary<string, Button> buttonDict = new Dictionary<string, Button>()
            {
                {"rotLeft", buttonRotLeft}, {"rotRight", buttonRotRight}, {"tare", buttonTare}
            };
            _cube = new Cube3dClass(quaternionCalc, paintPanel, buttonDict, _deviceManager.Device.InertialCal);

            //Subscribe the packet decoder event to the cube to update its orientation
            _deviceManager.PacketDecoder.PacketDecoded += _cube.OnPacketDecoded;
        }

        /// <summary>
        /// Button press event used to create a new avatar simulation object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void avatarSimButton_Click(object sender, EventArgs e)
        {
            if (_allDeviceManagers.Count == 0)
            {
                return;
            }
            if (_allDeviceManagers.Values.Select(c => !c.ConfigUpdated).Any(x => x))
            {
                toolStripStatusLabel.Text = "Please updated the configuration settings for each connected device";
                return;
            }

            if (comboBoxInstrumSide.Text == "")
            {
                toolStripStatusLabel.Text = "Please updated the subject side tested";
                return;
            }

            StopSimulation();

            // Set visibility of the table object and the glControl
            tableLayoutPanelRot.Visible = true;
            tableLayoutPanelAngle.Visible = true;

            // Create a list of devices to cycle through and a dict of quaternion calculate classes to populate
            List<Device> deviceList = _allDeviceManagers.Values.Select(d => d.Device).ToList();
            Dictionary<string, QuaternionCalc> allQuaternionCalc = new Dictionary<string, QuaternionCalc>();
            Dictionary<string, Dictionary<string, short>> allInertialCal = new Dictionary<string, Dictionary<string, short>>();
            Dictionary<string, string> allOrientation = new Dictionary<string, string>();

            foreach (Device device in deviceList)
            {
                QuaternionCalc quaternionCalc = new QuaternionCalc(device.InertialCal, device.ImuFreq);
                allQuaternionCalc.Add(device.Location, quaternionCalc);
                allOrientation.Add(device.Location, device.Orientation);
                allInertialCal.Add(device.Location, device.InertialCal);
            }

            Dictionary<string, Button> buttonDict = new Dictionary<string, Button>()
            {
                {"buttonRotLeft", buttonRotLeft },
                {"buttonRotRight", buttonRotRight },
                {"tare", buttonTare}
            };
            Dictionary<string, Label> labelDict = new Dictionary<string, Label>()
            {
                {"labelShoulderMag", labelShoulderMag},
                {"labelShoulderTwist", labelShoulderTwist},
                {"labelShoulderX", labelShoulderX},
                {"labelShoulderY", labelShoulderY },
                {"labelShoulderZ", labelShoulderZ },
                {"labelElbowMag", labelElbowMag },
                {"labelElbowTwist", labelElbowTwist},
                {"labelElbowX", labelElbowX },
                {"labelElbowY", labelElbowY },
                {"labelElbowZ", labelElbowZ }
            };

            string bodySide = comboBoxInstrumSide.Text;
            _avatar = new AvatarClass(allQuaternionCalc, allInertialCal, bodySide, allOrientation, paintPanel, buttonDict, labelDict);

            foreach (DeviceManager deviceManager in _allDeviceManagers.Values)
            {
                deviceManager.PacketDecoder.PacketDecoded += _avatar.OnPacketDecoded;
            }
        }

        private void stopVisButton_Click(object sender, EventArgs e)
        {
            StopSimulation();
        }

        private void StopSimulation()
        {
            if (_livePlot != null)
            {
                _livePlot.Streaming = false;
                _livePlot.LinePlot.Visible = false;
            }
            if (_cube != null)
            {
                _cube.FormUpdateTimer.Stop();
                _cube.GlControl.Dispose();
                _cube = null;
                tableLayoutPanelRot.Visible = false;
            }
            if (_avatar != null)
            {
                _avatar.FormUpdateTimer.Stop();
                _avatar.GlControl.Dispose();
                _avatar = null;
                tableLayoutPanelRot.Visible = false;
                tableLayoutPanelAngle.Visible = false;
            }
        }

        #endregion
    }
}
