﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace NU_GUI_Saver.Simulation
{
    class AvatarClass
    {
        #region Fields

        /// <summary>
        /// The GlControl passed from the GUI for painting the scene on
        /// </summary>
        public GLControl GlControl { get; }

        /// <summary>
        /// Dict of quaternions calculated for up to three simulateously connected devices
        /// </summary>
        private readonly Dictionary<string, QuaternionCalc> _allQuaternionCalc;

        /// <summary>
        /// Dict of dict with each dict containing calibration values for each metric axis
        /// </summary>
        private readonly Dictionary<string, Dictionary<string, short>> _allInertialCal;

        /// <summary>
        /// Dict of quaternions constantly updated using quaternion calc
        /// </summary>
        private readonly Dictionary<string, Quaternion> _allQuaternion = new Dictionary<string, Quaternion>();

        /// <summary>
        /// Dict of tare quaternions updated when tare is clicked
        /// </summary>
        private readonly Dictionary<string, Quaternion> _allQuaternionTare = new Dictionary<string, Quaternion>();

        /// <summary>
        /// List of the device unique IDs extracted from the quaternion calc dict
        /// Intermidiary used to match IMU order number (which is sent in the packet) to the location of that IMU
        /// </summary>
        private readonly List<string> _deviceKeys;

        /// <summary>
        /// The panel, created by the main thread, which acts as the parent container for the subsequently generated GLControl
        /// </summary>
        private readonly Panel _paintPanel;

        /// <summary>
        /// Delegate associated with the method to invoke the control with
        /// </summary>
        private delegate void InvokeDelegate(GLControl glControl);

        /// <summary>
        /// Dictionary of shaders
        /// </summary>
        private readonly Dictionary<string, ShaderProgram> _shaderDict = new Dictionary<string, ShaderProgram>();
        string _activeShader = "default";

        /// <summary>
        /// Dictionary to keep track of texture IDs
        /// </summary>
        private readonly Dictionary<string, int> _textureDict = new Dictionary<string, int>();
        private bool _frameUpdated = false;

        /// <summary>
        /// Store info about texture coordinates, vertice data, indice data, and colour data
        /// </summary>
        private Vector2[] _texCoordData;
        private Vector3[] _vertData;
        private int[] _indiceData;
        private Vector3[] _colData;

        /// <summary>
        /// Lists of vertice data, indice data, and colour data
        /// </summary>
        private List<Vector2> _texCoordList;
        private List<Vector3> _vertList;
        private List<int> _indList;
        private List<Vector3> _colourList;

        /// <summary>
        /// Buffer for storing the indice data of which vertices to use to draw the triangles
        /// </summary>
        private int _iboElements;

        /// <summary>
        /// Directory where the textures, objects, and shaders are stored
        /// </summary>
        private readonly string _texturesDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Simulation\\Textures");
        private readonly string _objectsDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Simulation\\Objects");
        private readonly string _shadersDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Simulation\\Shaders");

        /// <summary>
        /// 3D vectors of the start positions of the various avatar objects (defined in Madgwick axis frame and later rotated into OpenGL axis frame)
        /// </summary>
        //private readonly Vector3 _torsoStartPos = new Vector3(0f, 30f, 0f);
        //private readonly Vector3 _uArmStartPos = new Vector3(5f, 30f, 3f);
        //private readonly Vector3 _lArmStartPos = new Vector3(5f, 30f, -2.5f);
        private readonly Vector3 _waistStartPos = new Vector3(0f, 20f, -0.5f);
        private readonly Vector3 _torsoStartPos = new Vector3(0f, 20f, 0f);
        private readonly Vector3 _uArmStartPos = new Vector3(3.2f, 20f, 3.6f);
        private readonly Vector3 _lArmStartPos = new Vector3(3.2f, 20f, -0.1f);
        private readonly Vector3 _wristStartPos = new Vector3(3.2f, 20f, -3.1f);

        /// <summary>
        /// List of all the imported objects
        /// </summary>
        private readonly Dictionary<string, Volume> _volumeDict = new Dictionary<string, Volume>();

        /// <summary>
        /// Form Update Timer
        /// </summary>
        public readonly Timer FormUpdateTimer;

        /// <summary>
        /// Rotation around X axis rotates from the default Madgwick reference frame to the OpenGL reference frame
        /// </summary>
        private float _rotTorsoX = -90;
        private float _rotTorsoY = 0;
        private float _rotTorsoZ = 0;
        private float _rotArmX = -90;
        private float _rotArmY = 0;
        private float _rotArmZ = 0;

        /// <summary>
        /// Dictionary containing the labels for joint angles which are dynamically updated
        /// </summary>
        private readonly Dictionary<string, Label> _labelDict;

        /// <summary>
        /// Side of the body the sensors were attached (if Left then axes are inverted to simulate activity in right)
        /// </summary>
        private readonly string _bodySide;

        /// <summary>
        /// Dict of the orientation of each sensor (possible values are Downwards, Upwards, Left, Right)
        /// </summary>
        private readonly Dictionary<string, string> _allOrientation;

        #endregion

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="allQuaternionCalc"></param>
        /// <param name="allInertialCal"></param>
        /// <param name="bodySide"></param>
        /// <param name="allOrientation"></param>
        /// <param name="paintPanel"></param>
        /// <param name="buttonDict"></param>
        /// <param name="labelDict"></param>
        public AvatarClass(Dictionary<string, QuaternionCalc> allQuaternionCalc, Dictionary<string, Dictionary<string, short>> allInertialCal, string bodySide, Dictionary<string, string> allOrientation,
            Panel paintPanel, Dictionary<string,Button> buttonDict, Dictionary<string,Label> labelDict)
        {
            _allQuaternionCalc = allQuaternionCalc;
            _allInertialCal = allInertialCal;
            _bodySide = bodySide;
            _allOrientation = allOrientation;
            _deviceKeys = new List<string>(_allQuaternionCalc.Keys);
            _paintPanel = paintPanel;
            _labelDict = labelDict;

            GlControl = new GLControl();
            GlControl.Dock = System.Windows.Forms.DockStyle.Fill;

            // Create an ordered list (order of device connected) of quaternions values
            foreach (string location in _deviceKeys)
            {
                _allQuaternion.Add(location, new Quaternion());
                _allQuaternionTare.Add(location, new Quaternion(0, 0, 0, 1));
            }

            // Setting up the event handlers for the buttons and the GLControl
            buttonDict["buttonRotLeft"].Click += new System.EventHandler(this.rotLeft_click);
            buttonDict["buttonRotRight"].Click += new System.EventHandler(this.rotRight_click);
            buttonDict["tare"].Click += new System.EventHandler(this.tare_click);

            GlControl.Load += this.glControl_Load;
            GlControl.Paint += this.glControl_Paint;
            GlControl.Resize += this.glControl_Resize;

            // Adding the GLControl dynamically to the paint panel
            paintPanel.Invoke(new InvokeDelegate(AddControl), GlControl);

            // Create a new timer event to refresh the cube
            FormUpdateTimer = new Timer();
            FormUpdateTimer.Interval = 20;
            FormUpdateTimer.Tick += new EventHandler(formUpdateTimer_Tick);
            FormUpdateTimer.Start();
        }

        /// <summary>
        /// Method assigned to a delegate to be invoked by the panel since it is on the GUI thread 
        /// </summary>
        /// <param name="glControl"></param>
        private void AddControl(GLControl glControl)
        {
            _paintPanel.Controls.Add(glControl);
        }

        /// <summary>
        /// Timer tick event to refresh graphics.
        /// </summary>
        private void formUpdateTimer_Tick(object sender, EventArgs e)
        {
            lock (this)
            {
                GlControl.Refresh();
            }
        }

        // This event is called once when the screen loads
        private void glControl_Load(object sender, EventArgs e)
        {
            GL.PointSize(5f);

            // Enable depth testing and set to draw object if it is less than or equal to any already drawn objects
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);

            // Enable textures
            GL.Enable(EnableCap.Texture2D);

            // Image smoothing
            GL.ShadeModel(ShadingModel.Smooth);
            GL.Enable(EnableCap.LineSmooth);

            // Set background color
            GL.ClearColor(Color4.CornflowerBlue);

            InitProgram();
        }

        /// <summary>
        /// Method called once inside of the the glControl_Load event handler
        /// </summary>
        private void InitProgram()
        {
            // Generate a buffer for the indice data of the vertices
            GL.GenBuffers(1, out _iboElements);

            // Generates a program ID, generates buffer for the necessary attributes (i.e. vpos, vcol, vtex), compiles and links the shaders
            _shaderDict.Add("default", new ShaderProgram(Path.Combine(_shadersDir, "vs.glsl"), Path.Combine(_shadersDir, "fs.glsl"), true));
            _shaderDict.Add("textured", new ShaderProgram(Path.Combine(_shadersDir, "vs_tex.glsl"), Path.Combine(_shadersDir, "fs_tex.glsl"), true));

            // Set the default active shader (set as textured since applying textures not colors to objects)
            _activeShader = "textured";

            // Add textures to the dictionary
            _textureDict.Add("texture1", ContentPipe.LoadTexture(Path.Combine(_texturesDir, "texture1.png")));
            _textureDict.Add("texture2", ContentPipe.LoadTexture(Path.Combine(_texturesDir, "texture3.png")));

            // Add the volumes defined in the Madgwick Axis Frame
            ObjVolume obj1 = ContentPipe.LoadObjectFromFile(Path.Combine(_objectsDir, "Waist_New.obj"));
            obj1.TextureId = _textureDict["texture2"];
            _volumeDict.Add("Waist", obj1);

            ObjVolume obj2 = ContentPipe.LoadObjectFromFile(Path.Combine(_objectsDir, "Torso_New.obj"));
            obj2.TextureId = _textureDict["texture2"];
            _volumeDict.Add("Torso", obj2);

            ObjVolume obj3 = ContentPipe.LoadObjectFromFile(Path.Combine(_objectsDir, "UpperArm_New.obj"));
            obj3.TextureId = _textureDict["texture2"];
            _volumeDict.Add("UpperArm", obj3);

            ObjVolume obj4 = ContentPipe.LoadObjectFromFile(Path.Combine(_objectsDir, "LowerArm_New.obj"));
            obj4.TextureId = _textureDict["texture2"];
            _volumeDict.Add("LowerArm", obj4);

            ObjVolume obj5 = ContentPipe.LoadObjectFromFile(Path.Combine(_objectsDir, "Hand_New.obj"));
            obj5.TextureId = _textureDict["texture2"];
            _volumeDict.Add("Wrist", obj5);

            // Initialises the view project matrix (otherwise it is only set once user changes the window size)
            foreach (Volume v in _volumeDict.Values)
            {
                v.ViewProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(1.3f, GlControl.Width / (float)GlControl.Height, 1.0f, 60.0f);
            }
        }

        /// <summary>
        /// Event handler to paint object on the Paint Panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl_Paint(object sender, PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Enable(EnableCap.DepthTest);

            // Enables the active shader attributes for rendering
            _shaderDict[_activeShader].EnableVertexAttribArrays();
            GL.UseProgram(_shaderDict[_activeShader].ProgramID);

            // Where the position/ orientation of camera/ model are actually set and buffers drawn
            // Set the uniform matrix as the modelviewprojectmatrix
            int indiceAt = 0;
            foreach (Volume v in _volumeDict.Values)
            {
                // Sets the uniform matrix for the current object using the modelviewprojectionmatrix
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, v.TextureId);
                GL.UniformMatrix4(_shaderDict[_activeShader].GetUniform("modelview"), false, ref v.ModelViewProjectionMatrix);

                // If the active shaders accepts a texture then send the texture id
                if (_shaderDict[_activeShader].GetUniform("maintexture") != -1)
                {
                    GL.Uniform1(_shaderDict[_activeShader].GetUniform("maintexture"), 0);
                }

                // Draws each set of vertices where mode (primitives to render), type (type of values in indices),
                // indices (pointer to the correct byte the index starts from)
                // frameUpdated is hack to fix problem where trying to access buffer memory that has not been assigned to yet
                if (_frameUpdated)
                {
                    GL.DrawElements(BeginMode.Triangles, v.IndiceCount, DrawElementsType.UnsignedInt, indiceAt * sizeof(uint));
                }

                // This variable continuously increments by the number of vertices of the object just drawn
                indiceAt += v.IndiceCount;
            }
            // Disables the active shader attributes for rendering
            _shaderDict[_activeShader].DisableVertexAttribArrays();

            GL.Flush();
            GlControl.SwapBuffers();

            UpdateFrame();
        }

        // This method is called once on loading of the screen and every time the screen is resized
        private void glControl_Resize(object sender, EventArgs e)
        {
            // Create view projection matrix using the width and height of the screen and between z 1-40
            foreach (Volume v in _volumeDict.Values)
            {
                v.ViewProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(1.3f, GlControl.Width / (float)GlControl.Height, 1.0f, 40.0f);
            }
            // Sets the users visable area (to match the window size)
            GL.Viewport(0, 0, GlControl.Width, GlControl.Height);
        }

        /// <summary>
        /// This method is called once for every new frame
        /// It sets up the conditions of the frame before it is to be rendered on the screen
        /// </summary>
        private void UpdateFrame()
        {
            if (!_frameUpdated)
            {
                _frameUpdated = true;
            }

            _vertList = new List<Vector3>();
            _indList = new List<int>();
            _colourList = new List<Vector3>();
            _texCoordList = new List<Vector2>();

            // Gets the arrays required for painting for each volume and then combine into one long array
            int vertcount = 0;
            foreach (Volume v in _volumeDict.Values)
            {
                _vertList.AddRange(v.GetVerts().ToList());
                _indList.AddRange(v.GetIndices(vertcount).ToList());
                _colourList.AddRange(v.GetColorData().ToList());
                _texCoordList.AddRange(v.GetTextureCoords());

                vertcount += v.VertCount;
            }

            _vertData = _vertList.ToArray();
            _indiceData = _indList.ToArray();
            _colData = _colourList.ToArray();
            _texCoordData = _texCoordList.ToArray();

            // Sending position attribute to the buffers (need this regardless if texture or colour shader)
            GL.BindBuffer(BufferTarget.ArrayBuffer, _shaderDict[_activeShader].GetBuffer("vPosition"));
            GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, (IntPtr)(_vertData.Length * Vector3.SizeInBytes), _vertData, BufferUsageHint.StaticDraw);
            // This method tells OpenGL the format of the buffer so that it can read it
            GL.VertexAttribPointer(_shaderDict[_activeShader].GetAttribute("vPosition"), 3, VertexAttribPointerType.Float, false, 0, 0);

            // Only sends color data if the shader supports it (if not texture shader)
            if (_shaderDict[_activeShader].GetAttribute("vColor") != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, _shaderDict[_activeShader].GetBuffer("vColor"));
                GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, (IntPtr)(_colData.Length * Vector3.SizeInBytes), _colData, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(_shaderDict[_activeShader].GetAttribute("vColor"), 3, VertexAttribPointerType.Float, true, 0, 0);
            }

            // Only sends texture data if the shader supports it (if not colour shader)
            if (_shaderDict[_activeShader].GetAttribute("texcoord") != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, _shaderDict[_activeShader].GetBuffer("texcoord"));
                GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(_texCoordData.Length * Vector2.SizeInBytes), _texCoordData, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(_shaderDict[_activeShader].GetAttribute("texcoord"), 2, VertexAttribPointerType.Float, true, 0, 0);
            }

            // Sends index data to the buffers (need this regardless if texture or colour shader)
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _iboElements);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(_indiceData.Length * sizeof(int)), _indiceData, BufferUsageHint.StaticDraw);

            // Initial rotation vectors describing the angle (in radians) of the starting rotations of the volumes
            // Torso rotation defines rotation from the Madgwick reference frame to the OpenGL reference frame (this will be applied to torso and subsequent body parts by proxy)
            Vector3 iWaistRot = new Vector3(_rotTorsoX * ((float)Math.PI / 180f), _rotTorsoY * ((float)Math.PI / 180f), _rotTorsoZ * ((float)Math.PI / 180f));
            Vector3 iTorsoRot = new Vector3(_rotTorsoX * ((float)Math.PI / 180f), _rotTorsoY * ((float)Math.PI / 180f), _rotTorsoZ * ((float)Math.PI / 180f));
            Vector3 iUpperArmRot = new Vector3(_rotArmX * ((float)Math.PI / 180f), _rotArmY * ((float)Math.PI / 180f), _rotArmZ * ((float)Math.PI / 180f));
            Vector3 iLowerArmRot = new Vector3(_rotArmX * ((float)Math.PI / 180f), _rotArmY * ((float)Math.PI / 180f), _rotArmZ * ((float)Math.PI / 180f));
            Vector3 iWristRot = new Vector3(_rotArmX * ((float)Math.PI / 180f), _rotArmY * ((float)Math.PI / 180f), _rotArmZ * ((float)Math.PI / 180f));

            // Drawing the Waist
            _volumeDict["Waist"].EarthQuaternionGl = Quaternion.FromEulerAngles(iWaistRot.Z, iWaistRot.Y, iWaistRot.X);
            _volumeDict["Waist"].Position = Quaternion.FromEulerAngles(iTorsoRot.Z, 0, iTorsoRot.X) * _waistStartPos;
            _volumeDict["Waist"].RotationMat = Matrix4.CreateFromQuaternion(_volumeDict["Waist"].EarthQuaternionGl);
            _volumeDict["Waist"].Scale = new Vector3(0.12f, 0.12f, 0.12f);

            // Calculating the Torso Rotation and Position
            if (_deviceKeys.Contains("Torso"))
            {
                _volumeDict["Torso"].EarthQuaternionGl = Quaternion.FromEulerAngles(iTorsoRot.Z, iTorsoRot.Y, iTorsoRot.X) * (Quaternion.Invert(_allQuaternionTare["Torso"]) * _allQuaternion["Torso"]);
                _volumeDict["Torso"].EarthQuaternion = Quaternion.Invert(_allQuaternionTare["Torso"]) * _allQuaternion["Torso"];
            }
            else
            {
                _volumeDict["Torso"].EarthQuaternionGl = Quaternion.FromEulerAngles(iTorsoRot.Z, iTorsoRot.Y, iTorsoRot.X);
                _volumeDict["Torso"].EarthQuaternion = Quaternion.FromEulerAngles(0, 0, 0);
            }
            // We set the y rotation to zero here to avoid the location of the torso dynamically changing when we change _rotY (want fixed position)
            _volumeDict["Torso"].Position = Quaternion.FromEulerAngles(iTorsoRot.Z, 0, iTorsoRot.X) * _torsoStartPos;
            _volumeDict["Torso"].RotationMat =  Matrix4.CreateFromQuaternion(_volumeDict["Torso"].EarthQuaternionGl);
            _volumeDict["Torso"].Scale = new Vector3(0.12f, 0.12f, 0.12f);

            // Calculating the updated upper arm position and rotation
            // Rotating the original vector between the upper arm and torso by the earth quaternion of the torso (which includes the rotation from Madgwick to OpenGL axis frame and rotation of the torso)
            // This is then added to the start position of the torso
            Vector3 uArmNewPos = _volumeDict["Torso"].EarthQuaternionGl * (_uArmStartPos - _torsoStartPos);
            uArmNewPos = uArmNewPos + _volumeDict["Torso"].Position;
            if (_deviceKeys.Contains("UpperArm"))
            {
                _volumeDict["UpperArm"].EarthQuaternionGl = Quaternion.FromEulerAngles(iUpperArmRot.Z, iUpperArmRot.Y, iUpperArmRot.X) * (Quaternion.Invert(_allQuaternionTare["UpperArm"]) * _allQuaternion["UpperArm"]);
                _volumeDict["UpperArm"].EarthQuaternion = Quaternion.Invert(_allQuaternionTare["UpperArm"]) * _allQuaternion["UpperArm"];
            }
            else
            {
                _volumeDict["UpperArm"].EarthQuaternionGl = _volumeDict["Torso"].EarthQuaternionGl;
                _volumeDict["UpperArm"].EarthQuaternion = _volumeDict["Torso"].EarthQuaternion;
            }
            _volumeDict["UpperArm"].Position = uArmNewPos;
            _volumeDict["UpperArm"].RotationMat = Matrix4.CreateFromQuaternion( _volumeDict["UpperArm"].EarthQuaternionGl);
            _volumeDict["UpperArm"].Scale = new Vector3(0.1f, 0.1f, 0.1f);

            // Calculating the updated lower arm position and rotation
            Vector3 lArmNewPosTemp = _volumeDict["UpperArm"].EarthQuaternionGl * (_lArmStartPos - _uArmStartPos);
            Vector3 lArmNewPos = lArmNewPosTemp + _volumeDict["UpperArm"].Position;
            if (_deviceKeys.Contains("LowerArm"))
            {
                _volumeDict["LowerArm"].EarthQuaternionGl = Quaternion.FromEulerAngles(iLowerArmRot.Z, iLowerArmRot.Y, iLowerArmRot.X) * (Quaternion.Invert(_allQuaternionTare["LowerArm"]) * _allQuaternion["LowerArm"]);
                _volumeDict["LowerArm"].EarthQuaternion = Quaternion.Invert(_allQuaternionTare["LowerArm"]) * _allQuaternion["LowerArm"];
            }
            else
            {
                _volumeDict["LowerArm"].EarthQuaternionGl = _volumeDict["UpperArm"].EarthQuaternionGl;
                _volumeDict["LowerArm"].EarthQuaternion = _volumeDict["UpperArm"].EarthQuaternion;
            }
            _volumeDict["LowerArm"].Position = lArmNewPos;
            _volumeDict["LowerArm"].RotationMat = Matrix4.CreateFromQuaternion(_volumeDict["LowerArm"].EarthQuaternionGl);
            _volumeDict["LowerArm"].Scale = new Vector3(0.1f, 0.1f, 0.1f);

            // Calculating the updated wrist position and rotation
            Vector3 wristNewPosTemp = _volumeDict["LowerArm"].EarthQuaternionGl * (_wristStartPos - _lArmStartPos);
            Vector3 wristNewPos = wristNewPosTemp + _volumeDict["LowerArm"].Position;
            if (_deviceKeys.Contains("Wrist"))
            {
                _volumeDict["Wrist"].EarthQuaternionGl = Quaternion.FromEulerAngles(iWristRot.Z, iWristRot.Y, iWristRot.X) * (Quaternion.Invert(_allQuaternionTare["Wrist"]) * _allQuaternion["Wrist"]);
                _volumeDict["Wrist"].EarthQuaternion = Quaternion.Invert(_allQuaternionTare["Wrist"]) * _allQuaternion["Wrist"];
            }
            else
            {
                _volumeDict["Wrist"].EarthQuaternionGl = _volumeDict["LowerArm"].EarthQuaternionGl;
                _volumeDict["Wrist"].EarthQuaternion = _volumeDict["LowerArm"].EarthQuaternion;
            }
            _volumeDict["Wrist"].Position = wristNewPos;
            _volumeDict["Wrist"].RotationMat = Matrix4.CreateFromQuaternion(_volumeDict["Wrist"].EarthQuaternionGl);
            _volumeDict["Wrist"].Scale = new Vector3(0.1f, 0.1f, 0.1f);

            // Calculating the magnitude of shoulder and elbow angles (shortest arc angle)
            double shoulderAngleMag = QuaternionCalc.VectorToAngleCrossMag(_volumeDict["Torso"].EarthQuaternion * (_lArmStartPos - _uArmStartPos),_volumeDict["UpperArm"].EarthQuaternion * (_lArmStartPos - _uArmStartPos));
            double elbowAngleMag = QuaternionCalc.VectorToAngleCrossMag(_volumeDict["UpperArm"].EarthQuaternion * (_lArmStartPos - _uArmStartPos),_volumeDict["LowerArm"].EarthQuaternion * (_wristStartPos - _lArmStartPos));
            
            // Calculating the shoulder and elbow twist angles (rotation around the limb)
            Quaternion twistUpper = QuaternionCalc.DecomposeSwingTwist(_volumeDict["UpperArm"].EarthQuaternion * _volumeDict["Torso"].EarthQuaternion.Inverted(), new Vector3(0, 0, -1f));
            double shoulderAngleTwist = QuaternionCalc.VectorToAngleCross(new Quaternion(), twistUpper, new Vector3(0, 0, 1f), new Vector3(0, 1f, 0));

            Quaternion twistLower = QuaternionCalc.DecomposeSwingTwist(_volumeDict["LowerArm"].EarthQuaternion * _volumeDict["UpperArm"].EarthQuaternion.Inverted(), new Vector3(0, 0, -1f));
            double elbowAngleTwist = QuaternionCalc.VectorToAngleCross(new Quaternion(), twistLower, new Vector3(0, 0, 1f), new Vector3(0, 1f, 0));

            // Displaying normalised kinematic metrics
            _labelDict["labelShoulderMag"].Text = Math.Round(shoulderAngleMag, 3).ToString();
            _labelDict["labelShoulderTwist"].Text = Math.Round(shoulderAngleTwist, 3).ToString();
            _labelDict["labelShoulderX"].Text = Math.Round(lArmNewPosTemp.X / 3.7f, 3).ToString();
            _labelDict["labelShoulderY"].Text = Math.Round(lArmNewPosTemp.Y / 3.7f, 3).ToString();
            _labelDict["labelShoulderZ"].Text = Math.Round(lArmNewPosTemp.Z / 3.7f, 3).ToString();

            _labelDict["labelElbowMag"].Text = Math.Round(elbowAngleMag, 3).ToString();
            _labelDict["labelElbowTwist"].Text = Math.Round(elbowAngleTwist, 3).ToString();
            _labelDict["labelElbowX"].Text = Math.Round(wristNewPosTemp.X / 3.0f, 3).ToString();
            _labelDict["labelElbowY"].Text = Math.Round(wristNewPosTemp.Y / 3.0f, 3).ToString();
            _labelDict["labelElbowZ"].Text = Math.Round(wristNewPosTemp.Z / 3.0f, 3).ToString();

            // SETTING CAMERA AND MODEL VIEW AND ORIENTATION
            // Calculate the model matrix and set the projection matrix and modelview project matrix
            foreach (Volume v in _volumeDict.Values)
            {
                v.CalculateModelMatrix();
                v.ModelViewProjectionMatrix = v.ModelMatrix * v.ViewProjectionMatrix;
            }

            // Clear the buffer bindings
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        /// <summary>
        /// Rotate the Avatar object around the OpenGL y axis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rotLeft_click(object sender, EventArgs e)
        {
            _rotTorsoY += 20;
            _rotArmY += 20;
        }
        private void rotRight_click(object sender, EventArgs e)
        {
            _rotTorsoY -= 20;
            _rotArmY -= 20;
        }

        /// <summary>
        /// Tare the quaternions for each connected IMU
        /// Upper and Lower arm segments are tared according to the tare positions (semi-pronated arm vertical and 90 degrees from vertical)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tare_click(object sender, EventArgs e)
        {
            _rotTorsoY = 0;
            _rotArmY = 0;

            // Tare values for semi-pronated arm, 90 degrees from vertical
            //_rotArmY = -90;
            //_rotArmX = 0;

            // Tare values for semi-pronated arm

            foreach (string location in _deviceKeys)
            {
                _allQuaternionTare[location] = _allQuaternion[location];
            }
        }

        #region EventHandlers

        /// <summary>
        /// Handles packet decoded event handler
        /// Passes the IMU data (if included in packet) and uses it to update the stored value of quaternion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnPacketDecoded(object sender, PacketDecodedEventArgs e)
        {
            Dictionary<string, int[]> dataDict = e.DecodedPacket;
            if (dataDict.ContainsKey("IMU"))
            {
                string key = _deviceKeys[(int)dataDict["Packet Details"][2]];

                // Removing the calibration values from the inertial data
                // Z axis of the magnetometer is inverted so that it is in the same left handed axis frame as the other inertial data
                float gyrX = (dataDict["IMU"][0] - _allInertialCal[key]["GyrX"]);
                float gyrY = (dataDict["IMU"][1] - _allInertialCal[key]["GyrY"]);
                float gyrZ = (dataDict["IMU"][2] - _allInertialCal[key]["GyrZ"]);
                float magX = (dataDict["IMU"][3] - _allInertialCal[key]["MagX"]);
                float magY = (dataDict["IMU"][4] - _allInertialCal[key]["MagY"]);
                float magZ = (-(dataDict["IMU"][5] - _allInertialCal[key]["MagZ"]));
                float accX = (dataDict["IMU"][6] - _allInertialCal[key]["AccX"]);
                float accY = (dataDict["IMU"][7] - _allInertialCal[key]["AccY"]);
                float accZ = (dataDict["IMU"][8] - _allInertialCal[key]["AccZ"]);

                // Rotating from the sensor axis frame to the Madgwick expected axis frame (x and z axis effectively inverted)
                float[] mA = QuaternionCalc.RotationMatrixY(gyrX, gyrY, gyrZ, magX, magY, magZ, accX, accY, accZ, 180);

                // Rotating the axis frame for the IMU to be pointing parallel to the gravity vector (rather than perpendicular)
                mA = QuaternionCalc.RotationMatrixX(mA[0], mA[1], mA[2], mA[3], mA[4], mA[5], mA[6], mA[7], mA[8], -90);

                // Rotate the axis frame for the case that the IMU is pointing a direction other than downwards
                // Method of getting no rotation quaternion (0 0 0 1) at different starting orientations
                if (_allOrientation[key] == "Upwards")
                {
                    mA = QuaternionCalc.RotationMatrixY(mA[0], mA[1], mA[2], mA[3], mA[4], mA[5], mA[6], mA[7], mA[8], 180);
                }
                else if (_allOrientation[key] == "Left")
                {
                    mA = QuaternionCalc.RotationMatrixY(mA[0], mA[1], mA[2], mA[3], mA[4], mA[5], mA[6], mA[7], mA[8], 90);
                }
                else if (_allOrientation[key] == "Right")
                {
                    mA = QuaternionCalc.RotationMatrixY(mA[0], mA[1], mA[2], mA[3], mA[4], mA[5], mA[6], mA[7], mA[8], -90);
                }

                // If the side of the body the sensors are attached is left then certain axes are inverted to simulate right side movement
                if (_bodySide == "Left" && key != "Torso")
                {
                    mA[1] = -mA[1];
                    mA[2] = -mA[2];
                    mA[3] = -mA[3];
                    mA[6] = -mA[6];
                }

                // Data passed to Quatupdate in form GXYZ, MXYZ, AXYZ
                _allQuaternion[key] = _allQuaternionCalc[key].Quatupdate(mA[0], mA[1], mA[2], mA[3], mA[4], mA[5], mA[6], mA[7], mA[8]);
                // var eulerAngles = QuaternionCalc.QuatToEulerAngles(_allQuaternion[key]);
            }
        }

        #endregion
    }
}
