﻿using OpenTK;

namespace NU_GUI_Saver.Simulation
{
    public abstract class Volume
    {
        /// <summary>
        /// Bool to signify whether the volume is to be textured
        /// </summary>
        public bool IsTextured = false;

        /// <summary>
        /// Id of the texture
        /// </summary>
        public int TextureId;

        /// <summary>
        /// Normals
        /// </summary>
        readonly Vector3[] Normals = new Vector3[0];

        /// <summary>
        /// Earth Quaternion in the OpenGl reference frame
        /// </summary>
        public Quaternion EarthQuaternionGl;

        /// <summary>
        /// Earth Quaternion in the Madgwick reference frame
        /// </summary>
        public Quaternion EarthQuaternion;

        /// <summary>
        /// Position, rotation, and scale of the volume
        /// </summary>
        public Vector3 Position = Vector3.Zero;
        public Vector3 Rotation = Vector3.Zero;
        public Vector3 Scale = Vector3.One;

        /// <summary>
        /// Rotation matrix used to rotate the object
        /// </summary>
        public Matrix4 RotationMat;

        /// <summary>
        /// Vertice, indice, and colour data count
        /// </summary>
        public virtual int VertCount { get; set; }
        public virtual int IndiceCount { get; set; }
        public virtual int ColourDataCount { get; set; }

        /// <summary>
        /// Get the normal count
        /// </summary>
        public virtual int NormalCount
        {
            get { return Normals.Length; }
        }

        /// <summary>
        /// Get the normals
        /// </summary>
        /// <returns></returns>
        public virtual Vector3[] GetNormals()
        {
            return Normals;
        }

        /// <summary>
        /// Convert Euler Angles to Rotation Matrix
        /// </summary>
        /// <param name="eulerAngles"></param>
        public void EulerToRotMatrix(Vector3 eulerAngles)
        {
            RotationMat = Matrix4.CreateRotationX(eulerAngles.X) * Matrix4.CreateRotationY(eulerAngles.Y) *
                                     Matrix4.CreateRotationZ(eulerAngles.Z);
        }

        // ModelMatrix - frame position of what is being drawn
        // ViewMatrix - position (location and orientation of the camera)
        // ModelViewMatrix - concatenation of model matrix and view matrix
        public Matrix4 ModelMatrix = Matrix4.Identity;
        public Matrix4 ViewProjectionMatrix = Matrix4.Identity;
        public Matrix4 ModelViewProjectionMatrix = Matrix4.Identity;

        // Abstract methods to be implemented in the inherited class
        public abstract Vector3[] GetVerts();
        public abstract int[] GetIndices(int offset = 0);
        public abstract Vector3[] GetColorData();
        public abstract void CalculateModelMatrix();
        public abstract Vector2[] GetTextureCoords();
    }
}
