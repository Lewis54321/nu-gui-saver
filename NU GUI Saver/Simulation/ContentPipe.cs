﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Drawing.Imaging;
using NU_GUI_Saver.Simulation;

namespace NU_GUI_Saver
{

    /// <summary>
    /// Class for storing static methods for managing content for OpenTK
    /// Stores methods for loading textures and objects 
    /// Also contains a method for normalising the vertices so that the axis of rotation is approximately the centre of the object
    /// </summary>

    class ContentPipe
    {
        // Method to load an image as a bitmap
        public static int LoadTexture(Bitmap image)
        {
            int texID = GL.GenTexture();

            // Bind the texture as a 2-D texture
            GL.BindTexture(TextureTarget.Texture2D, texID);
            BitmapData data = image.LockBits(new System.Drawing.Rectangle(0, 0, image.Width, image.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            // Send the data for its pixels to the graphics card
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

            image.UnlockBits(data);

            // Graphics card generates mipmaps for the texture
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

            return texID;
        }

        // Overload of load image to provide filename rather than image
        public static int LoadTexture(string filename)
        {
            try
            {
                Bitmap file = new Bitmap(filename);
                //file.RotateFlip(RotateFlipType.RotateNoneFlipX);
                return LoadTexture(file);
            }
            catch (FileNotFoundException e)
            {
                return -1;
            }
        }

        /// <summary>
        /// Method for loading a object from file directory
        /// Passes the entire object string extracted from the filename to the LoadObjectFromString method
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static ObjVolume LoadObjectFromFile(string filename)
        {
            ObjVolume obj = new ObjVolume();
            try
            {
                using (StreamReader reader = new StreamReader(new FileStream(filename, FileMode.Open, FileAccess.Read)))
                {
                    obj = LoadObjectFromString(reader.ReadToEnd());
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading file: {0}", filename);
            }

            // Added method for finding the normalised vertices
            //Vector3[] normVertices = NormaliseVertices(obj.vertices);
            //obj.vertices = normVertices;

            return obj;
        }

        // 
        /// <summary>
        /// Method for ensuring that the vertices have a mean of zero in each direction
        /// This should mean that they rotate approximately around their own respective axes
        /// </summary>
        /// <param name="vertices"></param>
        /// <returns></returns>
        private static Vector3[] NormaliseVertices(Vector3[] vertices)
        {
            Vector3 sumVertices = Vector3.Zero;
            Vector3 meanVertices;
            Vector3[] normVertices = new Vector3[vertices.Length];
            int count = 0;

            foreach (Vector3 vertice in vertices)
            {
                sumVertices[0] += vertice[0];
                sumVertices[1] += vertice[1];
                sumVertices[2] += vertice[2];
            }

            meanVertices = sumVertices / vertices.Length;

            foreach (Vector3 vertice in vertices)
            {
                normVertices[count] = vertice - meanVertices;
                count += 1;
            }

            return normVertices;
        }

        /// <summary>
        /// Loads an object from the entire string of characters
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static ObjVolume LoadObjectFromString(string obj)
        {
            // Seperate lines from the file
            List<String> lines = new List<string>(obj.Split('\n'));

            // Lists to hold model data
            List<Vector3> vertList = new List<Vector3>();
            List<Vector3> colorList = new List<Vector3>();
            List<Vector2> texList = new List<Vector2>();
            List<Tuple<int, int, int>> faces = new List<Tuple<int, int, int>>();

            // Read file line by line
            foreach (String line in lines)
            {
                // Line contains a vertex definition
                if (line.StartsWith("v "))
                {
                    // Cut off beginning of line
                    String temp = line.Substring(2);

                    Vector3 vec = new Vector3();

                    // Checks that there are two spaces in between the 3 values
                    if (temp.Count((char c) => c == ' ') == 2)
                    {
                        String[] vertparts = temp.Split(' ');

                        // Attempt to parse each part of the vertice
                        bool success = float.TryParse(vertparts[0], out vec.X);
                        success &= float.TryParse(vertparts[1], out vec.Y);
                        success &= float.TryParse(vertparts[2], out vec.Z);

                        // Dummy color/texture coordinates for now
                        colorList.Add(new Vector3((float)Math.Sin(vec.Z), (float)Math.Sin(vec.Z),
                            (float)Math.Sin(vec.Z)));
                        texList.Add(new Vector2((float)Math.Sin(vec.Z), (float)Math.Sin(vec.Z)));

                        // If any of the parses failed, report the error
                        if (!success)
                        {
                            Console.WriteLine("Error parsing vertex: {0}", line);
                        }
                    }
                    vertList.Add(vec);
                }

                else if (line.StartsWith("f "))
                {
                    // Cut off beginning of line
                    string temp = line.Substring(2);

                    Tuple<int, int, int> face = new Tuple<int, int, int>(0, 0, 0);

                    // Check if there is enough elements for a face
                    if (temp.Count((char c) => c == ' ') == 2)
                    {
                        string[] faceParts = new string[3];

                        // If the obj file is in the slash format then need an extra step
                        if (temp.Count((char c) => c == '/') == 6)
                        {
                            string[] sectParts = temp.Split(' ');

                            string[] splitVar = new string[1] { @"//" };
                            int count = 0;

                            foreach (string valuepart in sectParts)
                            {
                                string[] output = valuepart.Split(splitVar, StringSplitOptions.RemoveEmptyEntries);
                                faceParts[count] = output[0];
                                count += 1;
                            }
                        }
                        else
                        {
                            faceParts = temp.Split(' ');
                        }

                        int i1, i2, i3;

                        // Attempt to parse each part of the face
                        bool success = int.TryParse(faceParts[0], out i1);
                        success &= int.TryParse(faceParts[1], out i2);
                        success &= int.TryParse(faceParts[2], out i3);

                        // If any of the parses failed, report the error
                        if (!success)
                        {
                            Console.WriteLine("Error parsing face: {0}", line);
                        }
                        else
                        {
                            // Decrement to get zero-based vertex numbers
                            face = new Tuple<int, int, int>(i1 - 1, i2 - 1, i3 - 1);
                            faces.Add(face);
                        }
                    }
                }
            }

            // Create the ObjVolume
            ObjVolume vol = new ObjVolume();
            vol.Vertices = vertList.ToArray();
            vol.faces = new List<Tuple<int, int, int>>(faces);
            vol.Colours = colorList.ToArray();
            vol.TextureCoords = texList.ToArray();

            return vol;
        }
    }
}

