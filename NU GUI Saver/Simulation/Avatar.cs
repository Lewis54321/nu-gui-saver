﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Neokabuto_Tutorials;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace NU_GUI_Saver.Simulation
{
    public partial class Avatar : Form
    {
        #region Fields

        /// <summary>
        /// Stores shaders
        /// </summary>
        Dictionary<string, ShaderProgram> shaders = new Dictionary<string, ShaderProgram>();
        string activeShader = "default";

        /// <summary>
        /// Dictionary to keep track of texture IDs
        /// </summary>
        Dictionary<string, int> textures = new Dictionary<string, int>();
        //List<Texture2D> textures = new List<Texture2D>();

        private bool frameUpdated = false;

        /// <summary>
        /// Store info about texture coordinates
        /// </summary>
        private Vector2[] texcoorddata;

        private Vector3[] vertdata;
        private int[] indicedata;
        private Vector3[] coldata;

        private List<Vector3> verts;
        private List<int> inds;
        private List<Vector3> colors;

        // Texture coordinates
        private List<Vector2> texcoords;

        /// <summary>
        /// Buffer for storing the indice data of which vertices to use to draw the triangles
        /// </summary>
        private int ibo_elements;

        /// <summary>
        /// Directory where the textures, objects, and shaders are stored
        /// </summary>
        string texturesDir = Path.Combine(Directory.GetCurrentDirectory(), "Simulation\\Images");
        string objectsDir = Path.Combine(Directory.GetCurrentDirectory(), "Simulation\\Objects");
        string shadersDir = Path.Combine(Directory.GetCurrentDirectory(), "Simulation\\Shaders");

        /// <summary>
        /// 3D vectors of the start positions of the various avatar objects
        /// </summary>
        Vector3 torsoStartPos = new Vector3(0f, 0f, -30.0f);
        Vector3 uArmStartPos = new Vector3(5f, 2f, -30.0f);
        Vector3 lArmStartPos = new Vector3(5f, -5f, -30.0f);

        /// <summary>
        /// List of all the imported objects
        /// </summary>
        List<Volume> objects = new List<Volume>();

        /// <summary>
        /// Form Update Timer
        /// </summary>
        private Timer formUpdateTimer;

        /// <summary>
        /// Counter for the frame number
        /// </summary>
        private int frameNo = 0;

        /// <summary>
        /// Updated quaternion returned from the quaternion update function
        /// </summary>
        private Quaternion quaternion = new Quaternion(0f, 0f, 0f, 1f);

        #endregion

        public Avatar()
        {
            InitializeComponent();

            // Load the torso
            //ObjVolume torso = ContentPipe.LoadObjectFromFile(
            //    @"D:\Box Sync\IP 10 - 05 - 17\SC - PhD\NU GUI Saver\NU GUI Saver\Simulation\Avatar Objects\Avatar_Torso.obj");
            //objects.Add(torso);

            // Create a new timer event to refresh the cube
            formUpdateTimer = new Timer();
            formUpdateTimer.Interval = 40;
            formUpdateTimer.Tick += new EventHandler(formUpdateTimer_Tick);
            formUpdateTimer.Start();
        }

        // This event is called once when the screen loads
        private void glControl1_Load(object sender, EventArgs e)
        {
            initProgram();

            GL.PointSize(5f);

            // Enable depth testing and set to draw object if it is less than or equal to any already drawn objects
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);

            // Enable textures
            GL.Enable(EnableCap.Texture2D);

            // Image smoothing
            GL.ShadeModel(ShadingModel.Smooth);
            GL.Enable(EnableCap.LineSmooth);

            // Set background colour
            GL.ClearColor(Color4.CornflowerBlue);
        }

        // This method is called once inside of OnLoad event handler
        private void initProgram()
        {
            // Generate a buffer for the indice data of the vertices
            GL.GenBuffers(1, out ibo_elements);

            // Generates a program ID, generates buffer for the necessary attributes (i.e. vpos, vcol, vtex), compiles and links the shaders
            shaders.Add("default", new ShaderProgram(Path.Combine(shadersDir,"vs.glsl"), Path.Combine(shadersDir, "fs.glsl"), true));
            shaders.Add("textured", new ShaderProgram(Path.Combine(shadersDir,"vs_tex.glsl"), Path.Combine(shadersDir,"fs_tex.glsl"), true));

            // Set the default active shader (set as textured since applying textures not colours to objects)
            activeShader = "textured";

            // Add textures
            textures.Add("opentksquare.png", ContentPipe.LoadTexture(Path.Combine(texturesDir, "opentksquare.png")));
            textures.Add("opentksquare2.png", ContentPipe.LoadTexture(Path.Combine(texturesDir, "opentksquare2.png")));

            // Add Objects to display
            ObjVolume obj1 = ContentPipe.LoadObjectFromFile(Path.Combine(objectsDir, "TorsoF.obj"));
            obj1.TextureID = textures["opentksquare.png"];
            objects.Add(obj1);

            ObjVolume obj2 = ContentPipe.LoadObjectFromFile(Path.Combine(objectsDir, "UpperArmF.obj"));
            obj2.TextureID = textures["opentksquare.png"];
            objects.Add(obj2);

            ObjVolume obj3 = ContentPipe.LoadObjectFromFile(Path.Combine(objectsDir, "LowerArmF.obj"));
            obj3.TextureID = textures["opentksquare.png"];
            objects.Add(obj3);

            // Initialises the view project matrix (otherwise it is only set once user changes the window size)
            foreach (Volume v in objects)
            {
                v.ViewProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(1.3f, ClientSize.Width / (float)ClientSize.Height, 1.0f, 60.0f);
            }
        }

        // Paint method used to render the scene
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.Enable(EnableCap.DepthTest);

            // Enables the active shader attributes for rendering
            shaders[activeShader].EnableVertexAttribArrays();

            GL.UseProgram(shaders[activeShader].ProgramID);

            // Draw the volumes
            int indiceat = 0;

            // Where the position/ orientation of camera/ model are actually set and buffers drawn
            // Set the uniform matrix as the modelviewprojectmatrix
            foreach (Volume v in objects)
            {
                // Sets the uniform matrix for the current object using the modelviewprojectionmatrix
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, v.TextureID);
                GL.UniformMatrix4(shaders[activeShader].GetUniform("modelview"), false, ref v.ModelViewProjectionMatrix);

                // If the active shaders accepts a texture then send the texture id
                if (shaders[activeShader].GetUniform("maintexture") != -1)
                {
                    GL.Uniform1(shaders[activeShader].GetUniform("maintexture"), 0);
                }

                // Draws each set of vertices
                // Mode- specifies what primitives to render
                // IndiceCount- The number of elements to be rendered (equal to no faces times 3) (faces xyz coords)
                // Type- The type of values in indices (faces is tuple of unsigned ints)
                // Indices- Pointed to the location where the indices are stored (finds the correct byte the index starts from)
                // frameUpdated is hack to fix problem where trying to access buffer memory that has not been assigned to yet
                if (frameUpdated)
                {
                    GL.DrawElements(BeginMode.Triangles, v.IndiceCount, DrawElementsType.UnsignedInt, indiceat * sizeof(uint));
                }

                // This variable continuously increments by the number of vertices of the object just drawn
                indiceat += v.IndiceCount;
            }
            // Disables the active shader attributes for rendering
            shaders[activeShader].DisableVertexAttribArrays();

            GL.Flush();
            glControl1.SwapBuffers();
        }

        // This method is called once on loading of the screen and every time the screen is resized
        private void glControl1_Resize(object sender, EventArgs e)
        {
            // Create view projection matrix using the width and height of the screen and between z 1-40
            foreach (Volume v in objects)
            {
                v.ViewProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(1.3f, ClientSize.Width / (float)ClientSize.Height, 1.0f, 40.0f);
            }
            // Sets the users visable area (to match the window size)
            //GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);
            GL.Viewport(0, 0, Width, Height);
        }

        /// <summary>
        /// Timer tick event to refresh graphics.
        /// </summary>
        private void formUpdateTimer_Tick(object sender, EventArgs e)
        {
            lock (this)
            {
                glControl1.Refresh();

                UpdateFrame();
            }
        }

        /// <summary>
        /// This method is called once for every new frame
        /// It sets up the conditions of the frame before it is to be rendered on the screen
        /// </summary>
        private void UpdateFrame()
        {
            if (!frameUpdated)
            {
                frameUpdated = true;
            }

            // SETTING VOLUME SIZE AND COLOUR
            verts = new List<Vector3>();
            inds = new List<int>();
            colors = new List<Vector3>();

            // Texture coordinates
            texcoords = new List<Vector2>();

            int vertcount = 0;

            foreach (Volume v in objects)
            {
                verts.AddRange(v.GetVerts().ToList());
                inds.AddRange(v.GetIndices(vertcount).ToList());
                colors.AddRange(v.GetColorData().ToList());

                texcoords.AddRange(v.GetTextureCoords());

                vertcount += v.VertCount;
            }

            vertdata = verts.ToArray();
            indicedata = inds.ToArray();
            coldata = colors.ToArray();
            texcoorddata = texcoords.ToArray();

            // Sending position attribute to the buffers (need this regardless if texture or colour shader)
            GL.BindBuffer(BufferTarget.ArrayBuffer, shaders[activeShader].GetBuffer("vPosition"));
            GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, (IntPtr)(vertdata.Length * Vector3.SizeInBytes), vertdata, BufferUsageHint.StaticDraw);
            // This method tells OpenGL the format of the buffer so that it can read it
            GL.VertexAttribPointer(shaders[activeShader].GetAttribute("vPosition"), 3, VertexAttribPointerType.Float, false, 0, 0);

            // Only sends color data if the shader supports it (if not texture shader)
            if (shaders[activeShader].GetAttribute("vColor") != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, shaders[activeShader].GetBuffer("vColor"));
                GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, (IntPtr)(coldata.Length * Vector3.SizeInBytes), coldata, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(shaders[activeShader].GetAttribute("vColor"), 3, VertexAttribPointerType.Float, true, 0, 0);
            }

            // Only sends texture data if the shader supports it (if not colour shader)
            if (shaders[activeShader].GetAttribute("texcoord") != -1)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, shaders[activeShader].GetBuffer("texcoord"));
                GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(texcoorddata.Length * Vector2.SizeInBytes), texcoorddata, BufferUsageHint.StaticDraw);
                GL.VertexAttribPointer(shaders[activeShader].GetAttribute("texcoord"), 2, VertexAttribPointerType.Float, true, 0, 0);
            }

            // Sends index data to the buffers (need this regardless if texture or colour shader)
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, ibo_elements);
            GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indicedata.Length * sizeof(int)), indicedata, BufferUsageHint.StaticDraw);

            frameNo += 1;

            Vector3 torsoRot = new Vector3(0f, 0.3f * frameNo / (float)10, 0f);
            Vector3 uArmRot = new Vector3(0f, 0f, 0.3f * (float)Math.Sin(0.1f * frameNo));
            Vector3 lArmRot = new Vector3(0f, 0f, 0f);

            objects[0].Position = torsoStartPos;
            objects[0].Rotation = torsoRot;
            //objects[0].Rotation = new Vector3(0f, 0.55f * frameNo / (float)10, 0f);
            objects[0].Scale = new Vector3(0.1f, 0.1f, 0.1f);

            // Calculating the updated upper arm position
            Matrix4 torsoRotMatrix = Matrix4.CreateRotationX(torsoRot.X) * Matrix4.CreateRotationY(torsoRot.Y) *
                                    Matrix4.CreateRotationZ(torsoRot.Z);
            Vector3 uArmNewPos = torsoRotMatrix.ExtractRotation() * (uArmStartPos - torsoStartPos);
            uArmNewPos = uArmNewPos + torsoStartPos;

            // Calculating the updated upper arm rotation
            Matrix4 uArmRotMatrix = Matrix4.CreateRotationX(uArmRot.X) * Matrix4.CreateRotationY(uArmRot.Y) *
                                     Matrix4.CreateRotationZ(uArmRot.Z);
            Vector3 uArmRotNewRF = torsoRotMatrix.ExtractRotation() * uArmRot;
            Matrix4 uArmRotMatrixRF = Matrix4.CreateRotationX(uArmRotNewRF.X) * Matrix4.CreateRotationY(uArmRotNewRF.Y) *
                                      Matrix4.CreateRotationZ(uArmRotNewRF.Z);

            // Calculating the updates lower arm position
            Vector3 lArmNewPos = uArmRotMatrixRF.ExtractRotation() * (lArmStartPos - uArmStartPos);
            lArmNewPos = lArmNewPos + uArmNewPos;

            objects[1].Position = uArmNewPos;
            objects[1].Rotation = uArmRotNewRF;
            //objects[1].Rotation = new Vector3(0f, 0.55f * frameNo / (float)10, 0);
            objects[1].Scale = new Vector3(0.1f, 0.1f, 0.1f);

            objects[2].Position = lArmNewPos;
            objects[2].Rotation = lArmRot;
            //objects[2].Rotation = new Vector3(0f, 0.55f * frameNo / (float)10, 0);
            objects[2].Scale = new Vector3(0.1f, 0.1f, 0.1f);

            // SETTING CAMERA AND MODEL VIEW AND ORIENTATION
            // Calculate the model matrix and set the projection matrix and modelview project matrix
            foreach (Volume v in objects)
            {
                v.CalculateModelMatrix();
                v.ModelViewProjectionMatrix = v.ModelMatrix * v.ViewProjectionMatrix;
            }

            //GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);

            // Clear the buffer bindings
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        #region EventHandlers

        /// <summary>
        /// Handles packet decoded event handler
        /// Passes the IMU data (if included in packet) and uses it to update the stored value of quaternion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //public void OnPacketDecoded(object sender, PacketDecodedEventArgs e)
        //{
        //    Dictionary<string, int[]> _dataDict = e.decoded_packet;
        //    if (_dataDict.ContainsKey("IMU"))
        //    {
        //        if (_dataDict["Packet Details"][0] == 0)
        //        {
        //            // Data passed to Quatupdate in form GXYZ, MXYZ, AXYZ
        //            quaternion = quaternionCalc.Quatupdate(_dataDict["IMU"][0], _dataDict["IMU"][1], _dataDict["IMU"][2],
        //                _dataDict["IMU"][3],
        //                _dataDict["IMU"][4], _dataDict["IMU"][5], _dataDict["IMU"][6], _dataDict["IMU"][7],
        //                _dataDict["IMU"][8]);
        //        }
        //    }
        //}

        #endregion
    }
}
