﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Threading;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace NU_GUI_Saver.Simulation
{
    public class Cube3dClass
    {
        #region Fields

        /// <summary>
        /// Instance of the object used to calculated quaternions from raw sensor data
        /// </summary>
        private readonly QuaternionCalc _quaternionCalc;

        /// <summary>
        /// Quaternion value which is updated for each packet decoded by the quaternionCalc object instance
        /// </summary>
        private Quaternion _quaternion;

        private Quaternion _tareQuat = new Quaternion(0,0,0,1);

        /// <summary>
        /// The panel, created by the main thread, which acts as the parent container for the subsequently generated GLControl
        /// </summary>
        private readonly Panel _paintPanel;

        /// <summary>
        /// Delegate associated with the method to invoke the control with
        /// </summary>
        /// <param name="glControl"></param>
        private delegate void InvokeDelegate(GLControl glControl);

        /// <summary>
        /// Directory where the images of the cuboid are stored
        /// </summary>
        private readonly string _imageDir;

        /// <summary>
        /// User set dimensions of the cuboid
        /// </summary>
        private readonly float _halfDimensionX;
        private readonly float _halfDimensionY;
        private readonly float _halfDimensionZ;

        /// <summary>
        /// GLControl widget to be painted on, passed from the main thread 
        /// </summary>
        public readonly GLControl GlControl;

        /// <summary>
        /// dict of texture name assigned to its respective texture ID
        /// </summary>
        private readonly Dictionary<string, int> _texturesDict;

        /// <summary>
        /// Variable whose value is set depending on user button presses and allows rotation of the cuboid around the gravity vector
        /// Cube must be rotated 90 degrees around the x axis in order to display the cube correctly (due to the 90 degree rotation applied
        /// to this axis frame in order to convert from the Madgwick to OpenGL axis frame
        /// </summary>
        private float _rotX = -90;
        private float _rotY = 0;
        private float _rotZ = 0;

        /// <summary>
        /// Form update timer used to refresh the cuboid and repaint it
        /// </summary>
        public System.Windows.Forms.Timer FormUpdateTimer { get; private set; }

        private Dictionary<string, Int16> _calValues;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor that calls default constructor with default dimensions and image directory
        /// </summary>
        public Cube3dClass(QuaternionCalc quaternionCalc, Panel paintPanel, Dictionary<string,Button> buttonDict, Dictionary<string, Int16> calValues)
            : this(quaternionCalc, paintPanel, buttonDict, calValues, new float[] { 4f, 6f, 2f }, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Simulation\\Textures"))
        {

        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="quaternionCalc"></param>
        /// <param name="paintPanel"></param>
        /// <param name="buttonDict"></param>
        /// <param name="dimensions"></param>
        /// <param name="imageDir"></param>
        public Cube3dClass(QuaternionCalc quaternionCalc, Panel paintPanel, Dictionary<string,Button> buttonDict, Dictionary<string, Int16> calValues, float[] dimensions, string imageDir)
        {
            _paintPanel = paintPanel;
            _quaternionCalc = quaternionCalc;
            GlControl = new GLControl();
            GlControl.Dock = System.Windows.Forms.DockStyle.Fill;
            _imageDir = imageDir;
            _calValues = calValues;

            // Setting up all the events for the glControl and the buttons associated with it and making the screen visable
            GlControl.Load += this.glControl_Load;
            GlControl.Paint += this.glControl_Paint;
            GlControl.Resize += this.glControl_Resize;

            paintPanel.Invoke(new InvokeDelegate(AddControl), GlControl);

            // Dimensions of the cuboid
            _halfDimensionX = dimensions[0] / 2;
            _halfDimensionY = dimensions[1] / 2;
            _halfDimensionZ = dimensions[2] / 2;

            // Making the glControl visable which will then call the glControlLoad event handler
            //glControl.Visible = true;

            buttonDict["rotLeft"].Click += new System.EventHandler(this.rotLeft_click);
            buttonDict["rotRight"].Click += new System.EventHandler(this.rotRight_click);
            buttonDict["tare"].Click += new System.EventHandler(this.tare_click);

            // Load all the textures for each side of the cube
            var imageFiles = Directory.GetFiles(_imageDir, "*.PNG");
            _texturesDict = LoadTexturesFromImages(imageFiles);

            // Create a new timer event to refresh the cube
            FormUpdateTimer = new System.Windows.Forms.Timer();
            FormUpdateTimer.Interval = 20;
            FormUpdateTimer.Tick += new EventHandler(formUpdateTimer_Tick);
            FormUpdateTimer.Start();
        }

        /// <summary>
        /// Method assinged to a delegate to be invoked by the panel since it is on the GUI thread 
        /// </summary>
        /// <param name="linePlot"></param>
        private void AddControl(GLControl glControl)
        {
            _paintPanel.Controls.Add(glControl);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Functions takes list of filenames of png files and returns a dictionary of Text2D objects which contain
        /// the id of the GL texture as well as its width, height, and filename
        /// </summary>
        /// <param name="fileNames"></param>
        /// <returns></returns>
        private Dictionary<string, int> LoadTexturesFromImages(string[] fileNames)
        {
            int numPics = fileNames.Length;
            Dictionary<string, int> texturesDict = new Dictionary<string, int>();

            for (int im = 0; im < numPics; im++)
            {
                string file = Path.GetFileName(fileNames[im]);
                texturesDict.Add(file, ContentPipe.LoadTexture(fileNames[im]));
            }

            return texturesDict;
        }

        /// <summary>
        /// Timer tick event to refresh graphics.
        /// </summary>
        private void formUpdateTimer_Tick(object sender, EventArgs e)
        {
             GlControl.Refresh();
        }

        private void glControl_Load(object sender, EventArgs e)
        {
            // Enable depth testing and set to draw object if it is less than or equal to any already drawn objects
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);

            // Enable textures
            GL.Enable(EnableCap.Texture2D);

            // Image smoothing
            GL.ShadeModel(ShadingModel.Smooth);
            GL.Enable(EnableCap.LineSmooth);

            // Set background colour
            GL.ClearColor(Color4.CornflowerBlue);
        }

        /// <summary>
        /// Paints new images onto the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void glControl_Paint(object sender, PaintEventArgs e)
        {
            // Clear the color and depth buffers
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Set polygon mode
            GL.PolygonMode(MaterialFace.Front, PolygonMode.Fill);

            // Rotate the cube using the calculated quaternion
            // In addition applying a rotation of -90 degrees in the z axis to rotate from the quaternion calculated in the Madgwick algorithm to the OpenGL axis frame
            Quaternion rotQuaternion =  Quaternion.FromEulerAngles(_rotZ * ((float)Math.PI / 180) , _rotY * ((float)Math.PI / 180), _rotX * ((float)Math.PI / 180)) * (Quaternion.Invert(_tareQuat) * _quaternion);
            Matrix4 modelViewMatrix = Matrix4.CreateScale(1f, 1f, 1f) * Matrix4.CreateFromQuaternion(rotQuaternion) *
                                      Matrix4.CreateTranslation(0f, 0f, 0f);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref modelViewMatrix);

            // Drawing the cuboid pointing forwards in the Madwick axis frame (rotated into OpenGL reference frame by rotQuaternion above)

            // +ve x face
            GL.BindTexture(TextureTarget.Texture2D, _texturesDict["Right.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(1, 0, 0); GL.TexCoord2(0, 0); GL.Vertex3(_halfDimensionX, -_halfDimensionY, -_halfDimensionZ);
            GL.Normal3(1, 0, 0); GL.TexCoord2(0, -1); GL.Vertex3(_halfDimensionX, -_halfDimensionY, _halfDimensionZ);
            GL.Normal3(1, 0, 0); GL.TexCoord2(1, -1); GL.Vertex3(_halfDimensionX, _halfDimensionY, _halfDimensionZ);
            GL.Normal3(1, 0, 0); GL.TexCoord2(1, 0); GL.Vertex3(_halfDimensionX, _halfDimensionY, -_halfDimensionZ);
            GL.End();

            // -ve x face
            GL.BindTexture(TextureTarget.Texture2D, _texturesDict["Left.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(-1, 0, 0); GL.TexCoord2(1, 0); GL.Vertex3(-_halfDimensionX, -_halfDimensionY, -_halfDimensionZ);
            GL.Normal3(-1, 0, 0); GL.TexCoord2(1, -1); GL.Vertex3(-_halfDimensionX, -_halfDimensionY, _halfDimensionZ);
            GL.Normal3(-1, 0, 0); GL.TexCoord2(0, -1); GL.Vertex3(-_halfDimensionX, _halfDimensionY, _halfDimensionZ);
            GL.Normal3(-1, 0, 0); GL.TexCoord2(0, 0); GL.Vertex3(-_halfDimensionX, _halfDimensionY, -_halfDimensionZ);
            GL.End();

            // +ve y face
            GL.BindTexture(TextureTarget.Texture2D, _texturesDict["Front.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(0, 1, 0); GL.TexCoord2(0, 0); GL.Vertex3(_halfDimensionX, _halfDimensionY, -_halfDimensionZ);
            GL.Normal3(0, 1, 0); GL.TexCoord2(0, -1); GL.Vertex3(_halfDimensionX, _halfDimensionY, _halfDimensionZ);
            GL.Normal3(0, 1, 0); GL.TexCoord2(1, -1); GL.Vertex3(-_halfDimensionX, _halfDimensionY, _halfDimensionZ);
            GL.Normal3(0, 1, 0); GL.TexCoord2(1, 0); GL.Vertex3(-_halfDimensionX, _halfDimensionY, -_halfDimensionZ);
            GL.End();

            // -ve y face
            GL.BindTexture(TextureTarget.Texture2D, _texturesDict["Back.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(0, -1, 0); GL.TexCoord2(1, 0); GL.Vertex3(_halfDimensionX, -_halfDimensionY, -_halfDimensionZ);
            GL.Normal3(0, -1, 0); GL.TexCoord2(1, -1); GL.Vertex3(_halfDimensionX, -_halfDimensionY, _halfDimensionZ);
            GL.Normal3(0, -1, 0); GL.TexCoord2(0, -1); GL.Vertex3(-_halfDimensionX, -_halfDimensionY, _halfDimensionZ);
            GL.Normal3(0, -1, 0); GL.TexCoord2(0, 0); GL.Vertex3(-_halfDimensionX, -_halfDimensionY, -_halfDimensionZ);
            GL.End();

            // +ve z face
            GL.BindTexture(TextureTarget.Texture2D, _texturesDict["Top.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(0, 0, 1); GL.TexCoord2(0, 0); GL.Vertex3(_halfDimensionX, -_halfDimensionY, _halfDimensionZ);
            GL.Normal3(0, 0, 1); GL.TexCoord2(0, -1); GL.Vertex3(-_halfDimensionX, -_halfDimensionY, _halfDimensionZ);
            GL.Normal3(0, 0, 1); GL.TexCoord2(1, -1); GL.Vertex3(-_halfDimensionX, _halfDimensionY, _halfDimensionZ);
            GL.Normal3(0, 0, 1); GL.TexCoord2(1, 0); GL.Vertex3(_halfDimensionX, _halfDimensionY, _halfDimensionZ);
            GL.End();

            // -ve z face
            GL.BindTexture(TextureTarget.Texture2D, _texturesDict["Bottom.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(0, 0, -1); GL.TexCoord2(0, 0); GL.Vertex3(-_halfDimensionX, -_halfDimensionY, -_halfDimensionZ);
            GL.Normal3(0, 0, -1); GL.TexCoord2(0, -1); GL.Vertex3(_halfDimensionX, -_halfDimensionY, -_halfDimensionZ);
            GL.Normal3(0, 0, -1); GL.TexCoord2(1, -1); GL.Vertex3(_halfDimensionX, _halfDimensionY, -_halfDimensionZ);
            GL.Normal3(0, 0, -1); GL.TexCoord2(1, 0); GL.Vertex3(-_halfDimensionX, _halfDimensionY, -_halfDimensionZ);
            
            GL.End();

            GlControl.SwapBuffers();
        }

        private void glControl_Resize(object sender, EventArgs e)
        {
            // Set the viewport 
            GL.Viewport(GlControl.ClientRectangle.X, GlControl.ClientRectangle.Y, GlControl.ClientRectangle.Width, GlControl.ClientRectangle.Height);

            // Create a field of view (fovy, aspect ratio, and z range)
            Matrix4 projection =
                Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 2, GlControl.Width / (float)GlControl.Height, 1.0f, 250.0f);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projection);

            // Adjust zoom in z-direction so that the cube does not completely fill the screen
            GL.Translate(0, 0, -10.0f);
        }

        private void rotLeft_click(object sender, EventArgs e)
        {
            _rotY += 10;
        }
        private void rotRight_click(object sender, EventArgs e)
        {
            _rotY -= 10;
        }

        private void tare_click(object sender, EventArgs e)
        {
            _rotY = 0;
            _tareQuat = _quaternion;
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Handles packet decoded event handler
        /// Passes the IMU data (if included in packet) and uses it to update the stored value of quaternion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnPacketDecoded(object sender, PacketDecodedEventArgs e)
        {
            Dictionary<string, int[]> dataDict = e.DecodedPacket;
            if (dataDict.ContainsKey("IMU"))
            {
                // MagZ inverted to match the axes frames of gyro and acc
                float gyrX = (dataDict["IMU"][0] - _calValues["GyrX"]);
                float gyrY = (dataDict["IMU"][1] - _calValues["GyrY"]);
                float gyrZ = (dataDict["IMU"][2] - _calValues["GyrZ"]);
                float magX = (dataDict["IMU"][3] - _calValues["MagX"]);
                float magY = (dataDict["IMU"][4] - _calValues["MagY"]);
                float magZ = (-(dataDict["IMU"][5] - _calValues["MagZ"]));
                float accX = (dataDict["IMU"][6] - _calValues["AccX"]);
                float accY = (dataDict["IMU"][7] - _calValues["AccY"]);
                float accZ = (dataDict["IMU"][8] - _calValues["AccZ"]);

                // Rotating from the sensor axis frame to the Madgwick expected axis frame (x and z axis effectively inverted)
                float[] mA = QuaternionCalc.RotationMatrixY(gyrX, gyrY, gyrZ, magX, magY, magZ, accX, accY, accZ, 180);

                // Rotating from the Madgwick axis to the OpenGL axis frame
                // These axis frames are effectively inverted since one is left handed (Madgwick) and one is right handed (OpenGL)
                // mA = QuaternionCalc.RotationMatrixX(mA[0], mA[1], mA[2], mA[3], mA[4], mA[5], mA[6], mA[7], mA[8], -90);

                _quaternion = _quaternionCalc.Quatupdate(mA[0], mA[1], mA[2], mA[3], mA[4], mA[5], mA[6], mA[7], mA[8]);
            }
        }

        #endregion
    }
}
