﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.WindowsForms;

namespace NU_GUI_Saver
{
    /// <summary>
    /// LivePlot is a class (inherits from form) which displays a plot of real time data
    /// collected from the bluetooth IMU devices
    /// </summary>

    public partial class LivePlot : Form
    {

        #region Variables

        /// <summary>
        /// Creates a new OxyPlot model
        /// </summary>
        private PlotModel myModel;

        /// <summary>
        /// The incoming packet data (collected from event in packetDecoder class object)
        /// </summary>
        private Dictionary<string, int[]> _dataDict;

        /// <summary>
        /// Line series of the plot which is updated in real time
        /// </summary>
        private LineSeries s1;

        private PlotView myPlot;

        #endregion

        /// <summary>
        /// Class Constructor
        /// </summary>
        public LivePlot()
        {
            InitializeComponent();
        }

        #region Methods

        public void CreatePlot()
        {
            //Create Plotview Object
            myPlot = new PlotView();

            //Create Plotmodel object and assign the plot model to the plot view
            myPlot.Model = new PlotModel { Title = "Simple Plot" };

            //Setting up the X and Y axis for the plot model
            LinearAxis xAxis = new LinearAxis {Position = AxisPosition.Bottom, Title = "Time"};
            LinearAxis yAxis = new LinearAxis {Position = AxisPosition.Left, Title = "Values"};
            xAxis.Zoom(0,100);
            yAxis.Zoom(0,1023);

            //Set up plot for display
            myPlot.Dock = System.Windows.Forms.DockStyle.Bottom;
            myPlot.Location = new System.Drawing.Point(0, 0);
            myPlot.Size = new System.Drawing.Size(500, 500);
            myPlot.TabIndex = 0;

            //Add plot control to form
            this.Controls.Add(myPlot);
            myPlot.Model.Background = OxyColor.FromRgb(255, 255, 255);
            myPlot.Model.TextColor = OxyColor.FromRgb(0, 0, 0);

            s1 = new LineSeries {Title = "LineSeries", StrokeThickness = 1};
            for (int i = 0; i <= 99; i++)
            {
                s1.Points.Add(new DataPoint(i, 0));
            }
            myPlot.Model.Series.Add(s1);

            myPlot.Model.Axes.Add(xAxis);
            myPlot.Model.Axes.Add(yAxis);
        }

        public void PlotUpdate()
        {
            while(true)
            {
                if (_dataDict.ContainsKey("ADC"))
                {
                    s1.Points.Add(new DataPoint(99, _dataDict["ADC"][0]));
                    for (int i = 0; i <= 99; i++)
                    {
                        s1.Points[i] = new DataPoint(s1.Points[i].X - 1, s1.Points[i].Y);
                    }

                    s1.Points.RemoveAt(0);
                    myPlot.InvalidatePlot(true);

                    Application.DoEvents();
                }
            }
        }

        #endregion

        public void OnPacketDecoded(object sender, PacketDecodedEventArgs e)
        {
            _dataDict = e.DecodedPacket;
            PlotUpdate();
        }
    }
}
