﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NU_GUI_Saver.Simulation;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
//using System.Windows.Media.Media3D;

namespace NU_GUI_Saver
{
    public partial class Cube3D : Form
    {
        #region Variables

        /// <summary>
        /// Form Update Timer
        /// </summary>
        private Timer formUpdateTimer;

        /// <summary>
        /// Array of image files paths to be used as textures
        /// </summary>
        private string[] imageFiles;

        /// <summary>
        /// Main image directory to look for the image files
        /// </summary>
        private string imageDir;

        /// <summary>
        /// Dict of the reference number for all the textures assigned to
        /// </summary>
        private Dictionary<string, int> textures;

        /// <summary>
        /// Defined half x,y,z dimensions of the cube
        /// </summary>
        private float halfXdimension, halfYdimension, halfZdimension;

        /// <summary>
        /// Object instance contains the quaternion update function for continuously updating quaternion every time packet received
        /// </summary>
        private QuaternionCalc quaternionCalc;

        /// <summary>
        /// Updated quaternion returned from the quaternion update function
        /// </summary>
        private Quaternion quaternion = new Quaternion(0f, 0f, 0f, 1f);

        /// <summary>
        /// Rotation matrix derived from the quaternion in order to rotate the cube
        /// </summary>
        private float[] rotationMatrix;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor that calls default constructor with default dimensions and image directory
        /// </summary>
        public Cube3D(QuaternionCalc quaternionCalc)
            : this(quaternionCalc, new float[] { 4f, 6f, 2f }, @"D:\Box Sync\IP 10-05-17\SC- PhD\IMU Saver\NU GUI Saver\Simulation\Textures\")
        {
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="dimensions"></param>
        /// <param name="imageDir"></param>
        public Cube3D(QuaternionCalc quaternionCalc, float[] dimensions, string imageDir)
        {
            // Initialise the GL window
            InitializeComponent();

            // quaternion calculation class object passed from the main GUI
            this.quaternionCalc = quaternionCalc;

            // Set the image directory
            this.imageDir = imageDir;

            // Dimensions of the cuboid
            halfXdimension = dimensions[0] / 2;
            halfYdimension = dimensions[1] / 2;
            halfZdimension = dimensions[2] / 2;

            // Create a new timer event to refresh the cube
            formUpdateTimer = new Timer();
            formUpdateTimer.Interval = 20;
            formUpdateTimer.Tick += new EventHandler(formUpdateTimer_Tick);
            formUpdateTimer.Start();
        }

        #endregion

        #region Methods

        private void glControl1_Load(object sender, EventArgs e)
        {
            // Enable depth testing and set to draw object if it is less than or equal to any already drawn objects
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);

            // Enable textures
            GL.Enable(EnableCap.Texture2D);

            // Image smoothing
            GL.ShadeModel(ShadingModel.Smooth);
            GL.Enable(EnableCap.LineSmooth);

            // Set background colour
            GL.ClearColor(Color4.CornflowerBlue);

            // Load all the textures for each side of the cube
            imageFiles = Directory.GetFiles(imageDir, "*.PNG");
            textures = LoadTexturesFromImages(imageFiles);
            bool bool2 = true;
        }

        /// <summary>
        /// Functions takes list of filenames of png files and returns a dictionary of Text2D objects which contain
        /// the id of the GL texture as well as its width, height, and filename
        /// </summary>
        /// <param name="fileNames"></param>
        /// <returns></returns>
        private Dictionary<string, int> LoadTexturesFromImages(string[] fileNames)
        {
            int numPics = fileNames.Length;
            //Texture2D[] textureArr = new Texture2D[numPics];
            Dictionary<string, int> texturesDict = new Dictionary<string, int>();


            for (int im = 0; im < numPics; im++)
            {
                //textureArr[im] = ContentPipe.LoadTexture(fileNames[im]);
                string file = Path.GetFileName(fileNames[im]);
                texturesDict.Add(file, ContentPipe.LoadTexture(fileNames[im]));
            }

            //return textureArr.ToDictionary(q => q.FileName, q => q);
            return texturesDict;
        }

        /// <summary>
        /// Timer tick event to refresh graphics.
        /// </summary>
        private void formUpdateTimer_Tick(object sender, EventArgs e)
        {
            lock (this)
            {
                glControl1.Refresh();
            }
        }

        /// <summary>
        /// Paints new images onto the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            // Clear the color and depth buffers
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            
            // Set polygon mode
            GL.PolygonMode(MaterialFace.Front, PolygonMode.Fill);

            //Matrix4 Rotation = Matrix4.CreateRotationX(0) * Matrix4.CreateRotationY(0) * Matrix4.CreateRotationZ(0);
            //Matrix4 modelViewMatrix = Matrix4.CreateScale(1f, 1f, 1f) * Rotation *
            //                          Matrix4.CreateTranslation(0f, 0f, 0f);
            // Rotate the cube using the calculated quaternion
            Matrix4 modelViewMatrix = Matrix4.CreateScale(1f, 1f, 1f) * Matrix4.CreateFromQuaternion(quaternion) *
                                      Matrix4.CreateTranslation(0f, 0f, 0f);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref modelViewMatrix);

            // Apply a rotation to the cube
            //GL.Rotate(1f, 1, 0, 0);
            //GL.Rotate(1f, 0, 1, 0);

            // +ve x face
            GL.BindTexture(TextureTarget.Texture2D, textures["Back.png"]);
            GL.Begin(PrimitiveType.Quads);
            //GL.Normal3(1, 0, 0); GL.TexCoord2(0, 0); GL.Vertex3(halfXdimension, -halfYdimension, -halfZdimension);
            //GL.Normal3(1, 0, 0); GL.TexCoord2(0, 1); GL.Vertex3(halfXdimension, -halfYdimension, halfZdimension);
            //GL.Normal3(1, 0, 0); GL.TexCoord2(1, 1); GL.Vertex3(halfXdimension, halfYdimension, halfZdimension);
            //GL.Normal3(1, 0, 0); GL.TexCoord2(1, 0); GL.Vertex3(halfXdimension, halfYdimension, -halfZdimension);
            GL.Normal3(1, 0, 0); GL.TexCoord2(0, 0); GL.Vertex3(halfXdimension, halfYdimension, -halfZdimension);
            GL.Normal3(1, 0, 0); GL.TexCoord2(0, 1); GL.Vertex3(halfXdimension, halfYdimension, halfZdimension);
            GL.Normal3(1, 0, 0); GL.TexCoord2(1, 1); GL.Vertex3(halfXdimension, -halfYdimension, halfZdimension);
            GL.Normal3(1, 0, 0); GL.TexCoord2(1, 0); GL.Vertex3(halfXdimension, -halfYdimension, -halfZdimension);
            GL.End();

            // -ve x face
            GL.BindTexture(TextureTarget.Texture2D, textures["Front.png"]);
            GL.Begin(PrimitiveType.Quads);
            //GL.Normal3(-1, 0, 0); GL.TexCoord2(1, 0); GL.Vertex3(-halfXdimension, -halfYdimension, -halfZdimension);
            //GL.Normal3(-1, 0, 0); GL.TexCoord2(1, 1); GL.Vertex3(-halfXdimension, -halfYdimension, halfZdimension);
            //GL.Normal3(-1, 0, 0); GL.TexCoord2(0, 1); GL.Vertex3(-halfXdimension, halfYdimension, halfZdimension);
            //GL.Normal3(-1, 0, 0); GL.TexCoord2(0, 0); GL.Vertex3(-halfXdimension, halfYdimension, -halfZdimension);
            GL.Normal3(-1, 0, 0); GL.TexCoord2(0, 0); GL.Vertex3(-halfXdimension, -halfYdimension, -halfZdimension);
            GL.Normal3(-1, 0, 0); GL.TexCoord2(0, 1); GL.Vertex3(-halfXdimension, -halfYdimension, halfZdimension);
            GL.Normal3(-1, 0, 0); GL.TexCoord2(1, 1); GL.Vertex3(-halfXdimension, halfYdimension, halfZdimension);
            GL.Normal3(-1, 0, 0); GL.TexCoord2(1, 0); GL.Vertex3(-halfXdimension, halfYdimension, -halfZdimension);
            GL.End();

            // +ve y face
            GL.BindTexture(TextureTarget.Texture2D, textures["Right.png"]);
            GL.Begin(PrimitiveType.Quads);
            //GL.Normal3(0, 1, 0); GL.TexCoord2(1, 0); GL.Vertex3(-halfXdimension, halfYdimension, -halfZdimension);
            //GL.Normal3(0, 1, 0); GL.TexCoord2(1, 1); GL.Vertex3(-halfXdimension, halfYdimension, halfZdimension);
            //GL.Normal3(0, 1, 0); GL.TexCoord2(0, 1); GL.Vertex3(halfXdimension, halfYdimension, halfZdimension);
            //GL.Normal3(0, 1, 0); GL.TexCoord2(0, 0); GL.Vertex3(halfXdimension, halfYdimension, -halfZdimension);
            GL.Normal3(0, 1, 0); GL.TexCoord2(0, 0); GL.Vertex3(-halfXdimension, halfYdimension, -halfZdimension);
            GL.Normal3(0, 1, 0); GL.TexCoord2(0, 1); GL.Vertex3(-halfXdimension, halfYdimension, halfZdimension);
            GL.Normal3(0, 1, 0); GL.TexCoord2(1, 1); GL.Vertex3(halfXdimension, halfYdimension, halfZdimension);
            GL.Normal3(0, 1, 0); GL.TexCoord2(1, 0); GL.Vertex3(halfXdimension, halfYdimension, -halfZdimension);
            GL.End();

            // -ve y face
            GL.BindTexture(TextureTarget.Texture2D, textures["Left.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(0, -1, 0); GL.TexCoord2(0, 0); GL.Vertex3(-halfXdimension, -halfYdimension, -halfZdimension);
            GL.Normal3(0, -1, 0); GL.TexCoord2(0, 1); GL.Vertex3(-halfXdimension, -halfYdimension, halfZdimension);
            GL.Normal3(0, -1, 0); GL.TexCoord2(1, 1); GL.Vertex3(halfXdimension, -halfYdimension, halfZdimension);
            GL.Normal3(0, -1, 0); GL.TexCoord2(1, 0); GL.Vertex3(halfXdimension, -halfYdimension, -halfZdimension);
            GL.End();

            // +ve z face
            GL.BindTexture(TextureTarget.Texture2D, textures["Top.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(0, 0, 1); GL.TexCoord2(0, 0); GL.Vertex3(-halfXdimension, -halfYdimension, halfZdimension);
            GL.Normal3(0, 0, 1); GL.TexCoord2(0, 1); GL.Vertex3(halfXdimension, -halfYdimension, halfZdimension);
            GL.Normal3(0, 0, 1); GL.TexCoord2(1, 1); GL.Vertex3(halfXdimension, halfYdimension, halfZdimension);
            GL.Normal3(0, 0, 1); GL.TexCoord2(1, 0); GL.Vertex3(-halfXdimension, halfYdimension, halfZdimension);
            GL.End();

            // -ve z face
            GL.BindTexture(TextureTarget.Texture2D, textures["Bottom.png"]);
            GL.Begin(PrimitiveType.Quads);
            GL.Normal3(0, 0, -1); GL.TexCoord2(0, 0); GL.Vertex3(halfXdimension, -halfYdimension, -halfZdimension);
            GL.Normal3(0, 0, -1); GL.TexCoord2(0, 1); GL.Vertex3(-halfXdimension, -halfYdimension, -halfZdimension);
            GL.Normal3(0, 0, -1); GL.TexCoord2(1, 1); GL.Vertex3(-halfXdimension, halfYdimension, -halfZdimension);
            GL.Normal3(0, 0, -1); GL.TexCoord2(1, 0); GL.Vertex3(halfXdimension, halfYdimension, -halfZdimension);
            GL.End();

            glControl1.SwapBuffers();
        }

        private void glControl1_Resize(object sender, EventArgs e)
        {
            // Set the viewport 
            GL.Viewport(ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width, ClientRectangle.Height);

            // Create a field of view (fovy, aspect ratio, and z range)
            Matrix4 projection =
                Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 2, Width / (float)Height, 1.0f, 250.0f);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projection);

            // Adjust zoom in z-direction so that the cube does not completely fill the screen
            GL.Translate(0, 0, -10.0f);
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Handles packet decoded event handler
        /// Passes the IMU data (if included in packet) and uses it to update the stored value of quaternion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnPacketDecoded(object sender, PacketDecodedEventArgs e)
        {
            Dictionary<string, int[]> _dataDict = e.DecodedPacket;
            if (_dataDict.ContainsKey("IMU"))
            {
                // Data passed to Quatupdate in form GXYZ, MXYZ, AXYZ
                quaternion = quaternionCalc.Quatupdate(_dataDict["IMU"][0], _dataDict["IMU"][1], _dataDict["IMU"][2],
                    _dataDict["IMU"][3],
                    _dataDict["IMU"][4], _dataDict["IMU"][5], _dataDict["IMU"][6], _dataDict["IMU"][7],
                    _dataDict["IMU"][8]);
                bool temp = true;
            }
        }

        #endregion
    }
}
