﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{

    /// <summary>
    /// 2-D texture class for managing textures used in OpenTK
    /// </summary>

    class Texture2D
    {
        public Texture2D(int id, string fileName)
        {
            this.id = id;
            this.fileName = fileName;
        }

        private int id;
        private string fileName;
        private int width, height;

        public int ID { get { return id; } }
        public string FileName { get { return fileName; } }
    }
}
