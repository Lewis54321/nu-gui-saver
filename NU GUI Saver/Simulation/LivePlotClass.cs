﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.WindowsForms;

namespace NU_GUI_Saver.Simulation
{
    class LivePlotClass
    {
        /// <summary>
        /// The incoming packet data (collected from event in packetDecoder class object)
        /// </summary>
        private Dictionary<string, int[]> _dataDict;

        /// <summary>
        /// Manual reset event used for blocking the background thread while waiting for packets to be receieved
        /// and releasing the thread when packet avaialable for processing
        /// </summary>
        private readonly ManualResetEvent _mre = new ManualResetEvent(false);

        /// <summary>
        /// Window panel passed to the class from the main GUI thread, onto which the line plot will be painted
        /// </summary>
        private readonly Panel _windowPanel;

        /// <summary>
        /// Metric Type represents the specific metric type being recorded (ADC or Acc or Gyro or Mag)
        /// </summary>
        private readonly string _metricType;

        /// <summary>
        /// Metric Base represents the base type of metric (ADC or IMU)
        /// </summary>
        private string _metricBase;

        /// <summary>
        /// Packet Index represents the index within the packet where the relevant data is stored for the different modalities 
        /// </summary>
        private int _packetIdx;

        /// <summary>
        /// Delegate associated with the method to invoke the control with
        /// </summary>
        /// <param name="linePlot"></param>
        private delegate void InvokeDelegate(PlotView linePlot);

        /// <summary>
        /// Variable used to determine whether the user wants to continue streaming or stop to kill the thread
        /// </summary>
        public bool Streaming = true;

        /// <summary>
        /// Lock used to prevent multiple packetDecoder class instances from accessing csv file simultaneously
        /// </summary>
        private readonly Object _thisLock = new Object();

        public PlotView LinePlot;

        private int[] _yaxis;

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="windowPanel"></param>
        public LivePlotClass(Panel windowPanel, string metricType)
        {
            _windowPanel = windowPanel;
            _metricType = metricType;

            ConfigurePlotSettings(metricType);
        }

        private void ConfigurePlotSettings(string metricLabel)
        {
            switch (metricLabel)
            {
                case "ADC":
                    _metricBase = "ADC";
                    _packetIdx = 0;
                    _yaxis = new int[] {0, 1023};
                    break;
                case "Gyr":
                    _metricBase = "IMU";
                    _packetIdx = 0;
                    _yaxis = new int[] { -32768, 32767 };
                    break;
                case "Mag":
                    _metricBase = "IMU";
                    _packetIdx = 3;
                    _yaxis = new int[] { -32768, 32767 };
                    break;
                case "Acc":
                    _metricBase = "IMU";
                    _packetIdx = 6;
                    _yaxis = new int[] { -32768, 32767 };
                    break;
            }
        }

        public void GenPlot()
        {
            LinePlot = new PlotView();

            //Create Plotmodel object and assign the plot model to the plot view
            LinePlot.Model = new PlotModel { Title = "Plot_" + _metricType };

            //Setting up the X and Y axis for the plot model
            LinearAxis xAxis = new LinearAxis { Position = AxisPosition.Bottom, Title = "Time" };
            LinearAxis yAxis = new LinearAxis { Position = AxisPosition.Left, Title = "Values" };
            xAxis.Zoom(0, 500);
            yAxis.Zoom(_yaxis[0], _yaxis[1]);

            //Set up plot for display
            LinePlot.Dock = System.Windows.Forms.DockStyle.Fill;
            LinePlot.Location = new System.Drawing.Point(0, 0);
            LinePlot.TabIndex = 0;
            var margin = LinePlot.Margin;
            margin.Right = 30;
            LinePlot.Margin = margin;

            //Add plot control to form
            _windowPanel.Invoke(new InvokeDelegate(AddControl), LinePlot);
            LinePlot.Model.Background = OxyColor.FromRgb(255, 255, 255);
            LinePlot.Model.TextColor = OxyColor.FromRgb(0, 0, 0);

            // Creates three line series to be able to display up to 3 ADC pins or all axes of the acc/gyro/mag
            List<LineSeries> listLineSeries = new List<LineSeries>();
            for (int seriesNum = 1; seriesNum <= 8; seriesNum++)
            {
                LineSeries series = new LineSeries { Title = "Series_" + seriesNum, StrokeThickness = 1 };
                for (int pointNum = 0; pointNum <= 499; pointNum++)
                {
                    series.Points.Add(new DataPoint(pointNum, 0));
                }
                LinePlot.Model.Series.Add(series);
                listLineSeries.Add(series);
            }

            LinePlot.Model.Axes.Add(xAxis);
            LinePlot.Model.Axes.Add(yAxis);

            PlotUpdate(listLineSeries, LinePlot);
        }

        /// <summary>
        /// Method for updating the current plot
        /// Updating the plot each time a decoded packet has arrived, restarting the background thread
        /// </summary>
        /// <param name="listLineSeries"></param>
        /// <param name="linePlot"></param>
        private void PlotUpdate(List<LineSeries> listLineSeries, PlotView linePlot)
        {
            while (Streaming)
            {
                _mre.WaitOne();
                _mre.Reset();
                Update(_dataDict, listLineSeries, linePlot);
            }
        }

        /// <summary>
        /// Pass Data Dict into this new method as a local variable to prevent the case of it being overwritten mid-way through processing
        /// by a new packet arriving and therefore causing an error
        /// </summary>
        /// <param name="dataDict"></param>
        /// <param name="listLineSeries"></param>
        /// <param name="linePlot"></param>
        private void Update(Dictionary<string, int[]> dataDict, List<LineSeries> listLineSeries, PlotView linePlot)
        {
            if (!dataDict.ContainsKey(_metricBase))
            {
                return;
            }

            var count = _packetIdx;
            int numPlots;
            if (_metricBase == "ADC")
            {
                numPlots = 8;
            }
            else
            {
                numPlots = 3;
            }

            for (int seriesNum = 0; seriesNum < numPlots; seriesNum++)
            {
                listLineSeries[seriesNum].Points.Add(new DataPoint(499, dataDict[_metricBase][count]));
                for (int i = 0; i <= 499; i++)
                {
                    listLineSeries[seriesNum].Points[i] = new DataPoint(listLineSeries[seriesNum].Points[i].X - 1, listLineSeries[seriesNum].Points[i].Y);
                }
                listLineSeries[seriesNum].Points.RemoveAt(0);
                count++;
            }

            linePlot.InvalidatePlot(true);
            Application.DoEvents();
        }

        /// <summary>
        /// Method assinged to a delegate to be invoked by the panel since it is on the GUI thread 
        /// </summary>
        /// <param name="linePlot"></param>
        private void AddControl(PlotView linePlot)
        {
            _windowPanel.Controls.Add(linePlot);
        }

        /// <summary>
        /// Packet decoded event handler called every time a new packet has been decoded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnPacketDecoded(object sender, PacketDecodedEventArgs e)
        {
             _dataDict = e.DecodedPacket;
            _mre.Set();
        }
    }
}
