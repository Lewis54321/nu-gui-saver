﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenTK;

namespace NU_GUI_Saver.Simulation
{
    public class QuaternionCalc
    {
        #region Variables

        /// <summary>
        /// Scaling factor used when performing gradient descent in the Madgwick algorithm.
        /// </summary>
        private readonly float _stepSize;

        /// <summary>
        /// The processed quaternion produced from the latest raw data input.
        /// </summary>
        private Quaternion _procQuat = new Quaternion(0, 0, 0, 1);

        /// <summary>
        /// Array of the calibration values for the connected device
        /// </summary>
        private readonly Dictionary<string, Int16> _calValues;

        #endregion

        #region Constructors

        /// <summary>
        /// Test Constructor
        /// </summary>
        public QuaternionCalc()
        {
            this._stepSize = 0.01f;
            this._calValues = new Dictionary<string, Int16>()
            {
                {"GyrX", 0},{"GyrY", 0},{"GyrZ", 0},{"AccX", 0},{"AccY", 0},{"AccZ", 0},{"MagX", 0},{"MagY", 0},{"MagZ", 0}
            };
        }

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="calValues"></param>
        /// <param name="inertialFreq"></param>
        public QuaternionCalc(Dictionary<string, Int16> calValues, int inertialFreq)
        {
            this._stepSize = 1f / inertialFreq;
            this._calValues = calValues;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Implementation of the Madgwick algorithm, using IMU data to update orientation Quaternion.
        /// </summary>
        /// <param name="gx">Gyro X value (will be converted from float to rad/sec)</param>
        /// <param name="gy">Gyro Y value (will be converted from float to rad/sec)</param>
        /// <param name="gz">Gyro Z value (will be converted from float to rad/sec)</param>
        /// <param name="ax">Accel X value (normalised so magnitude not important)</param>
        /// <param name="ay">Accel Y value (normalised so magnitude not important)</param>
        /// <param name="az">Accel Z value (normalised so magnitude not important)</param>
        /// <param name="mx">Mag X value (normalised so magnitude not important)</param>
        /// <param name="my">Mag Y value (normalised so magnitude not important)</param>
        /// <param name="mz">Mag Z value (normalised so magnitude not important)</param>
        public Quaternion Quatupdate(float gx, float gy, float gz, float mx, float my, float mz, float ax, float ay, float az)
        {
            // Converting the gyro data to rads per sec
            gx = (gx * 500f * (float)Math.PI) / (32758f * 180f);
            gy = (gy * 500f * (float)Math.PI) / (32758f * 180f);
            gz = (gz * 500f * (float)Math.PI) / (32758f * 180f);

            // Setting up some of the quaternion variables
            float q1 = (float)_procQuat.W, q2 = (float)_procQuat.X, q3 = (float)_procQuat.Y, q4 = (float)_procQuat.Z;   // short name local variable for readability
            float norm;
            float hx, hy, _2bx, _2bz, _8bx, _8bz;
            float s1, s2, s3, s4;
            float qDot1, qDot2, qDot3, qDot4;

            // Auxiliary variables to avoid repeated arithmetic
            float _2q1mx;
            float _2q1my;
            float _2q1mz;
            float _2q2mx;
            float _4bx;
            float _4bz;
            float _2q1 = 2f * q1;
            float _2q2 = 2f * q2;
            float _2q3 = 2f * q3;
            float _2q4 = 2f * q4;
            float _2q1q3 = 2f * q1 * q3;
            float _2q3q4 = 2f * q3 * q4;
            float q1q1 = q1 * q1;
            float q1q2 = q1 * q2;
            float q1q3 = q1 * q3;
            float q1q4 = q1 * q4;
            float q2q2 = q2 * q2;
            float q2q3 = q2 * q3;
            float q2q4 = q2 * q4;
            float q3q3 = q3 * q3;
            float q3q4 = q3 * q4;
            float q4q4 = q4 * q4;

            // Normalise accelerometer measurement
            norm = (float)Math.Sqrt(ax * ax + ay * ay + az * az);
            if (norm == 0f) throw new InvalidOperationException(); // handle NaN
            norm = 1 / norm;        // use reciprocal for division
            ax *= norm;
            ay *= norm;
            az *= norm;

            // Normalise magnetometer measurement
            norm = (float)Math.Sqrt(mx * mx + my * my + mz * mz);
            if (norm == 0f) throw new InvalidOperationException(); // handle NaN
            norm = 1 / norm;        // use reciprocal for division
            mx *= norm;
            my *= norm;
            mz *= norm;

            // Reference direction of Earth's magnetic field
            _2q1mx = 2f * q1 * mx;
            _2q1my = 2f * q1 * my;
            _2q1mz = 2f * q1 * mz;
            _2q2mx = 2f * q2 * mx;
            hx = mx * q1q1 - _2q1my * q4 + _2q1mz * q3 + mx * q2q2 + _2q2 * my * q3 + _2q2 * mz * q4 - mx * q3q3 - mx * q4q4;
            hy = _2q1mx * q4 + my * q1q1 - _2q1mz * q2 + _2q2mx * q3 - my * q2q2 + my * q3q3 + _2q3 * mz * q4 - my * q4q4;
            _2bx = (float)Math.Sqrt(hx * hx + hy * hy);
            _2bz = -_2q1mx * q3 + _2q1my * q2 + mz * q1q1 + _2q2mx * q4 - mz * q2q2 + _2q3 * my * q4 - mz * q3q3 + mz * q4q4;
            _4bx = 2f * _2bx;
            _4bz = 2f * _2bz;
            _8bx = 2f * _4bx;
            _8bz = 2f * _4bz;
            // Gradient decent algorithm corrective step
            s1 = -_2q3 * (2f * q2q4 - _2q1q3 - ax) + _2q2 * (2f * q1q2 + _2q3q4 - ay) - _2bz * q3 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q4 + _2bz * q2) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q3 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
            s2 = _2q4 * (2f * q2q4 - _2q1q3 - ax) + _2q1 * (2f * q1q2 + _2q3q4 - ay) - 4f * q2 * (1 - 2f * q2q2 - 2f * q3q3 - az) + _2bz * q4 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q3 + _2bz * q1) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q4 - _4bz * q2) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
            s3 = -_2q1 * (2f * q2q4 - _2q1q3 - ax) + _2q4 * (2f * q1q2 + _2q3q4 - ay) - 4f * q3 * (1 - 2f * q2q2 - 2f * q3q3 - az) + (-_4bx * q3 - _2bz * q1) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q2 + _2bz * q4) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q1 - _4bz * q3) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
            s4 = _2q2 * (2f * q2q4 - _2q1q3 - ax) + _2q3 * (2f * q1q2 + _2q3q4 - ay) + (-_4bx * q4 + _2bz * q2) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q1 + _2bz * q3) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q2 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
            norm = 1f / (float)Math.Sqrt(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);    // normalise step magnitude
            s1 *= norm;
            s2 *= norm;
            s3 *= norm;
            s4 *= norm;

            // Compute rate of change of quaternion
            qDot1 = 0.5f * (-q2 * gx - q3 * gy - q4 * gz) - 0.5f * s1;
            qDot2 = 0.5f * (q1 * gx + q3 * gz - q4 * gy) - 0.5f * s2;
            qDot3 = 0.5f * (q1 * gy - q2 * gz + q4 * gx) - 0.5f * s3;
            qDot4 = 0.5f * (q1 * gz + q2 * gy - q3 * gx) - 0.5f * s4;

            // Integrate to yield quaternion
            q1 += qDot1 * _stepSize;
            q2 += qDot2 * _stepSize;
            q3 += qDot3 * _stepSize;
            q4 += qDot4 * _stepSize;
            norm = 1f / (float)Math.Sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalise quaternion

            // Quaternion values are supplied to the Media3D in format x y z w
            _procQuat = new Quaternion(q2 * norm, q3 * norm, q4 * norm, q1 * norm);
            return _procQuat;
        }

        /// <summary>
        /// Converts data to rotation matrix.
        /// </summary>
        /// <remarks>
        /// Index order is row major. See http://en.wikipedia.org/wiki/Row-major_order
        /// </remarks> 
        public double[] QuatToRotationMatrix()
        {
            double r11 = 2 * _procQuat.W * _procQuat.W - 1 + 2 * _procQuat.X * _procQuat.X;
            double r12 = 2 * (_procQuat.X * _procQuat.Y + _procQuat.W * _procQuat.Z);
            double r13 = 2 * (_procQuat.X * _procQuat.Z - _procQuat.W * _procQuat.Y);
            double r21 = 2 * (_procQuat.X * _procQuat.Y - _procQuat.W * _procQuat.Z);
            double r22 = 2 * _procQuat.W * _procQuat.W - 1 + 2 * _procQuat.Y * _procQuat.Y;
            double r23 = 2 * (_procQuat.Y * _procQuat.Z + _procQuat.W * _procQuat.X);
            double r31 = 2 * (_procQuat.X * _procQuat.Z + _procQuat.W * _procQuat.Y);
            double r32 = 2 * (_procQuat.Y * _procQuat.Z - _procQuat.W * _procQuat.X);
            double r33 = 2 * _procQuat.W * _procQuat.W - 1 + 2 * _procQuat.Z * _procQuat.Z;

            // Return rotation matrix
            return new double[] { r11, r12, r13,
                r21, r22, r23,
                r31, r32, r33 };
        }

        /// <summary>
        /// Convert single quaternion to it's euler angle representation
        /// </summary>
        /// <param name="quat"></param>
        /// <returns></returns>
        public static Vector3 QuatToEulerAngles(Quaternion quat)
        {
            //quat = quat.Inverted();
            double sqw = quat.W * quat.W;
            double sqx = quat.X * quat.X;
            double sqy = quat.Y * quat.Y;
            double sqz = quat.Z * quat.Z;

            Vector3 pitchYawRoll;

            //pitchYawRoll.X = (float)Math.Atan2(2f * (quat.X * quat.W + quat.Y * quat.Z), 1 - 2f * (sqx + sqy)) * (180f / (float)Math.PI); // Roll
            pitchYawRoll.X = (float) Math.Atan2(2f * (quat.Y * quat.Z + quat.W * quat.X), (sqw - sqx - sqy - sqz)) * (180f / (float)Math.PI) + 180; // Roll
            pitchYawRoll.Y = (float)Math.Asin(2f * (quat.X * quat.Z - quat.W * quat.Y)) * (180f / (float)Math.PI) + 180;     // Pitch
            pitchYawRoll.Z = (float)Math.Atan2(2f * (quat.X * quat.Y + quat.Z * quat.W), 1 - 2f * (sqy + sqz)) * (180f / (float)Math.PI) + 180; // Yaw

            return pitchYawRoll;
        }

        /// <summary>
        /// Convert single quaternion to it's euler angle representation (order of operations chosen to prioritise axis of interest)
        /// </summary>
        /// <param name="quat"></param>
        /// <returns></returns>
        public static Vector3 QuatToAngles(Quaternion quat)
        {
            Vector3 pitchYawRoll;

            // 1-2-3
            Vector3 rotatedZ = quat * new Vector3(0f, 0f, 1f);
            pitchYawRoll.X = (float)Math.Atan2(-rotatedZ[1], rotatedZ[2]) * (180f / (float)Math.PI) + 180;

            // 2-3-1
            Vector3 rotatedX = quat * new Vector3(1f, 0f, 0f);
            pitchYawRoll.Y = (float)Math.Atan2(-rotatedX[2], rotatedX[0]) * (180f / (float)Math.PI) + 180;

            // 3-1-2
            Vector3 rotatedY = quat * new Vector3(0f, 1f, 0f);
            pitchYawRoll.Z = (float)Math.Atan2(-rotatedY[0], rotatedY[1]) * (180f / (float)Math.PI) + 180;

            return pitchYawRoll;
        }

        /// <summary>
        /// Decompose quaternion into its twist and swing quaternion representation
        /// </summary>
        /// <param name="quat"></param>
        /// <param name="twistAxis"></param>
        /// <returns></returns>
        public static Quaternion DecomposeTwistSwing(Quaternion quat, Vector3 twistAxis)
        {
            Vector3 r = new Vector3(quat.X, quat.Y, quat.Z);
            Quaternion swing;
            Quaternion twist;
            
            // Singularity: rotation by 180 degrees
            if (r.LengthSquared < Double.Epsilon)
            {
                Vector3 rotatedTwistAxis = quat * twistAxis;
                Vector3 swingAxis = Vector3.Cross(twistAxis, rotatedTwistAxis);

                if (swingAxis.LengthSquared > double.Epsilon)
                {
                    float swingAngle = Vector3.CalculateAngle(twistAxis, rotatedTwistAxis);
                    swing = Quaternion.FromAxisAngle(swingAxis, swingAngle);
                }
                else
                {
                    swing = Quaternion.Identity;
                }
                twist = Quaternion.FromAxisAngle(twistAxis, 180.0f);
            }
            else
            {
                // Project the axis of the quaternion onto the twist axis
                Vector3 p = (r * (twistAxis / twistAxis.Length)) * (twistAxis / twistAxis.Length);
                twist = new Quaternion(p.X, p.Y, p.Z, quat.W);
                twist = Quaternion.Normalize(twist);
                swing = quat * Quaternion.Invert(twist);
            }
            return twist;
        }

        /// <summary>
        /// Decompose quaternion into its swing and twist quaternion representation
        /// </summary>
        /// <param name="quat"></param>
        /// <param name="twistAxis"></param>
        /// <returns></returns>
        public static Quaternion DecomposeSwingTwist(Quaternion quat, Vector3 twistAxis)
        {
            // Finding the shortest arc quaternion between two vectors (the swing)
            Vector3 diffVec = CrossProduct(twistAxis, quat * twistAxis);
            float mag = DotProduct(twistAxis, quat * twistAxis);
            Quaternion swing = new Quaternion(diffVec.X, diffVec.Y, diffVec.Z, 1 + mag);
            swing = Quaternion.Normalize(swing);
            // Finding the respective twist between the two vectors
            Quaternion twist = Quaternion.Invert(swing) * quat;
            Quaternion test = swing * twist;
            return twist;
        }

        /// <summary>
        /// Calculates the magnitude of angle (mimumum angle rotation around any axis) between two vectors
        /// </summary>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public static double VectorToAngleCrossMag(Vector3 vec1, Vector3 vec2)
        {
            var crossProd = CrossProduct(vec1, vec2);
            var dotProd = DotProduct(vec1, vec2);
            return Math.Atan2(Norm(CrossProduct(vec1, vec2)), DotProduct(vec1, vec2)) * (180f / (float)Math.PI);
        }

        /// <summary>
        /// Same as method above except calculates the magnitude of angle in individual 2D planes (subject to errors when vector points out of plane)
        /// </summary>
        /// <param name="quat1"></param>
        /// <param name="quat2"></param>
        /// <returns></returns>
        public static Vector3 VectorToAngleCross(Quaternion quat1, Quaternion quat2)
        {
            // Calculating rotated vectors which are all orthogonal to each other (in Madgwick space)
            var vecForward1 = quat1 * new Vector3(0, 1f, 0);
            var vecForward2 = quat2 * new Vector3(0, 1f, 0);
            var vecUp1 = quat1 * new Vector3(0f, 0f, 1f);
            var vecUp2 = quat2 * new Vector3(0f, 0f, 1f);
            var vecRight1 = quat1.Inverted() * new Vector3(1f, 0f, 0);
            var vecRight2 = quat2.Inverted() * new Vector3(1f, 0f, 0);

            // Projecting the 3D vector to 2D plane and calculating the angle in this plane
            Vector3 vec;
            vec.X = (float)Math.Atan2(CrossProduct(new Vector2(vecForward1.Y, vecForward1.Z), new Vector2(vecForward2.Y, vecForward2.Z)), DotProduct(new Vector2(vecForward1.Y, vecForward1.Z), new Vector2(vecForward2.Y, vecForward2.Z))) * (180f / (float)Math.PI);
            vec.Y = (float)Math.Atan2(CrossProduct(new Vector2(vecRight1.X, vecRight1.Z), new Vector2(vecRight2.X, vecRight2.Z)), DotProduct(new Vector2(vecRight1.X, vecRight1.Z), new Vector2(vecRight2.X, vecRight2.Z))) * (180f / (float)Math.PI);
            vec.Z = (float)Math.Atan2(CrossProduct(new Vector2(vecRight1.X, vecRight1.Y), new Vector2(vecRight2.X, vecRight2.Y)), DotProduct(new Vector2(vecRight1.X, vecRight1.Y), new Vector2(vecRight2.X, vecRight2.Y))) * (180f / (float)Math.PI);

            return vec;
        }

        /// <summary>
        /// Method for calculating the angle between two vectors in 2D space by selecting a vector to rotate and then projecting this vector onto 2D space
        /// </summary>
        /// <param name="quat1"></param>
        /// <param name="quat2"></param>
        /// <param name="planeVector"></param>
        /// <param name="rotVec"></param>
        /// <returns></returns>
        public static double VectorToAngleCross(Quaternion quat1, Quaternion quat2, Vector3 planeVector, Vector3 rotVec)
        {
            var rotatedVec1 = quat1 * rotVec;
            var rotatedVec2 = quat2 * rotVec;

            // Project the two rotated vectors to be orthogonal to the plane vector (essentially becomes vector in the 2D space that is orthogonal to the plane vector since the plane vector is itself perfectly orthogonal to a plane)
            var orthVec1 = CrossProduct(rotatedVec1, planeVector);
            var orthVec2 = CrossProduct(rotatedVec2, planeVector);

            // Dot and cross product between the projected lines
            var dotVec = DotProduct(orthVec1, orthVec2);
            var crossVec = CrossProduct(orthVec1, orthVec2);

            var angle = (float)Math.Atan2(Norm(crossVec), dotVec);

            // If the vectors are not very overlapped (dotProd ~= 1.0 and rotVec vertical) and the vector is pointing very out of plane (crossProd < 0.2) then dismiss this value
            if (Math.Abs(dotVec) < 0.7 & Norm(crossVec) < 0.2)
            {
                var temp1 = Norm(crossVec);
                var temp2 = dotVec;
                angle = float.NaN;
            }

            if (DotProduct(crossVec, planeVector) < 0)
            {
                angle = -angle;
            }

            return angle * (180f / (float)Math.PI);
        }

        /// <summary>
        /// Finds the norm of a 3D vector
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        public static double Norm(Vector3 vec)
        {
            return Math.Sqrt(vec.X * vec.X + vec.Y * vec.Y + vec.Z * vec.Z);
        }

        /// <summary>
        /// Simple method for estimating the angle in each plane for a single vector
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        public static Vector3 VectorToAngle(Vector3 vec)
        {
            Vector3 pitchYawRoll;

            pitchYawRoll.X = (float)Math.Atan2(vec.Y, vec.Z) * (180f / (float)Math.PI);
            pitchYawRoll.Y = (float)Math.Atan2(vec.X, vec.Z) * (180f / (float)Math.PI);
            pitchYawRoll.Z = (float)Math.Atan2(vec.X, vec.Y) * (180f / (float)Math.PI);

            return pitchYawRoll;
        }

        /// <summary>
        /// Cross product of two 3D vectors
        /// </summary>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public static Vector3 CrossProduct(Vector3 vec1, Vector3 vec2)
        {
            Vector3 cProduct;
            cProduct.X = (vec1.Y * vec2.Z) - (vec1.Z * vec2.Y);
            cProduct.Y = (vec1.Z * vec2.X) - (vec1.X * vec2.Z);
            cProduct.Z = (vec1.X * vec2.Y) - (vec1.Y * vec2.X);
            return cProduct;
        }

        /// <summary>
        /// Cross product of two 2D vectors
        /// </summary>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public static float CrossProduct(Vector2 vec1, Vector2 vec2)
        {
            return vec1.X * vec2.Y - vec1.Y * vec2.X;
        }

        /// <summary>
        /// Dot product of two 3D vectors
        /// </summary>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public static float DotProduct(Vector3 vec1, Vector3 vec2)
        {
            return (vec1.X * vec2.X + vec1.Y * vec2.Y + vec1.Z * vec2.Z);
        }

        /// <summary>
        /// Dot product of two 2D vectors
        /// </summary>
        /// <param name="vec1"></param>
        /// <param name="vec2"></param>
        /// <returns></returns>
        public static float DotProduct(Vector2 vec1, Vector2 vec2)
        {
            return (vec1.X * vec2.X + vec1.Y * vec2.Y);
        }

        /// <summary>
        /// Converting a quaternion to its axis angle representation
        /// </summary>
        /// <param name="quat"></param>
        /// <returns></returns>
        public static float[] QuatToAxisAngle(Quaternion quat)
        {
            float angle = 2f * (float)Math.Acos(quat.W);
            float s = (float) Math.Sqrt(1 - quat.W * quat.W);
            float x;
            float y;
            float z;
            if (s < 0.001)
            {
                x = quat.X;
                y = quat.Y;
                z = quat.Z;
            }
            else
            {
                x = quat.X / s;
                y = quat.Y / s;
                z = quat.Z / s;
            }
            return new float[]{x,y,z,angle};
        }

        /// <summary>
        /// Calculates the different between two quaternions as euler angles
        /// </summary>
        /// <param name="quat1"></param>
        /// <param name="quat2"></param>
        /// <returns></returns>
        public static Vector3 QuatDifferenceAngle(Quaternion quat1, Quaternion quat2)
        {
            Vector3 eulerAngle = QuatToEulerAngles(quat1 * Quaternion.Invert(quat2));
            return eulerAngle;
        }

        public static float[] RotationMatrixX(float gx, float gy, float gz, float mx, float my, float mz, float ax, float ay, float az, float theta)
        {
            theta = (theta * (float)Math.PI) / (float)180;

            float gyrX = gx;
            float gyrY = ((float)Math.Cos(theta) * gy) + ((float)-Math.Sin(theta) * gz);
            float gyrZ = ((float)Math.Sin(theta) * gy) + ((float)Math.Cos(theta) * gz);
            float magX = mx;
            float magY = ((float)Math.Cos(theta) * my) + ((float)-Math.Sin(theta) * mz);
            float magZ = ((float)Math.Sin(theta) * my) + ((float)Math.Cos(theta) * mz);
            float accX = ax;
            float accY = ((float)Math.Cos(theta) * ay) + ((float)-Math.Sin(theta) * az);
            float accZ = ((float)Math.Sin(theta) * ay) + ((float)Math.Cos(theta) * az);

            return new float[] { gyrX, gyrY, gyrZ, magX, magY, magZ, accX, accY, accZ };
        }

        public static float[] RotationMatrixY(float gx, float gy, float gz, float mx, float my, float mz, float ax, float ay, float az, float theta)
        {
            theta = (theta * (float) Math.PI) / (float) 180;

            float gyrX = ((float) Math.Cos(theta) * gx) + ((float) Math.Sin(theta) * gz);
            float gyrY = gy;
            float gyrZ = ((float)-Math.Sin(theta) * gx) + ((float) Math.Cos(theta) * gz);

            float magX = ((float) Math.Cos(theta) * mx) + ((float) Math.Sin(theta) * mz);
            float magY = my;
            float magZ = ((float)-Math.Sin(theta) * mx) + ((float) Math.Cos(theta) * mz);

            float accX = ((float) Math.Cos(theta) * ax) + ((float) Math.Sin(theta) * az);
            float accY = ay;
            float accZ = ((float)-Math.Sin(theta) * ax) + ((float) Math.Cos(theta) * az);

            return new float[] { gyrX, gyrY, gyrZ, magX, magY, magZ, accX, accY, accZ };
        }

        public static float[] RotationMatrixZ(float gx, float gy, float gz, float mx, float my, float mz, float ax, float ay, float az, float theta)
        {
            theta = (theta * (float)Math.PI) / (float)180;

            float gyrX = ((float)Math.Cos(theta) * gx) + ((float)-Math.Sin(theta) * gy);
            float gyrY = ((float)Math.Sin(theta) * gx) + ((float)Math.Cos(theta) * gy);
            float gyrZ = gz;
            float magX = ((float)Math.Cos(theta) * mx) + ((float)-Math.Sin(theta) * my);
            float magY = ((float)Math.Sin(theta) * mx) + ((float)Math.Cos(theta) * my);
            float magZ = mz;
            float accX = ((float)Math.Cos(theta) * ax) + ((float)-Math.Sin(theta) * ay);
            float accY = ((float)Math.Sin(theta) * ax) + ((float)Math.Cos(theta) * ay);
            float accZ = az;

            return new float[]{ gyrX, gyrY, gyrZ, magX, magY, magZ, accX, accY, accZ };
        }

        #endregion
    }
}
