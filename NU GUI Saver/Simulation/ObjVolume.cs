﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace NU_GUI_Saver.Simulation
{
    class ObjVolume : Volume
    {
        /// <summary>
        /// Vertices of the object volume
        /// </summary>
        public Vector3[] Vertices;

        /// <summary>
        /// Colours of the object volume
        /// </summary>
        public Vector3[] Colours;

        /// <summary>
        /// Texture coordinates of the object volume
        /// </summary>
        public Vector2[] TextureCoords;

        /// <summary>
        /// List of face coordinates
        /// </summary>
        public List<Tuple<int, int, int>> faces = new List<Tuple<int, int, int>>();

        /// <summary>
        /// Return vertices, indices, faces count
        /// </summary>
        public override int VertCount
        {
            get { return Vertices.Length; }
        }
        public override int IndiceCount
        {
            get { return faces.Count * 3; }
        }
        public override int ColourDataCount
        {
            get { return Colours.Length; }
        }

        /// <summary>
        /// Get vertices, indices, colour data, texture coords
        /// </summary>
        /// <returns></returns>
        public override Vector3[] GetVerts()
        {
            return Vertices;
        }
        public override int[] GetIndices(int offset = 0)
        {
            List<int> temp = new List<int>();

            foreach (var face in faces)
            {
                temp.Add(face.Item1 + offset);
                temp.Add(face.Item2 + offset);
                temp.Add(face.Item3 + offset);
            }

            return temp.ToArray();
        }
        public override Vector3[] GetColorData()
        {
            return Colours;
        }
        public override Vector2[] GetTextureCoords()
        {
            return TextureCoords;
        }

        /// <summary>
        /// Calculate the model matrix using scale, rotation matrix, and translation
        /// </summary>
        public override void CalculateModelMatrix()
        {
            ModelMatrix = Matrix4.CreateScale(Scale) *
                          RotationMat * Matrix4.CreateTranslation(Position);
        }
    }
}
