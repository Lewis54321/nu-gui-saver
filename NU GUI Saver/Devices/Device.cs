﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InTheHand.Net.Sockets;

namespace NU_GUI_Saver
{
    /// <summary>
    /// Base class for all types of IMU devices which may be connected
    /// Provides bluetooth information and unique commands to that IMU device
    /// </summary>
    public abstract class Device
    {
        #region Variables

        /// <summary>
        /// Property that can be used to return the device name
        /// </summary>
        public string DeviceName { get; private set; }

        /// <summary>
        /// Flag (extracted from BluetoothDeviceInfo) which shows whether device is still connected
        /// </summary>
        private bool IsConnected { get; set; }

        /// <summary>
        /// BluetoothDeviceInfo object returned after a successful connection
        /// </summary>
        public BluetoothDeviceInfo DeviceInfo { get; private set; }

        /// <summary>
        /// IMU unique number in order to keep track of which IMU is connected (increments by 1 for every IMU connected)
        /// </summary>
        public int UniqueNumber { get; private set; }

        /// <summary>
        /// Directory where the calibration files are stored
        /// </summary>
        private string calDir = Path.Combine(Directory.GetCurrentDirectory(), "Simulation\\Calibration Files");

        /// Array of calibration values read from the text file
        /// Array in order of GyrXYZ, MagOffsetXYZ, MagCoefXYZ, AccXYZ
        // public float[] calValues { get; private set; }

        /// <summary>
        /// A dict containing the ADC and Inertial frequencies of the connected device
        /// Passed to the "SaveFileDetails" class instance so that the device frequencies may be recorded
        /// </summary>
        // public List<int> DeviceFreq { get; set; } = new List<int>();
        public UInt16 AdcFreq { get; set; }
        public UInt16 ImuFreq { get; set; }

        /// <summary>
        /// Settings for communicating with the device (stream and registers command) and packet information
        /// </summary>
        public string StreamCommand { get; set; }
        public string RegCommand { get; set; }
        public byte[] MemoryWriteCommand { get; set; }
        public byte PacketSize { get; set; }
        public byte[] PacketHeaders { get; set; }
        public byte RegHeader { get; set; }

        /// <summary>
        /// Firmware version of the device
        /// </summary>
        public int Firmware { get; set; }

        /// <summary>
        /// Dictionary for the user set inertial calibration values
        /// </summary>
        public Dictionary<string, Int16> InertialCal = new Dictionary<string, Int16>();

        /// <summary>
        /// Dictionary for the user set inertial advance settings (Scale and Rate settings)
        /// </summary>
        public Dictionary<string, byte> InertialAdv = new Dictionary<string, byte>();

        /// <summary>
        /// Dictionary for the user set sampling settings (includes the base frequency period and prescaler, frequency divisor to calculate inertial frequency, and ADC pins used)
        /// </summary>
        public Dictionary<string, UInt16> SampleSet = new Dictionary<string, UInt16>();

        /// <summary>
        /// Keys of the "InertialCal" Dict, used to read ordered calibration values from a notepad file
        /// </summary>
        private readonly string[] _calKeys = new string[]{"GyrX", "GyrY", "GyrZ", "MagX", "MagY", "MagZ", "AccX", "AccY", "AccZ"};

        /// <summary>
        /// List of pin functions for the connected device
        /// </summary>
        public List<string> PinFuncList { get; set; }

        /// <summary>
        /// A number which when represented in binary represents which of the 8 pins are active
        /// </summary>
        public UInt16 PinsActive { get; set; }

        /// <summary>
        /// Location of the connected device on the subject
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Orientation of the connected device (relative to the human body reference frame)
        /// </summary>
        public string Orientation { get; set; }

        #endregion

        /// <summary>
        /// Constructor which takes in the device info (returned by the scan) as well as the IMU number and the 
        /// data mode it is in
        /// </summary>
        /// <param name="deviceInfo"></param>
        /// <param name="uniqueNumber"></param>
        protected Device(BluetoothDeviceInfo deviceInfo, int uniqueNumber)
        {
            if (deviceInfo != null)
            {
                this.UniqueNumber = uniqueNumber;
                DeviceInfo = deviceInfo;
                IsConnected = deviceInfo.Connected;
                DeviceName = deviceInfo.DeviceName;
            }
        }

        /// <summary>
        /// Method used to read an array of calibration values from a text file
        /// Array in order of GyrXYZ, MagBiasXYZ, MagScaleXYZ, AccXYZ
        /// </summary>
        public void ReadCalibrationText()
        {
            string[] imageFiles = Directory.GetFiles(calDir, "*.TXT");

            int count = 0;
            bool fileFound = false;

            foreach (string filepath in imageFiles)
            {
                if (filepath.Contains(DeviceName))
                {
                    string line;
                    fileFound = true;
                    StreamReader reader = new StreamReader(filepath);
                    while ((line = reader.ReadLine()) != null)
                    {
                        InertialCal[_calKeys[count]] = Int16.Parse(line);
                        System.Console.WriteLine(line);
                        count++;
                    }
                    reader.Close();
                }
            }
            if (fileFound == false) throw new InvalidOperationException();
        }

        public void UpdateConfigSettings(List<string> pinList, string location, string orientation)
        {
            PinFuncList = pinList;
            Location = location;
            Orientation = orientation;
        }

        /// <summary>
        /// ReadRegisters method used for reading the period, preScalerIdx, and frequency from the 39 bytes of register information sent over by the NUIMU
        /// Called by an instance of BluetoothConnect
        /// Calculates the ADCfreq and IMUfreq
        /// </summary>
        /// <param name="registerInfo"> 39 bytes of register information passed in response to the "RegR" command </param>
        public abstract void ReadRegisters(byte[] registerInfo);

        /// <summary>
        /// Method for writing user set register values to the IMU 
        /// </summary>
        /// <param name="writeCalDict"></param>
        public abstract void WriteRegisters(Dictionary<string, int[]> writeCalDict, Stream stream);

        /// <summary>
        /// Method for sending commands to IMU devices, including the required headers and footers
        /// </summary>
        /// <param name="command"> A string of ASCII characters which are converted to their byte
        /// representation before sending </param>
        /// <param name="stream"></param>
        public abstract void SendCommand(string command, Stream stream);

        /// <summary>
        /// Method for sending commands to IMU devices, including the required headers and footers
        /// </summary>
        /// <param name="command"> A array of byte characters </param>
        /// <param name="stream"></param>
        public abstract void SendCommand(byte[] command, Stream stream);


    }
}
