﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace NU_GUI_Saver
{
    /// <summary>
    /// Class for handling the objects: Device, PacketDecoder, BluetoothConnect
    /// One DeviceManager object created per IMU connected
    /// </summary>
    class DeviceManager
    {
        #region Variables

        /// <summary>
        /// Device object
        /// </summary>
        public Device Device { get; private set; }

        /// <summary>
        /// Packet Decoder object
        /// </summary>
        public PacketDecoder PacketDecoder { get; }

        /// <summary>
        /// Bluetooth Connect object
        /// </summary>
        public BluetoothConnect BluetoothConnect { get; }

        /// <summary>
        /// Expose the device name from the device object so that these can be displayed in a list to the user
        /// </summary>
        public string DeviceName { get; private set; }

        /// <summary>
        /// Expose the device frequency so that it can be passed to the SaveFileDetails class when saving data
        /// </summary>
        public UInt16 AdcFreq { get; set; }
        public UInt16 ImuFreq { get; set; }

        /// <summary>
        /// Expose the device firmware so that is can be passed to the saveFileDetails class when saving data
        /// </summary>
        public int DeviceFirmware { get; set; }

        /// <summary>
        /// Boolean representing whether the configuration settings have been set for the connected IMU
        /// </summary>
        public bool ConfigUpdated = false;

        #endregion

        /// <summary>
        /// Constructor for DeviceManager
        /// Manages all over the lower level objects required for bluetooth connection
        /// </summary>
        /// <param name="device"></param>
        /// <param name="packetDecoder"></param>
        /// <param name="bluetoothConnect"></param>
        public DeviceManager(Device device, PacketDecoder packetDecoder, BluetoothConnect bluetoothConnect)
        {
            this.Device = device;
            this.PacketDecoder = packetDecoder;
            this.BluetoothConnect = bluetoothConnect;
        }

        /// <summary>
        /// Simple method for reading the underlying data in the Device object
        /// Used to make it easier to access this data as a list across all DeviceManagers later
        /// </summary>
        public void UpdateDeviceDetails()
        {
            this.DeviceFirmware = Device.Firmware;
            this.AdcFreq = Device.AdcFreq;
            this.ImuFreq = Device.ImuFreq;
            this.DeviceName = Device.DeviceInfo.DeviceName;
        }
    }
}
