﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InTheHand.Net.Sockets;
using OxyPlot.Annotations;


namespace NU_GUI_Saver
{
    /// <summary>
    /// Derived DeviceNUIMU class for the NUIMU which inherts from the Device base class
    /// </summary>
    public class DeviceNUIMU:Device
    {

        #region Variables

        #endregion

        /// <summary>
        /// Inheriting the constructor from the device base class
        /// </summary>
        /// <param name="deviceInfo"></param>
        /// /// <param name="UniqueNumber"></param>
        public DeviceNUIMU(BluetoothDeviceInfo deviceInfo, int uniqueNumber)
            : base(deviceInfo, uniqueNumber)
        {
            StreamCommand = "M4dg";
            RegCommand = "RegR";
            RegHeader = 0x43;
            MemoryWriteCommand = new byte[4] {RegHeader, 0xDD, 0x00, 0x00};
            PacketSize = 34;
            PacketHeaders = new byte[3] { 0xDD, 0xAA, 0x55 };
        }

        /// <inheritdoc cref="ReadRegisters(byte[])"/>
        public override void ReadRegisters(byte[] registerInfo)
        {
            InertialCal.Clear();
            InertialAdv.Clear();
            SampleSet.Clear();

            int[] preScalerRange = { 1, 8, 64, 256 };
            int clock = 30000000;

            PinsActive = PacketDecoder.CombineBytesUnsigned(registerInfo[8], registerInfo[9]);
            UInt16 period = PacketDecoder.CombineBytesUnsigned(registerInfo[4], registerInfo[5]);
            UInt16 frequencyDiv = PacketDecoder.CombineBytesUnsigned(registerInfo[6], registerInfo[7]);
            UInt16 preScalerIdx = PacketDecoder.CombineBytesUnsigned(registerInfo[10], registerInfo[11]);
            Firmware = PacketDecoder.CombineBytesUnsigned(registerInfo[36], registerInfo[37]);

            var preScaler = preScalerRange[preScalerIdx];
            AdcFreq = (UInt16)(clock / (period * preScaler * 2));
            ImuFreq = (UInt16)(AdcFreq / (frequencyDiv + 1));

            SampleSet.Add("AdcFreq", AdcFreq);
            SampleSet.Add("Period", period);
            SampleSet.Add("PrescalerIdx", preScalerIdx);
            SampleSet.Add("FrequencyDiv", frequencyDiv);
            SampleSet.Add("AdcPins", PinsActive);

            InertialAdv.Add("GyrScaleIdx", registerInfo[12]);
            InertialAdv.Add("GyrRateIdx", registerInfo[13]);
            InertialAdv.Add("AccScaleIdx", registerInfo[14]);
            InertialAdv.Add("AccRateIdx", registerInfo[15]);
            InertialAdv.Add("MagScaleIdx", registerInfo[16]);
            InertialAdv.Add("MagRateIdx", registerInfo[17]);

            InertialCal.Add("MagX", PacketDecoder.CombineBytesSigned(registerInfo[18], registerInfo[19]));
            InertialCal.Add("MagY", PacketDecoder.CombineBytesSigned(registerInfo[20], registerInfo[21]));
            InertialCal.Add("MagZ", PacketDecoder.CombineBytesSigned(registerInfo[22], registerInfo[23]));
            InertialCal.Add("AccX", PacketDecoder.CombineBytesSigned(registerInfo[24], registerInfo[25]));
            InertialCal.Add("AccY", PacketDecoder.CombineBytesSigned(registerInfo[26], registerInfo[27]));
            InertialCal.Add("AccZ", PacketDecoder.CombineBytesSigned(registerInfo[28], registerInfo[29]));
            InertialCal.Add("GyrX", PacketDecoder.CombineBytesSigned(registerInfo[30], registerInfo[31]));
            InertialCal.Add("GyrY", PacketDecoder.CombineBytesSigned(registerInfo[32], registerInfo[33]));
            InertialCal.Add("GyrZ", PacketDecoder.CombineBytesSigned(registerInfo[34], registerInfo[35]));
        }

        /// <inheritdoc cref="WriteRegisters(Dictionary{string, int[], Stream})"/>
        public override void WriteRegisters(Dictionary<string, int[]> writeCalDict, Stream stream)
        {
            foreach (var metric in writeCalDict)
            {
                var registerAddress = (byte)metric.Value[0];
                var registerHighValue = (byte)(metric.Value[1] >> 8);
                var registerLowValue = (byte)metric.Value[1];

                var registerCommand = new byte[] { RegHeader, registerAddress, registerHighValue, registerLowValue };

                SendCommand(registerCommand, stream);
            }

            var temp = MemoryWriteCommand;
            SendCommand(MemoryWriteCommand, stream);
        }

        /// <inheritdoc cref="SendCommand(string,System.IO.Stream)"/>
        public override void SendCommand(string command, Stream stream)
        {
            stream.WriteByte(0x07);
            foreach (char c in command)
            {
                stream.WriteByte((byte)c);
            }
            stream.WriteByte(0x0B);
        }

        /// <inheritdoc cref="SendCommand(byte[],System.IO.Stream)"/>
        public override void SendCommand(byte[] command, Stream stream)
        {
            stream.WriteByte(0x07);
            foreach (byte b in command)
            {
                stream.WriteByte(b);
            }
            stream.WriteByte(0x0B);
        }
    }
}
