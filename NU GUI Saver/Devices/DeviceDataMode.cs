﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    public class DeviceDataMode
    {
        public string Command { get; }
        public string RegCommand { get; }
        public byte PacketSize { get; }
        public byte[] PacketHeaders { get; }

        readonly int _data_modalities;

        public DeviceDataMode(string name, string command, byte packet_size, byte[] packet_headers, string file_column_headers, int data_modalities)
        {
            this.Command = command;
            this.PacketSize = packet_size;
            this.PacketHeaders = packet_headers;
        }

        public DeviceDataMode()
        {
            this.Command = "M4dg";
            this.RegCommand = "RegR";
            this.PacketSize = 34;
            this.PacketHeaders = new byte[3] { 0xDD, 0xAA, 0x55 };
        }
    }
}
