﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InTheHand.Net.Sockets;

namespace NU_GUI_Saver.Devices
{
    class DeviceXIMU : Device
    {
        /// <summary>
        /// Inheriting the constructor from the device base class
        /// </summary>
        /// <param name="deviceInfo"></param>
        /// /// <param name="UniqueNumber"></param>
        public DeviceXIMU(BluetoothDeviceInfo deviceInfo, int uniqueNumber)
            : base(deviceInfo, uniqueNumber)
        {
        }

        /// <inheritdoc cref="ReadRegisters(byte[])"/>
        public override void ReadRegisters(byte[] registerInfo)
        {
        }

        /// <inheritdoc cref="WriteRegisters(Dictionary{string, int[]}, Stream)"/>
        public override void WriteRegisters(Dictionary<string, int[]> writeCalDict, Stream stream)
        {
        }

        /// <inheritdoc cref="SendCommand(string,System.IO.Stream)"/>
        public override void SendCommand(string command, Stream stream)
        {
        }

        /// <inheritdoc cref="SendCommand(byte[],System.IO.Stream)"/>
        public override void SendCommand(byte[] command, Stream stream)
        {
        }
    }
}
