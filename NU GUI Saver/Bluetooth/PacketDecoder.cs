﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms.VisualStyles;
using NU_GUI_Saver.Classes;

namespace NU_GUI_Saver
{
    public class PacketDecoder
    {

        #region Variables

        /// <summary>
        /// packetDetails is made up of 4 integers (Time Stamp, IMU Number, Task, and Task Stage)
        /// Needs to be a field since it is updated with event handler from event in GUI whenever the task is changed
        /// </summary>
        private readonly int[] _packetDetails = new int[5];

        /// <summary>
        /// Default length of the imu data in the NUIMU packets
        /// </summary>
        private const byte ImuPacketLength = 18;

        /// <summary>
        /// Clock used to time stamp saved data (saved in units of 100 micro-seconds)
        /// </summary>
        private readonly Clock _packetClock;

        #endregion

        /// <summary>
        /// Class Constructor
        /// Device information and clock (shared between all packet decoders) passed
        /// </summary>
        /// <param name="device"></param>
        /// <param name="packetClock"></param>
        public PacketDecoder(Clock packetClock)
        {
            this._packetClock = packetClock;
        }

        #region Decode Packets

        /// <summary>
        /// Main method which is called by an event from the bluetooth connection class object every time a packet arrives
        /// </summary>
        /// <param name="packet"></param>
        /// <param name="packetLength"></param>
        /// <param name="packetCounter"></param>
        /// <param name="imuNumber"></param>
        public void DecodePackets(byte[] packet, int packetLength, byte packetCounter, int imuNumber)
        {
            int[] adc;

            // Updates first indices of packetDetails with packetClock and the imu number
            _packetDetails[0] = _packetClock.UtcNow;
            _packetDetails[1] = packetCounter;
            _packetDetails[2] = imuNumber;

            // Create decoded packet dictionary and add to it the packet details
            Dictionary<string, int[]> decodedPacket = new Dictionary<string, int[]>();
            decodedPacket.Add("Packet Details", _packetDetails);            

            // Process the packet differently depending on packet type (IMU only or IMU and ADC data)
            switch (PacketType(packetLength))
            {
                case "ADC Data Only":

                    adc = DecodeAdc(packet, packetLength, 0);

                    // Add the ADC data to the decoded packet and trigger event to pass packet to dataSaver class
                    decodedPacket.Add("ADC", adc);
                    OnPacketDecoded(new PacketDecodedEventArgs(decodedPacket));
                    break;

                case "IMU and ADC Data":

                    int[] imu = DecodeImu(packet);

                    int adcPacketLength = packetLength - ImuPacketLength;
                    adc = DecodeAdc(packet, adcPacketLength, 18);

                    // Add the ADC and IMU data to the decoded packet and trigger event to pass packet to dataSaver class
                    decodedPacket.Add("ADC", adc);
                    decodedPacket.Add("IMU", imu);
                    OnPacketDecoded(new PacketDecodedEventArgs(decodedPacket));
                    break;
            }
        }

        /// <summary>
        /// Method for decoding the IMU section of the packet
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        private int[] DecodeImu(byte[] packet)
        {
            var thread = Thread.CurrentThread;
            int[] imu = new int[9];

            // Extract inertial data into int arrays. (bytes 0-17)
            for (int i = 0; i < 9; i++)
            {
                int iImu = 2 * i;
                imu[i] = CombineBytesSigned(packet[iImu + 1], packet[iImu]);
            }
            return imu;         
        }

        /// <summary>
        /// Method for decoding the ADC section of the packet
        /// </summary>
        /// <param name="packet"> The packet of data </param>
        /// <param name="ADCPacketLength"> The length of the ADC data in the packet </param>
        /// <param name="adcStartIndex"> The index of the packet that the ADC data starts from </param>
        /// <returns></returns>
        private int[] DecodeAdc(byte[] packet, int adcPacketLength, int adcStartIndex)
        {
            int[] adc = new int[8];

            // Extract analog data in int arrays. (bytes 18 up to 33)
            for (int i = 0; i < adcPacketLength/2; i++)
            {
                int iADC = (2 * i) + adcStartIndex;
                adc[i] = CombineBytesUnsigned(packet[iADC], packet[iADC + 1]);
            }
            return adc;
        }

        /// <summary>
        /// Method for detecting the packet type based on the size received
        /// </summary>
        /// <param name="sizeReceived"></param>
        /// <returns></returns>
        private string PacketType(int sizeReceived)
        {
            if (sizeReceived <= 16)
                return "ADC Data Only";
            else//(size_received > 16 && size_received < 42)
                return "IMU and ADC Data";
        }

        #endregion

        #region Commands

        #endregion

        #region Combining Bytes

        /// <summary>
        /// Method for combined the high and low bytes of a signed 16 bit sequence
        /// </summary>
        /// <param name="highByte"></param>
        /// <param name="lowByte"></param>
        /// <returns></returns>
        public static short CombineBytesSigned(byte highByte, byte lowByte)
        {
            return (short)(((highByte & 0XFF) << 8) | (lowByte & 0xFF));
        }

        /// <summary>
        /// Method for combining the high and low bytes of an unsigned 16 bit sequence
        /// </summary>
        /// <param name="highByte"></param>
        /// <param name="lowByte"></param>
        /// <returns></returns>
        public static ushort CombineBytesUnsigned(byte highByte, byte lowByte)
        {
            return (ushort)(((highByte & 0xFF) << 8) | (lowByte & 0xFF));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Event handler for event in bluetooth connection class object (set up in GUI)
        /// Triggers every time a packet is received
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnPacketSent(object sender, PacketSentEventArgs e)
        {
            byte[] packet = e.Packet;
            int packetLength = e.PacketLength;
            byte packetCounter = e.PacketCounter;
            int imuNumber = e.ImuNumber;
            DecodePackets(packet, packetLength, packetCounter, imuNumber);
        }

        /// <summary>
        /// Event handler for taskChange event sent from GUI. This event sents data of the task number (index) and the task state 
        /// (0 for stopped, 1 for started)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnTaskChanged(object sender, TaskIndexEventArgs e)
        {
            _packetDetails[3] = e.TaskIndex;
            _packetDetails[4] = e.TaskState;
        }

        #endregion

        #region Events

        /// <summary>
        /// Event for every packet that is decoded
        /// Set up in GUI and triggers a method in Data saver class object to write a line to CSV file
        /// </summary>
        public event EventHandler<PacketDecodedEventArgs> PacketDecoded;
        public virtual void OnPacketDecoded(PacketDecodedEventArgs e)
        {
            PacketDecoded?.Invoke(this, e);
        }

        #endregion

    }
}
