﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using InTheHand.Net.Sockets;
using System.IO;
using System.Linq;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using OpenTK.Graphics.ES20;

namespace NU_GUI_Saver
{
    /// <summary>
    /// This class is responsible for forming a connection with bluetooth device
    /// using the bluetooth information provided by the bluetooth scanner
    /// </summary>
    class BluetoothConnect
    {
        #region Variables

        /// <summary>
        /// UUID required for creating a connection to a bluetooth device
        /// </summary>
        private static readonly Guid MyServiceUuid = new Guid("00001101-0000-1000-8000-00805F9B34FB");

        /// <summary>
        /// Bluetooth client responsible for making the connection
        /// </summary>
        private BluetoothClient _client;

        /// <summary>
        ///  Passed to provide the address to create the connection, and to add the bluetooth name to the displayed list
        /// </summary>
        private readonly BluetoothDeviceInfo _bluetoothDevice;

        /// <summary>
        /// The particular device that a bluetooth connection class object is made for
        /// Required for unique methods of the device and the datamode it is in
        /// Needs to be field since required in many methods
        /// </summary>
        private readonly DeviceNUIMU _device;

        /// <summary>
        /// Field which states whether to maintain a bluetooth connection or not
        /// Used in multiple different methods
        /// </summary>
        public bool ConnectFlag = true;

        /// <summary>
        /// List that contains all the bytes of the incoming packet before triggering event to be
        /// sent on to event handler in packet decoder
        /// </summary>
        private readonly List<byte> _packet;

        /// <summary>
        /// Count kept of the number of reconnect attempts for a given IMU when it is disconnected accidentally
        /// </summary>
        private int _reconnectAttempt;

        /// <summary>
        /// Bool used to determine whether the IMU should attempt to reconnect if disconnected
        /// Set to true if the IMU is inadvertedly disconnected
        /// </summary>
        private bool _attemptReconnect = false;

        /// <summary>
        /// The bluetooth stream which is read and written to
        /// </summary>
        private Stream _stream;

        #endregion

        #region Constructors

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="device"></param>
        public BluetoothConnect(DeviceNUIMU device)
        {
            this._device = device;
            _bluetoothDevice = device.DeviceInfo;
            _packet = new List<byte>(device.PacketSize);            
        }

        #endregion

        #region Stream Methods

        /// <summary>
        /// This is the main connection method which is called by the GUI to form a connection
        /// Creates a new client and connects it to a remote host (device)
        /// </summary>
        public void Connect()
        {
            _client = new BluetoothClient();
            if (!_client.Connected)
            {
                // connect to a device using the device address, UUID, callback result, and the client
                _client.BeginConnect(_bluetoothDevice.DeviceAddress, MyServiceUuid, this.ConnectCallback, _client);
            }
        }

        /// <summary>
        /// Callback method called after an attempt to connect to the bluetooth device is made
        /// If this is a user attempted connection then connection will either be successful or fail
        /// If this is an automated reconnect attempt (after an existing connection fails) then a connection attempt will be repeated until successful
        /// This call back method automatically creates a new thread (default name Worker Thread) which is then renamed for ease of organisation
        /// </summary>
        /// <param name="result"></param>
        private void ConnectCallback(IAsyncResult result)
        {
            var thread = Thread.CurrentThread;
            thread.Name = "Bluetooth_CB_Thread_" + _device.UniqueNumber;

            // Convert result from IAsync to BluetoothClient type
            _client = (BluetoothClient)result.AsyncState;
            if (!_client.Connected)
            {
                // Accepts an incoming connection attempt from remote device
                try
                {
                    _client.EndConnect(result);
                }
                // If this is a reconnect attempt then trigger reconnect event handler to reconnect
                // Otherwise (user connect attempt) display failure message and return
                catch
                {
                    if (_attemptReconnect)
                    {
                        BtReconnect();
                    }
                    else
                    {
                        OnBluetoothConnectionStatusChanged(new UpdatedStatusEventArgs("Error forming the connection. Please try again..."));
                    }
                    return;
                }
            }

            // For successful connection attempt display appropriate connection message and create a new bluetooth stream
            if (_attemptReconnect)
            {
                OnBluetoothConnectionStatusChanged(new UpdatedStatusEventArgs(string.Format("IMU number {0} reconnected!", _device.UniqueNumber)));
            }
            else
            {
                OnBluetoothConnectionStatusChanged(new UpdatedStatusEventArgs(string.Format("IMU number {0} connected!", _device.UniqueNumber)));
            }
            ConnectFlag = true;
            _attemptReconnect = false;

            BluetoothStream();
        }

        /// <summary>
        /// This method is called to form a stream with the connected client
        /// Reads the first 15 bytes send by the IMU on connection
        /// Then sent the register command in order to get the IMU and ADC register frequencies
        /// </summary>
        private void BluetoothStream()
        {
            _stream = new MemoryStream();
            _stream = _client.GetStream();
            _stream.ReadTimeout = 2400;

            if (_stream.CanRead)
            {
                // Read the default "Connection Open" message (first 15 bytes) and wait for IMU to be ready to receive commands
                // else statement required for the scenario that the IMU reconnects to GUI while still in data sending mode
                // In this case the device will not send a connection message and will stream data continuously (which must be cleared)
                Thread.Sleep(100);
                if (_client.Available == 15)
                {
                    byte[] connectionMessageBuffer = new byte[15];
                    _stream.Read(connectionMessageBuffer, 0, 15);
                    Thread.Sleep(100);
                }
                else
                {
                    ClearStream();
                }

                // Repeatedly tries to read the registers from the device until successful
                while (!ReadRegisters())
                {
                    ClearStream();
                }

                // Sends the command to receive a continuous stream of data from the device
                _device.SendCommand(_device.StreamCommand, _stream);

                // Send event back to the GUI that the bluetooth device has been connected so that it can be handled and displayed to the user
                OnBluetoothConnectionCompleted(new BtConnectionEventArgs(_device.DeviceName, _device.UniqueNumber));
            }

            while (ConnectFlag)
            {
                ReadData();
            }
        }

        /// <summary>
        /// Method for reading and processing streamed M4dg data from the IMU
        /// </summary>
        private void ReadData()
        {
            try
            {
                if (PacketHeader(_stream))
                {                   
                    // Calculating the packet length with the mode byte and the packet length byte removed (there is also the the counter to consider for firmware so remove 3 instead of 2 bytes)
                    int entirePacketLength = _stream.ReadByte();
                    int packetLength;
                    if (_device.Firmware == 6)
                    {
                        packetLength = entirePacketLength - 2;
                    }
                    else
                    {
                        packetLength = entirePacketLength - 3;
                    }

                    // Reading and discarding the mode byte (not used)
                    _stream.ReadByte();

                    // Reading the adc and imu data (if present)
                    for (int i = 0; i < packetLength; i++)
                    {
                        //while (!stream.CanRead);
                        _packet.Add((byte)_stream.ReadByte());
                    }

                    // Reading the packet counter byte (only included with firmware versions > 6 otherwise will default to 0)
                    byte packetCounter = (byte)0;
                    if (_device.Firmware != 6)
                    {
                        packetCounter = (byte)_stream.ReadByte();
                    }

                    // Reading the packet length byte at the end of the packet and making sure it agrees with the first packet length byte
                    int entirePacketLengthTwo = _stream.ReadByte();
                    if (entirePacketLength == entirePacketLengthTwo)
                    {
                        OnPacketSent(new PacketSentEventArgs(_packet.ToArray(), packetLength, packetCounter,
                            _device.UniqueNumber));
                    }
                    else
                    {
                        OnPacketDropped(new EventArgs());
                    }
                }
            }
            // Notifies user that the device has been disconnect and stops streaming of data
            // Initiates disconnect process which kills the client
            catch (SystemException e)
            {
                OnBluetoothConnectionStatusChanged(
                    new UpdatedStatusEventArgs(string.Format("IMU NUMBER {0} DISCONNECTED",
                        _device.UniqueNumber)));
                Thread.Sleep(3000);
                
                Disconnect();

                _attemptReconnect = true;
                BtReconnect();
                return;
            }
        }

        /// <summary>
        /// Method which cycles through each of the packet headers to make sure a packet has been received
        /// </summary>
        /// <param name="s"> The bluetooth stream </param>
        /// <returns> True if each byte of the stream matches each byte in the packetHeader array, False otherwise </returns>
        private bool PacketHeader(Stream s)
        {
            return _device.PacketHeaders.All(ph => s.ReadByte() == ph);
        }

        /// <summary>
        /// Method sends an fake command to stop the IMU from streaming and then clears all the remaining data available in the client
        /// May need to play around with the sleep command to allow enough time after registers have been changed to resume streaming
        /// </summary>
        public void ClearStream()
        {
            _device.SendCommand("fake", _stream);
            Thread.Sleep(500);
            while (_client.Available > 0)
                _stream.ReadByte();
        }

        /// <summary>
        /// Pauses M4dg data from being sent by the IMU without killing the client/ stream
        /// Also clears the stream to make sure their aren't any unprocessed bytes which could cause problems
        /// </summary>
        public void PauseStream()
        {
            ConnectFlag = false;
        }

        /// <summary>
        /// Disconnect method called from the GUI or when the device loses stream (changes connect flag and disposes of bluetooth client)
        /// Closing or disposing of the client and then creating a new one of the same name in the same class instance throws errors 
        /// because the new client (underlying bluetoothStream) is also disposed of for some reason
        /// Ensures the ConnectFlag is false before disposing of the client (otherwise this method may get called again as a result of the
        /// catch statement in the streaming data method)
        /// </summary>
        public void Disconnect()
        {
            ConnectFlag = false;

            Thread.Sleep(100);
            if (ConnectFlag == false && _client != null)
            {
                OnBtDisconnected(new BtConnectionEventArgs(_device.DeviceName, _device.UniqueNumber));
                _client.Close();
                _client.Dispose();
                _client = null;
            }
        }

        /// <summary>
        /// Autoreconnect event handler called when device loses connection unintentially
        /// Attempts to reconnect the IMU and updates the GUI with the status
        /// </summary>
        private void BtReconnect()
        {
            _reconnectAttempt++;
            OnBluetoothConnectionStatusChanged(new UpdatedStatusEventArgs(string.Format("AutoReconnect Attempt {0}", _reconnectAttempt)));
            Thread.Sleep(2000);
            Connect();
        }

        #endregion

        #region Register Commands

        /// <summary>
        /// Method for sending command to the IMU for reading the register values and passing these to the Device instance
        /// </summary>
        private bool ReadRegisters()
        {
            _device.SendCommand(_device.RegCommand, _stream);
            byte[] registerMessageBuffer = new byte[39];
            if (!PacketHeader(_stream))
            {
                OnBluetoothConnectionStatusChanged(new UpdatedStatusEventArgs("Error reading the IMU registers. Please try again"));
                return false;
            }

            // This is a workaround for the weirdness of reading a long array at a time which for some reason results in zeros at the end
            for (int i = 0; i < 39; i++)
            {
                registerMessageBuffer[i] = (byte)_stream.ReadByte();
            }

            _device.ReadRegisters(registerMessageBuffer);
            Thread.Sleep(100);
            return true;
        }

        /// <summary>
        /// Method for writing user set register values to the IMU 
        /// </summary>
        /// <param name="writeCalDict"></param>
        public void WriteRegisters(Dictionary<string, int[]> writeCalDict)
        {
            
            //Method is called from the main GUI (and hence is on the MainThread so sleeping should not be useful)
            //Thread.Sleep(100);
        }

        #endregion

        #region Events

        /// <summary>
        /// Bluetooth Connection status event- sends data about blueooth connection or disconnection
        /// Managed in GUI and calls event handler in GUI
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        public delegate void BluetoothConnectionStatusChangedEventHandler(object source, UpdatedStatusEventArgs args);
        public event BluetoothConnectionStatusChangedEventHandler BluetoothConnectionStatusChanged;
        protected virtual void OnBluetoothConnectionStatusChanged(UpdatedStatusEventArgs e)
        {
            BluetoothConnectionStatusChanged?.Invoke(this, e);
        }

        /// <summary>
        /// Bluetooth Connection event which provides information to GUI that a bluetooth connection
        /// has been completed (managed in GUI)
        /// </summary>
        public event EventHandler<BtConnectionEventArgs> BluetoothConnectionCompleted;
        protected virtual void OnBluetoothConnectionCompleted(BtConnectionEventArgs e)
        {
            BluetoothConnectionCompleted?.Invoke(this, e);
        }

        /// <summary>
        /// Bluetooth Disconnect event which passes the unique IMU id back to the GUI so that it can be removed from the list
        /// of connected IMUs
        /// </summary>
        public event EventHandler<BtConnectionEventArgs> BtDisconnected;
        protected virtual void OnBtDisconnected(BtConnectionEventArgs e)
        {
            BtDisconnected?.Invoke(this, e);
        }

        /// <summary>
        /// Packet Sent event which provides packet information to GUI and packet decoder objects where it can be decoded
        /// (managed in GUI)
        /// </summary>
        public event EventHandler<PacketSentEventArgs> PacketSent;
        protected virtual void OnPacketSent(PacketSentEventArgs e)
        {
            PacketSent?.Invoke(this, e);
            _packet.Clear();
        }

        /// <summary>
        /// Packet dropped event which provides this information back to the GUI (managed in GUI)
        /// </summary>
        public event EventHandler<EventArgs> PacketDropped;
        protected virtual void OnPacketDropped(EventArgs e)
        {
            PacketDropped?.Invoke(this, e);
            _packet.Clear();
        }

        #endregion
    }
}
