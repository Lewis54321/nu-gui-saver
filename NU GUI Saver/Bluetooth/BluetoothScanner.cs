﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using InTheHand.Net.Sockets;

namespace NU_GUI_Saver
{

    /// <summary>
    /// Class used for scanning for bluetooth devices in range
    /// </summary>
    class BluetoothScanner
    {
        /// <summary>
        /// Dictionary of the returned bluetooth devices in range
        /// </summary>
        public Dictionary<string, BluetoothDeviceInfo> BluetoothDevices = new Dictionary<string, BluetoothDeviceInfo>();

        /// <summary>
        /// Main function for performing a bluetooth search using Bluetooth client class object
        /// Added functionality to change the device name (by adding number incremented by 1) if there are duplicate names (which otherwise throws an error)
        /// </summary>
        public void bluetoothSearch()
        {
            BluetoothClient client = new BluetoothClient();
            BluetoothDeviceInfo[] bluetoothDevicesAr = client.DiscoverDevices(999, true, true, false, true);
            if (bluetoothDevicesAr != null)
            {
                List<string> deviceNames = new List<string>();
                int count = 0;
                foreach (BluetoothDeviceInfo device in bluetoothDevicesAr)
                {
                    if (deviceNames.Any(s => device.DeviceName.Equals(s)))
                    {
                        count += 1;
                        device.DeviceName = device.DeviceName + count.ToString();
                    }
                    deviceNames.Add(device.DeviceName);
                }

                BluetoothDevices = bluetoothDevicesAr.ToDictionary(key => key.DeviceName, value => value);
            }
        }
    }
}
