﻿using System;
using System.Diagnostics;
using System.Threading;

namespace NU_GUI_Saver.Classes
{
    /// <summary>
    /// This class provides a high resolution clock using DateTime.UtcNow to periodically collect clock accurate data
    /// and using stopwatch time stamp the result of the time to update with high temporal resolution data
    /// </summary>
    public sealed class Clock : IDisposable
    {
        /// <summary>
        /// Thread local variables ensure that each thread has access to its own data and remove the need for locks when accessing class instance
        /// The frequency of the stopwatch (should be > 2.5x10^6) is used 
        /// </summary>
        private ThreadLocal<double> _startTimestamp = new ThreadLocal<double>(() => Stopwatch.GetTimestamp(), false);

        public void ResetClock()
        {
            _startTimestamp = new ThreadLocal<double>(() => Stopwatch.GetTimestamp(), false);
        }

        public int UtcNow
        {
            get
            {
                double endTimestamp = Stopwatch.GetTimestamp();
                // Timestamp units are 10th of milliseconds
                int timestamp = (int)Math.Round(((endTimestamp - _startTimestamp.Value) / Stopwatch.Frequency) * 10000);
                if (timestamp < 0)
                {
                    timestamp = 0;
                }
                return timestamp;
            }
        }

        /// <summary>
        /// Releases all resources used by the instance of <see cref="Clock"/>
        /// </summary>
        public void Dispose()
        {
            _startTimestamp.Dispose();
        }
    }
}
