﻿

namespace NU_GUI_Saver
{
    class TimerClass
    {
        public double timeSeconds { get; set; }
        public System.Timers.Timer timer;

        public void tenthSecondTimer()
        {
            timer = new System.Timers.Timer(100);
            //timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);
        }

        public void oneSecondTimer()
        {
            timer = new System.Timers.Timer(1000);
        }
    }
}
