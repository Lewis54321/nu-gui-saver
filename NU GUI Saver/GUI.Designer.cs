﻿using NU_GUI_Saver.Simulation;

namespace NU_GUI_Saver
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {            
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GUI));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.backgroundWorkerScan = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.Connected_Devices_ListBox = new System.Windows.Forms.ListBox();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.setCalibrationButton = new System.Windows.Forms.Button();
            this.bluetoothConnect = new System.Windows.Forms.GroupBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.Available_Devices_ListBox = new System.Windows.Forms.ListBox();
            this.bluetoothScanButton = new System.Windows.Forms.Button();
            this.bluetoothConnectButton = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.connectionStatusGroupBox = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.SaveDataRateLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LineSavedLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.packetDecodeRateLabel = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.decodedPacketsLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DeviceTypeBox = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxPins = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelPins2 = new System.Windows.Forms.TableLayoutPanel();
            this.IMU_2_LABEL = new System.Windows.Forms.Label();
            this.buttonConfig2 = new System.Windows.Forms.Button();
            this.tableLayoutPanelPins3 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonConfig3 = new System.Windows.Forms.Button();
            this.IMU_3_LABEL = new System.Windows.Forms.Label();
            this.tableLayoutPanelPins4 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonConfig4 = new System.Windows.Forms.Button();
            this.IMU_4_LABEL = new System.Windows.Forms.Label();
            this.tableLayoutPanelPins1 = new System.Windows.Forms.TableLayoutPanel();
            this.IMU_1_LABEL = new System.Windows.Forms.Label();
            this.buttonConfig1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.trialNoTextBox = new System.Windows.Forms.TextBox();
            this.trialTypeDropBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBoxInstrumSide = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxSittingIncl = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.filePathTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.taskComboBox = new System.Windows.Forms.ComboBox();
            this.taskStateButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.stopSaveButton = new System.Windows.Forms.Button();
            this.saveDataButton = new System.Windows.Forms.Button();
            this.textBoxTaskTimer = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.stopVisButton = new System.Windows.Forms.Button();
            this.avatarSimButton = new System.Windows.Forms.Button();
            this.cubeSimButton = new System.Windows.Forms.Button();
            this.accSimButton = new System.Windows.Forms.Button();
            this.magSimButton = new System.Windows.Forms.Button();
            this.gyrSimButton = new System.Windows.Forms.Button();
            this.adcSimButton = new System.Windows.Forms.Button();
            this.paintPanel = new System.Windows.Forms.Panel();
            this.tableLayoutPanelAngle = new System.Windows.Forms.TableLayoutPanel();
            this.labelShoulder = new System.Windows.Forms.Label();
            this.labelElbow = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.labelShoulderMag = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelShoulderY = new System.Windows.Forms.Label();
            this.labelShoulderZ = new System.Windows.Forms.Label();
            this.labelShoulderX = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelShoulderTwist = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.labelElbowMag = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelElbowZ = new System.Windows.Forms.Label();
            this.labelElbowY = new System.Windows.Forms.Label();
            this.labelElbowX = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelElbowTwist = new System.Windows.Forms.Label();
            this.tableLayoutPanelRot = new System.Windows.Forms.TableLayoutPanel();
            this.buttonRotLeft = new System.Windows.Forms.Button();
            this.buttonRotRight = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonTare = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panelTabContainer = new System.Windows.Forms.Panel();
            this.statusStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel12.SuspendLayout();
            this.bluetoothConnect.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.connectionStatusGroupBox.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel1.SuspendLayout();
            this.DeviceTypeBox.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.groupBoxPins.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanelPins2.SuspendLayout();
            this.tableLayoutPanelPins3.SuspendLayout();
            this.tableLayoutPanelPins4.SuspendLayout();
            this.tableLayoutPanelPins1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.paintPanel.SuspendLayout();
            this.tableLayoutPanelAngle.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanelRot.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panelTabContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 661);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(2, 0, 21, 0);
            this.statusStrip.Size = new System.Drawing.Size(1284, 41);
            this.statusStrip.TabIndex = 4;
            this.statusStrip.Text = "NU GUI";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(732, 36);
            this.toolStripStatusLabel.Text = "Please press \'Start Scan\' to find available devices...";
            // 
            // backgroundWorkerScan
            // 
            this.backgroundWorkerScan.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerScan_DoWork);
            this.backgroundWorkerScan.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerScan_RunWorkerCompleted);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.Location = new System.Drawing.Point(10, 9);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1264, 643);
            this.tabControl.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel10);
            this.tabPage1.Location = new System.Drawing.Point(4, 45);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(18);
            this.tabPage1.Size = new System.Drawing.Size(1256, 594);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Connect";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.tableLayoutPanel1);
            this.panel10.Controls.Add(this.panel4);
            this.panel10.Controls.Add(this.tableLayoutPanel15);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(18, 18);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1220, 558);
            this.panel10.TabIndex = 6;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.bluetoothConnect, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1220, 341);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // groupBox4
            // 
            this.groupBox4.AutoSize = true;
            this.groupBox4.Controls.Add(this.panel12);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(613, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(16, 18, 16, 18);
            this.groupBox4.Size = new System.Drawing.Size(604, 335);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Connected Devices";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.Connected_Devices_ListBox);
            this.panel12.Controls.Add(this.disconnectButton);
            this.panel12.Controls.Add(this.setCalibrationButton);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(16, 46);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(572, 271);
            this.panel12.TabIndex = 7;
            // 
            // Connected_Devices_ListBox
            // 
            this.Connected_Devices_ListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Connected_Devices_ListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F);
            this.Connected_Devices_ListBox.FormattingEnabled = true;
            this.Connected_Devices_ListBox.ItemHeight = 37;
            this.Connected_Devices_ListBox.Location = new System.Drawing.Point(0, 0);
            this.Connected_Devices_ListBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Connected_Devices_ListBox.Name = "Connected_Devices_ListBox";
            this.Connected_Devices_ListBox.Size = new System.Drawing.Size(572, 71);
            this.Connected_Devices_ListBox.TabIndex = 4;
            // 
            // disconnectButton
            // 
            this.disconnectButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.disconnectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disconnectButton.Location = new System.Drawing.Point(0, 71);
            this.disconnectButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(572, 100);
            this.disconnectButton.TabIndex = 6;
            this.disconnectButton.Text = "Disconnect All";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.buttonDisconnectIMUs_Click);
            // 
            // setCalibrationButton
            // 
            this.setCalibrationButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.setCalibrationButton.Enabled = false;
            this.setCalibrationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setCalibrationButton.Location = new System.Drawing.Point(0, 171);
            this.setCalibrationButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.setCalibrationButton.Name = "setCalibrationButton";
            this.setCalibrationButton.Size = new System.Drawing.Size(572, 100);
            this.setCalibrationButton.TabIndex = 7;
            this.setCalibrationButton.Text = "Set Calibration Values";
            this.setCalibrationButton.UseVisualStyleBackColor = true;
            this.setCalibrationButton.Click += new System.EventHandler(this.setCalibrationButton_Click);
            // 
            // bluetoothConnect
            // 
            this.bluetoothConnect.AutoSize = true;
            this.bluetoothConnect.Controls.Add(this.panel11);
            this.bluetoothConnect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bluetoothConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bluetoothConnect.Location = new System.Drawing.Point(3, 3);
            this.bluetoothConnect.Name = "bluetoothConnect";
            this.bluetoothConnect.Padding = new System.Windows.Forms.Padding(16, 18, 16, 18);
            this.bluetoothConnect.Size = new System.Drawing.Size(604, 335);
            this.bluetoothConnect.TabIndex = 1;
            this.bluetoothConnect.TabStop = false;
            this.bluetoothConnect.Text = "Bluetooth Scan";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.Available_Devices_ListBox);
            this.panel11.Controls.Add(this.bluetoothScanButton);
            this.panel11.Controls.Add(this.bluetoothConnectButton);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(16, 46);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(572, 271);
            this.panel11.TabIndex = 7;
            // 
            // Available_Devices_ListBox
            // 
            this.Available_Devices_ListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Available_Devices_ListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Available_Devices_ListBox.FormattingEnabled = true;
            this.Available_Devices_ListBox.ItemHeight = 37;
            this.Available_Devices_ListBox.Location = new System.Drawing.Point(0, 0);
            this.Available_Devices_ListBox.Name = "Available_Devices_ListBox";
            this.Available_Devices_ListBox.Size = new System.Drawing.Size(572, 71);
            this.Available_Devices_ListBox.TabIndex = 1;
            // 
            // bluetoothScanButton
            // 
            this.bluetoothScanButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bluetoothScanButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bluetoothScanButton.Location = new System.Drawing.Point(0, 71);
            this.bluetoothScanButton.Name = "bluetoothScanButton";
            this.bluetoothScanButton.Size = new System.Drawing.Size(572, 100);
            this.bluetoothScanButton.TabIndex = 3;
            this.bluetoothScanButton.Text = "Start Scan";
            this.bluetoothScanButton.UseVisualStyleBackColor = true;
            this.bluetoothScanButton.Click += new System.EventHandler(this.bluetoothScanButton_Click);
            // 
            // bluetoothConnectButton
            // 
            this.bluetoothConnectButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bluetoothConnectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bluetoothConnectButton.Location = new System.Drawing.Point(0, 171);
            this.bluetoothConnectButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bluetoothConnectButton.Name = "bluetoothConnectButton";
            this.bluetoothConnectButton.Size = new System.Drawing.Size(572, 100);
            this.bluetoothConnectButton.TabIndex = 2;
            this.bluetoothConnectButton.Text = "Connect IMU";
            this.bluetoothConnectButton.UseVisualStyleBackColor = true;
            this.bluetoothConnectButton.Click += new System.EventHandler(this.bluetoothConnectButton_Click);
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 341);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1220, 26);
            this.panel4.TabIndex = 8;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.connectionStatusGroupBox, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.DeviceTypeBox, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 367);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(1220, 191);
            this.tableLayoutPanel15.TabIndex = 7;
            // 
            // connectionStatusGroupBox
            // 
            this.connectionStatusGroupBox.Controls.Add(this.panel3);
            this.connectionStatusGroupBox.Controls.Add(this.panel2);
            this.connectionStatusGroupBox.Controls.Add(this.panel8);
            this.connectionStatusGroupBox.Controls.Add(this.panel1);
            this.connectionStatusGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.connectionStatusGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectionStatusGroupBox.Location = new System.Drawing.Point(614, 5);
            this.connectionStatusGroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.connectionStatusGroupBox.Name = "connectionStatusGroupBox";
            this.connectionStatusGroupBox.Padding = new System.Windows.Forms.Padding(16, 18, 16, 18);
            this.connectionStatusGroupBox.Size = new System.Drawing.Size(602, 181);
            this.connectionStatusGroupBox.TabIndex = 4;
            this.connectionStatusGroupBox.TabStop = false;
            this.connectionStatusGroupBox.Text = "Connection Status";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.SaveDataRateLabel);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(16, 131);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(570, 35);
            this.panel3.TabIndex = 10;
            // 
            // SaveDataRateLabel
            // 
            this.SaveDataRateLabel.AutoSize = true;
            this.SaveDataRateLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.SaveDataRateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveDataRateLabel.Location = new System.Drawing.Point(207, 0);
            this.SaveDataRateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SaveDataRateLabel.Name = "SaveDataRateLabel";
            this.SaveDataRateLabel.Size = new System.Drawing.Size(56, 25);
            this.SaveDataRateLabel.TabIndex = 7;
            this.SaveDataRateLabel.Text = "0 Hz";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Left;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(207, 25);
            this.label7.TabIndex = 6;
            this.label7.Text = "Packet Save Rate:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LineSavedLabel);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(16, 106);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(570, 25);
            this.panel2.TabIndex = 9;
            // 
            // LineSavedLabel
            // 
            this.LineSavedLabel.AutoSize = true;
            this.LineSavedLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LineSavedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LineSavedLabel.Location = new System.Drawing.Point(176, 0);
            this.LineSavedLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LineSavedLabel.Name = "LineSavedLabel";
            this.LineSavedLabel.Size = new System.Drawing.Size(24, 25);
            this.LineSavedLabel.TabIndex = 2;
            this.LineSavedLabel.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(176, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Packets Saved:";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.packetDecodeRateLabel);
            this.panel8.Controls.Add(this.label20);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(16, 71);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(570, 35);
            this.panel8.TabIndex = 11;
            // 
            // packetDecodeRateLabel
            // 
            this.packetDecodeRateLabel.AutoSize = true;
            this.packetDecodeRateLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.packetDecodeRateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packetDecodeRateLabel.Location = new System.Drawing.Point(234, 0);
            this.packetDecodeRateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.packetDecodeRateLabel.Name = "packetDecodeRateLabel";
            this.packetDecodeRateLabel.Size = new System.Drawing.Size(56, 25);
            this.packetDecodeRateLabel.TabIndex = 7;
            this.packetDecodeRateLabel.Text = "0 Hz";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Left;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(234, 25);
            this.label20.TabIndex = 6;
            this.label20.Text = "Packet Decode Rate:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.decodedPacketsLabel);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(16, 46);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(570, 25);
            this.panel1.TabIndex = 8;
            // 
            // decodedPacketsLabel
            // 
            this.decodedPacketsLabel.AutoSize = true;
            this.decodedPacketsLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.decodedPacketsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.decodedPacketsLabel.Location = new System.Drawing.Point(203, 0);
            this.decodedPacketsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.decodedPacketsLabel.Name = "decodedPacketsLabel";
            this.decodedPacketsLabel.Size = new System.Drawing.Size(24, 25);
            this.decodedPacketsLabel.TabIndex = 5;
            this.decodedPacketsLabel.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(203, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Packets Decoded:";
            // 
            // DeviceTypeBox
            // 
            this.DeviceTypeBox.Controls.Add(this.panel6);
            this.DeviceTypeBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DeviceTypeBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviceTypeBox.Location = new System.Drawing.Point(4, 5);
            this.DeviceTypeBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DeviceTypeBox.Name = "DeviceTypeBox";
            this.DeviceTypeBox.Padding = new System.Windows.Forms.Padding(16, 18, 16, 18);
            this.DeviceTypeBox.Size = new System.Drawing.Size(602, 181);
            this.DeviceTypeBox.TabIndex = 2;
            this.DeviceTypeBox.TabStop = false;
            this.DeviceTypeBox.Text = "Device";
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(16, 46);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(570, 45);
            this.panel6.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel12);
            this.tabPage2.Location = new System.Drawing.Point(4, 45);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(18);
            this.tabPage2.Size = new System.Drawing.Size(1256, 594);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Save";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(18, 18);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1220, 558);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.groupBoxPins, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.groupBox2, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.8617F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.1383F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(604, 552);
            this.tableLayoutPanel13.TabIndex = 7;
            // 
            // groupBoxPins
            // 
            this.groupBoxPins.Controls.Add(this.tableLayoutPanel14);
            this.groupBoxPins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxPins.Location = new System.Drawing.Point(4, 291);
            this.groupBoxPins.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxPins.Name = "groupBoxPins";
            this.groupBoxPins.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxPins.Size = new System.Drawing.Size(596, 256);
            this.groupBoxPins.TabIndex = 10;
            this.groupBoxPins.TabStop = false;
            this.groupBoxPins.Text = "IMU Configuration";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanelPins2, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanelPins3, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanelPins4, 0, 3);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanelPins1, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(4, 42);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 4;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(588, 209);
            this.tableLayoutPanel14.TabIndex = 0;
            // 
            // tableLayoutPanelPins2
            // 
            this.tableLayoutPanelPins2.ColumnCount = 2;
            this.tableLayoutPanelPins2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelPins2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelPins2.Controls.Add(this.IMU_2_LABEL, 0, 0);
            this.tableLayoutPanelPins2.Controls.Add(this.buttonConfig2, 1, 0);
            this.tableLayoutPanelPins2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelPins2.Location = new System.Drawing.Point(4, 57);
            this.tableLayoutPanelPins2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanelPins2.Name = "tableLayoutPanelPins2";
            this.tableLayoutPanelPins2.RowCount = 1;
            this.tableLayoutPanelPins2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPins2.Size = new System.Drawing.Size(580, 42);
            this.tableLayoutPanelPins2.TabIndex = 32;
            this.tableLayoutPanelPins2.Visible = false;
            // 
            // IMU_2_LABEL
            // 
            this.IMU_2_LABEL.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.IMU_2_LABEL.AutoSize = true;
            this.IMU_2_LABEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IMU_2_LABEL.Location = new System.Drawing.Point(4, 6);
            this.IMU_2_LABEL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IMU_2_LABEL.Name = "IMU_2_LABEL";
            this.IMU_2_LABEL.Size = new System.Drawing.Size(81, 29);
            this.IMU_2_LABEL.TabIndex = 31;
            this.IMU_2_LABEL.Text = "IMU 2:";
            // 
            // buttonConfig2
            // 
            this.buttonConfig2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonConfig2.Location = new System.Drawing.Point(208, 0);
            this.buttonConfig2.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.buttonConfig2.Name = "buttonConfig2";
            this.buttonConfig2.Size = new System.Drawing.Size(357, 42);
            this.buttonConfig2.TabIndex = 32;
            this.buttonConfig2.Text = "Configure IMU";
            this.buttonConfig2.UseVisualStyleBackColor = true;
            this.buttonConfig2.Click += new System.EventHandler(this.buttonConfig2_Click);
            // 
            // tableLayoutPanelPins3
            // 
            this.tableLayoutPanelPins3.ColumnCount = 2;
            this.tableLayoutPanelPins3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelPins3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelPins3.Controls.Add(this.buttonConfig3, 0, 0);
            this.tableLayoutPanelPins3.Controls.Add(this.IMU_3_LABEL, 0, 0);
            this.tableLayoutPanelPins3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelPins3.Location = new System.Drawing.Point(4, 109);
            this.tableLayoutPanelPins3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanelPins3.Name = "tableLayoutPanelPins3";
            this.tableLayoutPanelPins3.RowCount = 1;
            this.tableLayoutPanelPins3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPins3.Size = new System.Drawing.Size(580, 42);
            this.tableLayoutPanelPins3.TabIndex = 33;
            this.tableLayoutPanelPins3.Visible = false;
            // 
            // buttonConfig3
            // 
            this.buttonConfig3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonConfig3.Location = new System.Drawing.Point(208, 0);
            this.buttonConfig3.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.buttonConfig3.Name = "buttonConfig3";
            this.buttonConfig3.Size = new System.Drawing.Size(357, 42);
            this.buttonConfig3.TabIndex = 33;
            this.buttonConfig3.Text = "Configure IMU";
            this.buttonConfig3.UseVisualStyleBackColor = true;
            this.buttonConfig3.Click += new System.EventHandler(this.buttonConfig3_Click);
            // 
            // IMU_3_LABEL
            // 
            this.IMU_3_LABEL.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.IMU_3_LABEL.AutoSize = true;
            this.IMU_3_LABEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IMU_3_LABEL.Location = new System.Drawing.Point(4, 6);
            this.IMU_3_LABEL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IMU_3_LABEL.Name = "IMU_3_LABEL";
            this.IMU_3_LABEL.Size = new System.Drawing.Size(81, 29);
            this.IMU_3_LABEL.TabIndex = 30;
            this.IMU_3_LABEL.Text = "IMU 3:";
            // 
            // tableLayoutPanelPins4
            // 
            this.tableLayoutPanelPins4.ColumnCount = 2;
            this.tableLayoutPanelPins4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelPins4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelPins4.Controls.Add(this.buttonConfig4, 0, 0);
            this.tableLayoutPanelPins4.Controls.Add(this.IMU_4_LABEL, 0, 0);
            this.tableLayoutPanelPins4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelPins4.Location = new System.Drawing.Point(4, 161);
            this.tableLayoutPanelPins4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanelPins4.Name = "tableLayoutPanelPins4";
            this.tableLayoutPanelPins4.RowCount = 1;
            this.tableLayoutPanelPins4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPins4.Size = new System.Drawing.Size(580, 43);
            this.tableLayoutPanelPins4.TabIndex = 34;
            this.tableLayoutPanelPins4.Visible = false;
            // 
            // buttonConfig4
            // 
            this.buttonConfig4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonConfig4.Location = new System.Drawing.Point(208, 0);
            this.buttonConfig4.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.buttonConfig4.Name = "buttonConfig4";
            this.buttonConfig4.Size = new System.Drawing.Size(357, 43);
            this.buttonConfig4.TabIndex = 33;
            this.buttonConfig4.Text = "Configure IMU";
            this.buttonConfig4.UseVisualStyleBackColor = true;
            this.buttonConfig4.Click += new System.EventHandler(this.buttonConfig4_Click);
            // 
            // IMU_4_LABEL
            // 
            this.IMU_4_LABEL.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.IMU_4_LABEL.AutoSize = true;
            this.IMU_4_LABEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IMU_4_LABEL.Location = new System.Drawing.Point(4, 7);
            this.IMU_4_LABEL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IMU_4_LABEL.Name = "IMU_4_LABEL";
            this.IMU_4_LABEL.Size = new System.Drawing.Size(81, 29);
            this.IMU_4_LABEL.TabIndex = 31;
            this.IMU_4_LABEL.Text = "IMU 4:";
            // 
            // tableLayoutPanelPins1
            // 
            this.tableLayoutPanelPins1.ColumnCount = 2;
            this.tableLayoutPanelPins1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelPins1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanelPins1.Controls.Add(this.IMU_1_LABEL, 0, 0);
            this.tableLayoutPanelPins1.Controls.Add(this.buttonConfig1, 1, 0);
            this.tableLayoutPanelPins1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelPins1.Location = new System.Drawing.Point(4, 5);
            this.tableLayoutPanelPins1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanelPins1.Name = "tableLayoutPanelPins1";
            this.tableLayoutPanelPins1.RowCount = 1;
            this.tableLayoutPanelPins1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelPins1.Size = new System.Drawing.Size(580, 42);
            this.tableLayoutPanelPins1.TabIndex = 35;
            this.tableLayoutPanelPins1.Visible = false;
            // 
            // IMU_1_LABEL
            // 
            this.IMU_1_LABEL.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.IMU_1_LABEL.AutoSize = true;
            this.IMU_1_LABEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IMU_1_LABEL.Location = new System.Drawing.Point(4, 6);
            this.IMU_1_LABEL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IMU_1_LABEL.Name = "IMU_1_LABEL";
            this.IMU_1_LABEL.Size = new System.Drawing.Size(81, 29);
            this.IMU_1_LABEL.TabIndex = 14;
            this.IMU_1_LABEL.Text = "IMU 1:";
            // 
            // buttonConfig1
            // 
            this.buttonConfig1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonConfig1.Location = new System.Drawing.Point(208, 0);
            this.buttonConfig1.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.buttonConfig1.Name = "buttonConfig1";
            this.buttonConfig1.Size = new System.Drawing.Size(357, 42);
            this.buttonConfig1.TabIndex = 15;
            this.buttonConfig1.Text = "Configure IMU";
            this.buttonConfig1.UseVisualStyleBackColor = true;
            this.buttonConfig1.Click += new System.EventHandler(this.buttonConfig1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel19);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(4, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(16, 18, 16, 18);
            this.groupBox2.Size = new System.Drawing.Size(596, 276);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Save File Details";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Controls.Add(this.tableLayoutPanel21, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.tableLayoutPanel20, 0, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(16, 55);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(564, 203);
            this.tableLayoutPanel19.TabIndex = 0;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 4;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.14094F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.85906F));
            this.tableLayoutPanel21.Controls.Add(this.trialNoTextBox, 3, 0);
            this.tableLayoutPanel21.Controls.Add(this.trialTypeDropBox, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel21.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.label15, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.comboBoxInstrumSide, 1, 1);
            this.tableLayoutPanel21.Controls.Add(this.label4, 2, 1);
            this.tableLayoutPanel21.Controls.Add(this.comboBoxSittingIncl, 3, 1);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(4, 78);
            this.tableLayoutPanel21.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(556, 120);
            this.tableLayoutPanel21.TabIndex = 14;
            // 
            // trialNoTextBox
            // 
            this.trialNoTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trialNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trialNoTextBox.Location = new System.Drawing.Point(435, 0);
            this.trialNoTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.trialNoTextBox.Name = "trialNoTextBox";
            this.trialNoTextBox.Size = new System.Drawing.Size(121, 53);
            this.trialNoTextBox.TabIndex = 10;
            // 
            // trialTypeDropBox
            // 
            this.trialTypeDropBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trialTypeDropBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.trialTypeDropBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trialTypeDropBox.FormattingEnabled = true;
            this.trialTypeDropBox.Items.AddRange(new object[] {
            "Patient Trial",
            "Test Trial"});
            this.trialTypeDropBox.Location = new System.Drawing.Point(160, 0);
            this.trialTypeDropBox.Margin = new System.Windows.Forms.Padding(0);
            this.trialTypeDropBox.Name = "trialTypeDropBox";
            this.trialTypeDropBox.Size = new System.Drawing.Size(125, 54);
            this.trialTypeDropBox.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(300, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 60);
            this.label2.TabIndex = 15;
            this.label2.Text = "Trail Number: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 60);
            this.label3.TabIndex = 12;
            this.label3.Text = "Test Type: ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(0, 60);
            this.label15.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(156, 60);
            this.label15.TabIndex = 13;
            this.label15.Text = "Instrumented Side:";
            // 
            // comboBoxInstrumSide
            // 
            this.comboBoxInstrumSide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxInstrumSide.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInstrumSide.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxInstrumSide.FormattingEnabled = true;
            this.comboBoxInstrumSide.Items.AddRange(new object[] {
            "Right",
            "Left"});
            this.comboBoxInstrumSide.Location = new System.Drawing.Point(160, 60);
            this.comboBoxInstrumSide.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxInstrumSide.Name = "comboBoxInstrumSide";
            this.comboBoxInstrumSide.Size = new System.Drawing.Size(125, 54);
            this.comboBoxInstrumSide.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(300, 60);
            this.label4.Margin = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 60);
            this.label4.TabIndex = 16;
            this.label4.Text = "Sitting\r\nInclination:";
            // 
            // comboBoxSittingIncl
            // 
            this.comboBoxSittingIncl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxSittingIncl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSittingIncl.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSittingIncl.FormattingEnabled = true;
            this.comboBoxSittingIncl.Items.AddRange(new object[] {
            "0",
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "70",
            "80",
            "90"});
            this.comboBoxSittingIncl.Location = new System.Drawing.Point(435, 60);
            this.comboBoxSittingIncl.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxSittingIncl.Name = "comboBoxSittingIncl";
            this.comboBoxSittingIncl.Size = new System.Drawing.Size(121, 54);
            this.comboBoxSittingIncl.TabIndex = 17;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel20.Controls.Add(this.filePathTextBox, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(4, 5);
            this.tableLayoutPanel20.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(556, 63);
            this.tableLayoutPanel20.TabIndex = 0;
            // 
            // filePathTextBox
            // 
            this.filePathTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filePathTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filePathTextBox.Location = new System.Drawing.Point(200, 0);
            this.filePathTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.filePathTextBox.Name = "filePathTextBox";
            this.filePathTextBox.Size = new System.Drawing.Size(676, 44);
            this.filePathTextBox.TabIndex = 7;
            this.filePathTextBox.Text = "Raw_Data_Files\\";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 63);
            this.label1.TabIndex = 8;
            this.label1.Text = "File Path: ";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(614, 5);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.31551F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.68449F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(602, 548);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 237);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0, 0, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(18);
            this.groupBox1.Size = new System.Drawing.Size(598, 306);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Record Task";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.taskComboBox, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.taskStateButton, 0, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(18, 55);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.27273F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(562, 233);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // taskComboBox
            // 
            this.taskComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taskComboBox.DropDownHeight = 512;
            this.taskComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.taskComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskComboBox.FormattingEnabled = true;
            this.taskComboBox.IntegralHeight = false;
            this.taskComboBox.ItemHeight = 64;
            this.taskComboBox.Location = new System.Drawing.Point(3, 24);
            this.taskComboBox.Name = "taskComboBox";
            this.taskComboBox.Size = new System.Drawing.Size(556, 72);
            this.taskComboBox.TabIndex = 0;
            // 
            // taskStateButton
            // 
            this.taskStateButton.AutoSize = true;
            this.taskStateButton.BackColor = System.Drawing.Color.Green;
            this.taskStateButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.taskStateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskStateButton.Location = new System.Drawing.Point(0, 105);
            this.taskStateButton.Margin = new System.Windows.Forms.Padding(0);
            this.taskStateButton.Name = "taskStateButton";
            this.taskStateButton.Size = new System.Drawing.Size(562, 105);
            this.taskStateButton.TabIndex = 1;
            this.taskStateButton.Text = "Start of Task";
            this.taskStateButton.UseVisualStyleBackColor = false;
            this.taskStateButton.Click += new System.EventHandler(this.taskStateButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel7);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(4, 5);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(18);
            this.groupBox3.Size = new System.Drawing.Size(594, 227);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Save Data";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.AutoScroll = true;
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.14607F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.85393F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 364F));
            this.tableLayoutPanel7.Controls.Add(this.stopSaveButton, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.saveDataButton, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.textBoxTaskTimer, 2, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(18, 55);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel7.MinimumSize = new System.Drawing.Size(225, 77);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.44444F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(558, 154);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // stopSaveButton
            // 
            this.stopSaveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stopSaveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopSaveButton.Location = new System.Drawing.Point(4, 90);
            this.stopSaveButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.stopSaveButton.MinimumSize = new System.Drawing.Size(75, 0);
            this.stopSaveButton.Name = "stopSaveButton";
            this.stopSaveButton.Size = new System.Drawing.Size(153, 59);
            this.stopSaveButton.TabIndex = 1;
            this.stopSaveButton.Text = "Stop Save";
            this.stopSaveButton.UseVisualStyleBackColor = true;
            this.stopSaveButton.Click += new System.EventHandler(this.stopSaveButton_Click);
            // 
            // saveDataButton
            // 
            this.saveDataButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveDataButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveDataButton.Location = new System.Drawing.Point(4, 5);
            this.saveDataButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.saveDataButton.MinimumSize = new System.Drawing.Size(75, 0);
            this.saveDataButton.Name = "saveDataButton";
            this.saveDataButton.Size = new System.Drawing.Size(153, 58);
            this.saveDataButton.TabIndex = 0;
            this.saveDataButton.Text = "Start Save";
            this.saveDataButton.UseVisualStyleBackColor = true;
            this.saveDataButton.Click += new System.EventHandler(this.saveDataButton_Click);
            // 
            // textBoxTaskTimer
            // 
            this.textBoxTaskTimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTaskTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTaskTimer.Location = new System.Drawing.Point(197, 5);
            this.textBoxTaskTimer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBoxTaskTimer.Name = "textBoxTaskTimer";
            this.tableLayoutPanel7.SetRowSpan(this.textBoxTaskTimer, 3);
            this.textBoxTaskTimer.Size = new System.Drawing.Size(357, 116);
            this.textBoxTaskTimer.TabIndex = 2;
            this.textBoxTaskTimer.Text = "0";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel8);
            this.tabPage3.Location = new System.Drawing.Point(4, 45);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(18);
            this.tabPage3.Size = new System.Drawing.Size(1256, 594);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Simulation";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.125F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.875F));
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.paintPanel, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(18, 18);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1220, 558);
            this.tableLayoutPanel8.TabIndex = 6;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AutoScroll = true;
            this.tableLayoutPanel9.AutoSize = true;
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.stopVisButton, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.avatarSimButton, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.cubeSimButton, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.accSimButton, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.magSimButton, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.gyrSimButton, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.adcSimButton, 0, 3);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(4, 5);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel9.MinimumSize = new System.Drawing.Size(225, 462);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 7;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(237, 548);
            this.tableLayoutPanel9.TabIndex = 6;
            // 
            // stopVisButton
            // 
            this.stopVisButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stopVisButton.Location = new System.Drawing.Point(0, 480);
            this.stopVisButton.Margin = new System.Windows.Forms.Padding(0, 12, 12, 12);
            this.stopVisButton.Name = "stopVisButton";
            this.stopVisButton.Size = new System.Drawing.Size(225, 56);
            this.stopVisButton.TabIndex = 6;
            this.stopVisButton.Text = "Stop Visualisation";
            this.stopVisButton.UseVisualStyleBackColor = true;
            this.stopVisButton.Click += new System.EventHandler(this.stopVisButton_Click);
            // 
            // avatarSimButton
            // 
            this.avatarSimButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.avatarSimButton.Location = new System.Drawing.Point(0, 402);
            this.avatarSimButton.Margin = new System.Windows.Forms.Padding(0, 12, 12, 12);
            this.avatarSimButton.Name = "avatarSimButton";
            this.avatarSimButton.Size = new System.Drawing.Size(225, 54);
            this.avatarSimButton.TabIndex = 5;
            this.avatarSimButton.Text = "Avatar";
            this.avatarSimButton.UseVisualStyleBackColor = true;
            this.avatarSimButton.Click += new System.EventHandler(this.avatarSimButton_Click);
            // 
            // cubeSimButton
            // 
            this.cubeSimButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cubeSimButton.Location = new System.Drawing.Point(0, 324);
            this.cubeSimButton.Margin = new System.Windows.Forms.Padding(0, 12, 12, 12);
            this.cubeSimButton.Name = "cubeSimButton";
            this.cubeSimButton.Size = new System.Drawing.Size(225, 54);
            this.cubeSimButton.TabIndex = 4;
            this.cubeSimButton.Text = "Simulate Cube";
            this.cubeSimButton.UseVisualStyleBackColor = true;
            this.cubeSimButton.Click += new System.EventHandler(this.cubeSimButton_Click);
            // 
            // accSimButton
            // 
            this.accSimButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accSimButton.Location = new System.Drawing.Point(0, 11);
            this.accSimButton.Margin = new System.Windows.Forms.Padding(0, 11, 10, 11);
            this.accSimButton.Name = "accSimButton";
            this.accSimButton.Size = new System.Drawing.Size(227, 56);
            this.accSimButton.TabIndex = 0;
            this.accSimButton.Text = "Simulate Accelerometer";
            this.accSimButton.UseVisualStyleBackColor = true;
            this.accSimButton.Click += new System.EventHandler(this.accSimButton_Click);
            // 
            // magSimButton
            // 
            this.magSimButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.magSimButton.Location = new System.Drawing.Point(0, 168);
            this.magSimButton.Margin = new System.Windows.Forms.Padding(0, 12, 12, 12);
            this.magSimButton.Name = "magSimButton";
            this.magSimButton.Size = new System.Drawing.Size(225, 54);
            this.magSimButton.TabIndex = 2;
            this.magSimButton.Text = "Simulate Magnetometer";
            this.magSimButton.UseVisualStyleBackColor = true;
            this.magSimButton.Click += new System.EventHandler(this.magSimButton_Click);
            // 
            // gyrSimButton
            // 
            this.gyrSimButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gyrSimButton.Location = new System.Drawing.Point(0, 89);
            this.gyrSimButton.Margin = new System.Windows.Forms.Padding(0, 11, 10, 11);
            this.gyrSimButton.Name = "gyrSimButton";
            this.gyrSimButton.Size = new System.Drawing.Size(227, 56);
            this.gyrSimButton.TabIndex = 1;
            this.gyrSimButton.Text = "Simulate Gyroscope";
            this.gyrSimButton.UseVisualStyleBackColor = true;
            this.gyrSimButton.Click += new System.EventHandler(this.gyrSimButton_Click);
            // 
            // adcSimButton
            // 
            this.adcSimButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adcSimButton.Location = new System.Drawing.Point(0, 246);
            this.adcSimButton.Margin = new System.Windows.Forms.Padding(0, 12, 12, 12);
            this.adcSimButton.Name = "adcSimButton";
            this.adcSimButton.Size = new System.Drawing.Size(225, 54);
            this.adcSimButton.TabIndex = 3;
            this.adcSimButton.Text = "Simulate ADC";
            this.adcSimButton.UseVisualStyleBackColor = true;
            this.adcSimButton.Click += new System.EventHandler(this.adcSimButton_Click);
            // 
            // paintPanel
            // 
            this.paintPanel.AutoSize = true;
            this.paintPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paintPanel.Controls.Add(this.tableLayoutPanelAngle);
            this.paintPanel.Controls.Add(this.tableLayoutPanelRot);
            this.paintPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paintPanel.Location = new System.Drawing.Point(248, 3);
            this.paintPanel.Name = "paintPanel";
            this.paintPanel.Padding = new System.Windows.Forms.Padding(0, 0, 16, 0);
            this.paintPanel.Size = new System.Drawing.Size(969, 552);
            this.paintPanel.TabIndex = 7;
            // 
            // tableLayoutPanelAngle
            // 
            this.tableLayoutPanelAngle.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelAngle.ColumnCount = 2;
            this.tableLayoutPanelAngle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelAngle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelAngle.Controls.Add(this.labelShoulder, 0, 0);
            this.tableLayoutPanelAngle.Controls.Add(this.labelElbow, 1, 0);
            this.tableLayoutPanelAngle.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanelAngle.Controls.Add(this.tableLayoutPanel11, 1, 1);
            this.tableLayoutPanelAngle.Location = new System.Drawing.Point(30, 394);
            this.tableLayoutPanelAngle.Name = "tableLayoutPanelAngle";
            this.tableLayoutPanelAngle.RowCount = 2;
            this.tableLayoutPanelAngle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.44715F));
            this.tableLayoutPanelAngle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.55285F));
            this.tableLayoutPanelAngle.Size = new System.Drawing.Size(426, 208);
            this.tableLayoutPanelAngle.TabIndex = 2;
            this.tableLayoutPanelAngle.Visible = false;
            // 
            // labelShoulder
            // 
            this.labelShoulder.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelShoulder.AutoSize = true;
            this.labelShoulder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShoulder.Location = new System.Drawing.Point(4, 6);
            this.labelShoulder.Name = "labelShoulder";
            this.labelShoulder.Size = new System.Drawing.Size(170, 20);
            this.labelShoulder.TabIndex = 0;
            this.labelShoulder.Text = "UA Angle and Coords";
            // 
            // labelElbow
            // 
            this.labelElbow.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelElbow.AutoSize = true;
            this.labelElbow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelElbow.Location = new System.Drawing.Point(216, 6);
            this.labelElbow.Name = "labelElbow";
            this.labelElbow.Size = new System.Drawing.Size(168, 20);
            this.labelElbow.TabIndex = 1;
            this.labelElbow.Text = "LA Angle and Coords";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.14706F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.85294F));
            this.tableLayoutPanel10.Controls.Add(this.label16, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.labelShoulderMag, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label13, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.label11, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.labelShoulderY, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.labelShoulderZ, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.labelShoulderX, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.labelShoulderTwist, 1, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(4, 36);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 5;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(205, 168);
            this.tableLayoutPanel10.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(3, 4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 25);
            this.label16.TabIndex = 8;
            this.label16.Text = "MA:";
            // 
            // labelShoulderMag
            // 
            this.labelShoulderMag.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelShoulderMag.AutoSize = true;
            this.labelShoulderMag.Location = new System.Drawing.Point(64, 0);
            this.labelShoulderMag.Name = "labelShoulderMag";
            this.labelShoulderMag.Size = new System.Drawing.Size(0, 34);
            this.labelShoulderMag.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 139);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 25);
            this.label13.TabIndex = 4;
            this.label13.Text = "Z:";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 106);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 25);
            this.label11.TabIndex = 2;
            this.label11.Text = "Y:";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 25);
            this.label8.TabIndex = 0;
            this.label8.Text = "X:";
            // 
            // labelShoulderY
            // 
            this.labelShoulderY.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelShoulderY.AutoSize = true;
            this.labelShoulderY.Location = new System.Drawing.Point(64, 102);
            this.labelShoulderY.Name = "labelShoulderY";
            this.labelShoulderY.Size = new System.Drawing.Size(0, 34);
            this.labelShoulderY.TabIndex = 6;
            // 
            // labelShoulderZ
            // 
            this.labelShoulderZ.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelShoulderZ.AutoSize = true;
            this.labelShoulderZ.Location = new System.Drawing.Point(64, 136);
            this.labelShoulderZ.Name = "labelShoulderZ";
            this.labelShoulderZ.Size = new System.Drawing.Size(0, 32);
            this.labelShoulderZ.TabIndex = 7;
            // 
            // labelShoulderX
            // 
            this.labelShoulderX.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelShoulderX.AutoSize = true;
            this.labelShoulderX.Location = new System.Drawing.Point(64, 68);
            this.labelShoulderX.Name = "labelShoulderX";
            this.labelShoulderX.Size = new System.Drawing.Size(0, 34);
            this.labelShoulderX.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(4, 38);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 25);
            this.label18.TabIndex = 10;
            this.label18.Text = "TA:";
            // 
            // labelShoulderTwist
            // 
            this.labelShoulderTwist.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelShoulderTwist.AutoSize = true;
            this.labelShoulderTwist.Location = new System.Drawing.Point(65, 34);
            this.labelShoulderTwist.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelShoulderTwist.Name = "labelShoulderTwist";
            this.labelShoulderTwist.Size = new System.Drawing.Size(0, 34);
            this.labelShoulderTwist.TabIndex = 11;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.19708F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.80292F));
            this.tableLayoutPanel11.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.labelElbowMag, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.label14, 0, 4);
            this.tableLayoutPanel11.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.labelElbowZ, 1, 4);
            this.tableLayoutPanel11.Controls.Add(this.labelElbowY, 1, 3);
            this.tableLayoutPanel11.Controls.Add(this.labelElbowX, 1, 2);
            this.tableLayoutPanel11.Controls.Add(this.label19, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.labelElbowTwist, 1, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(216, 36);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 5;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(206, 168);
            this.tableLayoutPanel11.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(3, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 25);
            this.label17.TabIndex = 9;
            this.label17.Text = "MA:";
            // 
            // labelElbowMag
            // 
            this.labelElbowMag.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelElbowMag.AutoSize = true;
            this.labelElbowMag.Location = new System.Drawing.Point(63, 0);
            this.labelElbowMag.Name = "labelElbowMag";
            this.labelElbowMag.Size = new System.Drawing.Size(0, 34);
            this.labelElbowMag.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 139);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 25);
            this.label14.TabIndex = 3;
            this.label14.Text = "Z:";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 25);
            this.label12.TabIndex = 2;
            this.label12.Text = "Y:";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 25);
            this.label10.TabIndex = 1;
            this.label10.Text = "X:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelElbowZ
            // 
            this.labelElbowZ.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelElbowZ.AutoSize = true;
            this.labelElbowZ.Location = new System.Drawing.Point(63, 136);
            this.labelElbowZ.Name = "labelElbowZ";
            this.labelElbowZ.Size = new System.Drawing.Size(0, 32);
            this.labelElbowZ.TabIndex = 6;
            // 
            // labelElbowY
            // 
            this.labelElbowY.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelElbowY.AutoSize = true;
            this.labelElbowY.Location = new System.Drawing.Point(63, 102);
            this.labelElbowY.Name = "labelElbowY";
            this.labelElbowY.Size = new System.Drawing.Size(0, 34);
            this.labelElbowY.TabIndex = 5;
            // 
            // labelElbowX
            // 
            this.labelElbowX.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelElbowX.AutoSize = true;
            this.labelElbowX.Location = new System.Drawing.Point(63, 68);
            this.labelElbowX.Name = "labelElbowX";
            this.labelElbowX.Size = new System.Drawing.Size(0, 34);
            this.labelElbowX.TabIndex = 4;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(4, 38);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 25);
            this.label19.TabIndex = 11;
            this.label19.Text = "TA:";
            // 
            // labelElbowTwist
            // 
            this.labelElbowTwist.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelElbowTwist.AutoSize = true;
            this.labelElbowTwist.Location = new System.Drawing.Point(64, 34);
            this.labelElbowTwist.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelElbowTwist.Name = "labelElbowTwist";
            this.labelElbowTwist.Size = new System.Drawing.Size(0, 34);
            this.labelElbowTwist.TabIndex = 12;
            // 
            // tableLayoutPanelRot
            // 
            this.tableLayoutPanelRot.ColumnCount = 4;
            this.tableLayoutPanelRot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanelRot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tableLayoutPanelRot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanelRot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanelRot.Controls.Add(this.buttonRotLeft, 2, 0);
            this.tableLayoutPanelRot.Controls.Add(this.buttonRotRight, 3, 0);
            this.tableLayoutPanelRot.Controls.Add(this.panel5, 1, 0);
            this.tableLayoutPanelRot.Controls.Add(this.buttonTare, 0, 0);
            this.tableLayoutPanelRot.Location = new System.Drawing.Point(30, 28);
            this.tableLayoutPanelRot.Name = "tableLayoutPanelRot";
            this.tableLayoutPanelRot.Padding = new System.Windows.Forms.Padding(3);
            this.tableLayoutPanelRot.RowCount = 1;
            this.tableLayoutPanelRot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRot.Size = new System.Drawing.Size(438, 92);
            this.tableLayoutPanelRot.TabIndex = 1;
            this.tableLayoutPanelRot.Visible = false;
            // 
            // buttonRotLeft
            // 
            this.buttonRotLeft.Location = new System.Drawing.Point(161, 6);
            this.buttonRotLeft.Name = "buttonRotLeft";
            this.buttonRotLeft.Size = new System.Drawing.Size(130, 80);
            this.buttonRotLeft.TabIndex = 7;
            this.buttonRotLeft.Text = "Rotate Left";
            this.buttonRotLeft.UseVisualStyleBackColor = true;
            // 
            // buttonRotRight
            // 
            this.buttonRotRight.Location = new System.Drawing.Point(299, 6);
            this.buttonRotRight.Name = "buttonRotRight";
            this.buttonRotRight.Size = new System.Drawing.Size(132, 80);
            this.buttonRotRight.TabIndex = 6;
            this.buttonRotRight.Text = "Rotate Right";
            this.buttonRotRight.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(147, 3);
            this.panel5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(5, 86);
            this.panel5.TabIndex = 8;
            // 
            // buttonTare
            // 
            this.buttonTare.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonTare.Location = new System.Drawing.Point(6, 6);
            this.buttonTare.Name = "buttonTare";
            this.buttonTare.Size = new System.Drawing.Size(132, 80);
            this.buttonTare.TabIndex = 9;
            this.buttonTare.Text = "Tare";
            this.buttonTare.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panelTabContainer);
            this.panel9.Controls.Add(this.statusStrip);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1284, 702);
            this.panel9.TabIndex = 5;
            // 
            // panelTabContainer
            // 
            this.panelTabContainer.Controls.Add(this.tabControl);
            this.panelTabContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTabContainer.Location = new System.Drawing.Point(0, 0);
            this.panelTabContainer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panelTabContainer.Name = "panelTabContainer";
            this.panelTabContainer.Padding = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.panelTabContainer.Size = new System.Drawing.Size(1284, 661);
            this.panelTabContainer.TabIndex = 7;
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1284, 702);
            this.Controls.Add(this.panel9);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "GUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NU GUI SAVER";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.bluetoothConnect.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.connectionStatusGroupBox.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.DeviceTypeBox.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.groupBoxPins.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanelPins2.ResumeLayout(false);
            this.tableLayoutPanelPins2.PerformLayout();
            this.tableLayoutPanelPins3.ResumeLayout(false);
            this.tableLayoutPanelPins3.PerformLayout();
            this.tableLayoutPanelPins4.ResumeLayout(false);
            this.tableLayoutPanelPins4.PerformLayout();
            this.tableLayoutPanelPins1.ResumeLayout(false);
            this.tableLayoutPanelPins1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.paintPanel.ResumeLayout(false);
            this.tableLayoutPanelAngle.ResumeLayout(false);
            this.tableLayoutPanelAngle.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanelRot.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panelTabContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.ComponentModel.BackgroundWorker backgroundWorkerScan;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox DeviceTypeBox;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox connectionStatusGroupBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label SaveDataRateLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label LineSavedLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label decodedPacketsLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox bluetoothConnect;
        private System.Windows.Forms.Button bluetoothScanButton;
        private System.Windows.Forms.Button bluetoothConnectButton;
        private System.Windows.Forms.ListBox Available_Devices_ListBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button setCalibrationButton;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.ListBox Connected_Devices_ListBox;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button stopVisButton;
        private System.Windows.Forms.Button avatarSimButton;
        private System.Windows.Forms.Button cubeSimButton;
        private System.Windows.Forms.Button accSimButton;
        private System.Windows.Forms.Button magSimButton;
        private System.Windows.Forms.Button gyrSimButton;
        private System.Windows.Forms.Button adcSimButton;
        private System.Windows.Forms.Panel paintPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelAngle;
        private System.Windows.Forms.Label labelShoulder;
        private System.Windows.Forms.Label labelElbow;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelShoulderX;
        private System.Windows.Forms.Label labelShoulderY;
        private System.Windows.Forms.Label labelShoulderZ;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelElbowX;
        private System.Windows.Forms.Label labelElbowY;
        private System.Windows.Forms.Label labelElbowZ;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRot;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.GroupBox groupBoxPins;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button stopSaveButton;
        private System.Windows.Forms.TextBox textBoxTaskTimer;
        private System.Windows.Forms.Button saveDataButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPins2;
        private System.Windows.Forms.Label IMU_2_LABEL;
        private System.Windows.Forms.Button buttonConfig2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPins3;
        private System.Windows.Forms.Button buttonConfig3;
        private System.Windows.Forms.Label IMU_3_LABEL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPins4;
        private System.Windows.Forms.Button buttonConfig4;
        private System.Windows.Forms.Label IMU_4_LABEL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelPins1;
        private System.Windows.Forms.Label IMU_1_LABEL;
        private System.Windows.Forms.Button buttonConfig1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.TextBox filePathTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox trialNoTextBox;
        private System.Windows.Forms.ComboBox trialTypeDropBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBoxInstrumSide;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonRotLeft;
        private System.Windows.Forms.Button buttonRotRight;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button buttonTare;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelShoulderMag;
        private System.Windows.Forms.Label labelElbowMag;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label labelShoulderTwist;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelElbowTwist;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label packetDecodeRateLabel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxSittingIncl;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ComboBox taskComboBox;
        private System.Windows.Forms.Button taskStateButton;
        private System.Windows.Forms.Panel panelTabContainer;
    }
}

