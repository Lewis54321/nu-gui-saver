﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms.Layout;
using InTheHand.Net.Bluetooth;
using InTheHand.Net;
using InTheHand.Net.Sockets;
using NU_GUI_Saver.Classes;

namespace NU_GUI_Saver
{
    class DataSaver
    {
        #region Variables

        /// <summary>
        /// Passed bluetoothConnect object of the last connected device 
        /// in order to be able to monitor connect_flag and saveDataFlag status
        /// </summary>
        private readonly BluetoothConnect _bluetoothConnection;

        /// <summary>
        /// StringBuilder for the header of the csv file
        /// </summary>
        private readonly StringBuilder _stringBuilderHeader = new StringBuilder();

        /// <summary>
        /// StringBuilder for all subsequent packet writes
        /// </summary>
        private readonly StringBuilder _stringBuilderUpdate = new StringBuilder();

        /// <summary>
        /// This is reference to the saveFileDetails class which contains all the header information for the csv file
        /// </summary>
        private readonly SaveFileDetails _saveFileDetails;

        /// <summary>
        /// Flag triggered by button in the GUI which stops data from being saved
        /// </summary>
        public bool SaveDataFlag = false;

        /// <summary>
        /// Contains the decoded packet when it is sent over by the packet decoded event in the PacketDecoder class object
        /// </summary>
        //private List<int[]> _dataDict;
        private Dictionary<string, int[]> _dataDict;

        /// <summary>
        /// Lock used to prevent multiple packetDecoder class instances from accessing csv file simultaneously
        /// </summary>
        private readonly Object _thisLock = new Object();

        private int _count;

        #endregion

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="bluetoothConnection"></param>
        /// <param name="saveFileDetails"></param>
        public DataSaver(BluetoothConnect bluetoothConnection, SaveFileDetails saveFileDetails)
        {
            this._bluetoothConnection = bluetoothConnection;
            this._saveFileDetails = saveFileDetails;
        }

        #region Methods

        /// <summary>
        /// First method called in the class object by the background worker. Initialises the CSV file to save to
        /// </summary>
        private void Save()
        {
            if (_bluetoothConnection.ConnectFlag)
            {
                InitialiseFile();
            }
        }

        /// <summary>
        /// Method for creating CSV file and using class <see cref="SaveFileDetails"/> information to write the file header using information
        /// provided in the GUI
        /// </summary>
        private void InitialiseFile()
        {
            var file = File.Create(_saveFileDetails.FullFilePath);
            file.Close();

            _stringBuilderHeader.AppendLine("HeaderLine1," + _saveFileDetails.FileHeader);
            _stringBuilderHeader.AppendLine();
            _stringBuilderHeader.AppendLine("TaskLine1,Task 0," + _saveFileDetails.TaskNums);
            _stringBuilderHeader.AppendLine("TaskLine2,No Task," + _saveFileDetails.TasksString);
            _stringBuilderHeader.AppendLine();
            _stringBuilderHeader.AppendLine("ImuConfigTitle," + SaveFileDetails.ImuConfigHeader);
            int count = 0;
            foreach (var imuConfigLine in _saveFileDetails.ImuConfigLineString)
            {
                count++;
                _stringBuilderHeader.AppendLine("ImuConfig" + count + "," + imuConfigLine);
            }

            _stringBuilderHeader.AppendLine(SaveFileDetails.FileColumnHeaders);

            File.AppendAllText(_saveFileDetails.FullFilePath, _stringBuilderHeader.ToString());
            _stringBuilderHeader.Clear();

            file.Close();

            SaveDataFlag = true;

            OnDataSaverStateChange(new UpdatedStatusEventArgs("Saving Data..."));
        }

        /// <summary>
        /// Method called by OnPacketDecoded event when packet has been decoded.
        /// Calls method and triggers event which signifies to GUI that a line has been saved
        /// Write row method takes as argument the time stamp and a dict containing the packet of data
        /// </summary>
        private void SaveData()
        {
            if (_bluetoothConnection.ConnectFlag && this.SaveDataFlag == true)
            {
                //WriteRow(_clock.UtcNow, _dataDict);
                WriteRow(_dataDict);
                OnLineSaved(new EventArgs());              
            }
            else
            {
                OnDataSaverStateChange(new UpdatedStatusEventArgs("Stopped Saving Data..."));
            }                 
        }

        /// <summary>
        /// Method called by SaveData method when packet received, which passes the list of packet details, ADC and IMU data, as 
        /// well as time stamp provided by the clock
        /// dataList is a dict which may contains an array of ADC data and possibly an array of IMU data
        /// </summary>
        /// <param name="dataList"></param>
        private void WriteRow(Dictionary<string, int[]> dataList)
        {
            lock (_thisLock)
            {
                // Loop through each array (ADC/IMU) in the packet
                // Want to append each array to the string builder and seperate with a comma (except the final array)
                foreach (int[] packetArray in dataList.Values)
                {
                    int count = 0;
                    foreach (int packet in packetArray)
                    {
                        if (count != packetArray.Length)
                        {
                            _stringBuilderUpdate.Append(packet + ",");
                            count++;
                        }
                        else
                        {
                            _stringBuilderUpdate.Append(packet);
                        }
                    }
                }
                _stringBuilderUpdate.Append("\r\n");
                this._count += 1;

                // Write to the CSV file every 10 lines (to avoid computational load associated with opening and closing file)
                if (this._count == 10)
                {
                    using (var streamWriter = File.AppendText(_saveFileDetails.FullFilePath))
                    {
                        streamWriter.Write(_stringBuilderUpdate);
                    }
                    _stringBuilderUpdate.Clear();
                    this._count = 0;
                }                
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Background worker event handler called by GUI to be run on a seperate thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Save();
        }

        /// <summary>
        /// Packet decoded event handler set up in GUI file with an event which is triggered in 
        /// the Packet Decoder class object (every time a packet is decoded)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnPacketDecoded(object sender, PacketDecodedEventArgs e)
        {
            _dataDict = e.DecodedPacket;
            SaveData();
        }

        #endregion

        #region Events
        
        /// <summary>
        /// Event occurs when there is a state change (start saving or stop saving data)
        /// Event triggers event handler in the GUI to update text
        /// </summary>
        public event EventHandler<UpdatedStatusEventArgs> DataSaverStateChange;
        protected virtual void OnDataSaverStateChange(UpdatedStatusEventArgs e)
        {
            DataSaverStateChange?.Invoke(this, e);
        }

        /// <summary>
        /// Event occurs whenever a line is saved
        /// Event triggers event handler in the GUI to update textbox
        /// </summary>
        public event EventHandler<EventArgs> LineSaved;
        protected virtual void OnLineSaved(EventArgs e)
        {
            LineSaved?.Invoke(this, e);
        }

        #endregion
    }
}
