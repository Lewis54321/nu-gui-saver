﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    class TestDetails
    {
        /// <summary>
        /// Number of tasks user could select from (calculated in GUI)
        /// </summary>
        public int NoTasks { get; private set; }

        /// <summary>
        /// A string combination of all tasks that user could choose from (i.e. Task 1, Task 2,...)
        /// </summary>
        public string TaskNums { get; private set; }

        /// <summary>
        /// A string combination of all the task names
        /// </summary>
        public string TasksString { get; private set; }

        /// <summary>
        /// File path to save the CSV file
        /// </summary>
        public string FilePath { get; private set; }

        /// <summary>
        /// Trial type selected by the user
        /// </summary>
        public readonly string TrialType;

        /// <summary>
        /// Trial number selected by the user
        /// </summary>
        public string TrialNo { get; private set; }

        /// <summary>
        /// Task set (fullFMA or partFMA) selected by the user
        /// </summary>
        public string TaskSet;

        /// <summary>
        /// The side of the subject the instrumentation is attached
        /// </summary>
        public string InstrumentedSide { get; set; }

        /// <summary>
        /// The sitting inclination of the subject
        /// </summary>
        public string SittingIncl { get; set; }

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="trialType"></param>
        /// <param name="trialNo"></param>
        /// <param name="instrumentedSide"></param>
        public TestDetails(string filePath, string trialType, string trialNo, string instrumentedSide, string sittingIncl)
        {
            FilePath = filePath;
            TrialType = trialType;
            TrialNo = trialNo;
            InstrumentedSide = instrumentedSide;
            SittingIncl = sittingIncl;
        }

        /// <summary>
        /// The full list of tasks from the FMA
        /// </summary>
        public readonly List<string> fullFMA = new List<string>()
        {
            "CAL-Tare IMUs (semi-pronated arm vertical)",
            "CAL-Tare IMUs (semi-pronate arm 90 degrees from vertical)",
            //"MVC-Biceps",
            //"MVC-Flexor Digitorum Superficialis",
            "Flexor Synergy",
            "Extensor Synergy",
            "Hand to Lumbar Spine",
            "Shoulder Flexion (0-90)",
            "Pronation-Supination (elbow at 90)",
            "Shoulder Abduction",
            "Shoulder Flexion (90-180)",
            "Pronation-Supination (elbow at 0)",
            //"Normal Reflex Activity",
            "Stability at 15 Dorsiflexion (elbow at 90)",
            "Repeated Dorsiflexion-Volar Flexion (elbow at 90)",
            "Stability at 15 Dorsiflexion (elbow at 0)",
            "Repeated Dorsiflexion-Volar Flexion (elbow at 0)",
            "Circumduction",
            "Mass Flexion",
            "Mass Extension",
            "Hook Grasp",
            "Thumb Adduction",
            "Pincer Grasp-opposition",
            "Cylinder Grasp",
            "Spherical Grasp",
            "Coordination-Speed"
        };

        /// <summary>
        /// Reduced set of tasks from the FMA
        /// </summary>
        public readonly List<string> partFMA = new List<string>()
        {
            "Flexor Synergy",
            "Extensor Synergy",
            "Spherical Grasp"
        };

        public readonly List<string> taskList = new List<string>()
        {
            "Task 1 Active",
            "Task 1 Passive",
            "Task 2 Active",
            "Task 2 Passive",
            "Task 3 Active",
            "Task 3 Passive",
            "Task 4 Active",
            "Task 4 Passive",
            "Task 5 Active",
            "Task 5 Passive",
            "Task 6 Active",
            "Task 6 Passive",
            "Task 7 Active",
            "Task 7 Passive",
            "Task 8 Active",
            "Task 8 Passive",
            "Task 9 Active",
            "Task 9 Passive",
            "Task 10 Active",
            "Task 10 Passive",
            "Task 11 Active",
            "Task 11 Passive",
            "Task 12 Active",
            "Task 12 Passive",
            "Task 13 Active",
            "Task 13 Passive",
            "Task 14 Active",
            "Task 14 Passive",
            "Task 15 Active",
            "Task 15 Passive",
        };

        /// <summary>
        /// Selects the appropriate FMA task set depending on the user input
        /// Also converts the list of tasks into a comma seperated string for use in the csv header
        /// </summary>
        /// <param name="taskSet"></param>
        public void TasksToString(string taskSet)
        {
            TaskSet = taskSet;
            List<string> taskSetList;

            if (taskSet.Equals("Full FM"))
            {
                taskSetList = fullFMA;
            }
            else
            {
                taskSetList = taskList;
            }

            int count = 0;
            NoTasks = taskSetList.Count() + 1;

            foreach (string uniqueTask in taskSetList)
            {
                count++;
                TaskNums += String.Format("Task {0},", count);
                TasksString += uniqueTask + ",";
            }
        }

    }
}
