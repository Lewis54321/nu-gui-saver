﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms.Layout;

namespace NU_GUI_Saver
{
    class SaveFileDetails
    {
        #region Variables

        // **************** CONSTRUCTOR FIELDS ******************** //

        /// <summary>
        /// test details object passed in the constructor
        /// </summary>
        private readonly TestDetails _testDetails;

        /// <summary>
        /// Collection of names for each connected IMU
        /// </summary>
        private readonly List<string> _allBluetoothConnectionNames;

        /// <summary>
        /// Number of imus connected calculated from the count of all bluetooth device names
        /// </summary>
        private readonly int _imuNumber;

        /// <summary>
        /// Location of the imu on the human body
        /// </summary>
        private readonly List<string> _imuLocation;

        /// <summary>
        /// Orientation of the imu relative to the human body axis frame
        /// </summary>
        private readonly List<string> _imuOrientation;

        /// <summary>
        /// List of pins used for each IMU passed in the constructor
        /// </summary>
        private readonly List<List<string>> _allPinsListNames;

        /// <summary>
        /// List of frequencies associated with each IMU passed in the constructor
        /// </summary>
        private readonly List<UInt16> _allAdcFreq;
        private readonly List<UInt16> _allImuFreq;

        /// <summary>
        /// List of firmware of each IMU passed in the constructor
        /// </summary>
        private readonly List<int> _allDeviceFirmware;

        // ******************* FILE PATH ******************** //

        /// <summary>
        /// FullFilePath property combines strings to get file name and exposes its value during file creation
        /// </summary>
        public string FullFilePath { get; private set; }

        // ******************* HEADER DATA ****************** //

        /// <summary>
        /// All possible task numbers and names
        /// </summary>
        public string TaskNums { get; }

        /// <summary>
        /// This string is a combination of all tasks that could have been selected and exposes its value during appending lines
        /// </summary>
        public string TasksString { get; private set; }

        /// <summary>
        /// This is the data headers to be printed on the csv file above the data rows
        /// </summary>
        public const string FileColumnHeaders= "Time Stamp,Counter,IMU Num,Task,Task Stage,ADC 1,ADC 2,ADC 3,ADC 4,ADC 5,ADC 6,ADC 7,ADC 8,Gyro X,Gyro Y,Gyro Z,Mag X,Mag Y,Mag Z,Acc X,Acc Y,Acc Z,";
        
        /// <summary>
        /// Lists of all the pins and frequencies for each connected IMU
        /// </summary>
        public readonly List<string> ImuConfigLineString = new List<string>();

        /// <summary>
        /// Constant Pin Titles is a line of frequency and pin headings
        /// </summary>
        public const string ImuConfigHeader = "IMU ID,IMU Order,ADC Freq,IMU Freq,Pin1,Pin2,Pin3,Pin4,Pin5,Pin6,Pin7,Pin8,Location,Orientation";

        /// <summary>
        /// String builders for the top two lines of the csv files
        /// </summary>
        private readonly StringBuilder _fileHeader = new StringBuilder();
        public string FileHeader { get; private set; }

        /// <summary>
        /// Temporary string builders for storing the pins used for each IMU and their frequencies respectively
        /// </summary>
        private readonly StringBuilder _imuConfigLineSb = new StringBuilder();

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="testDetails"></param>
        /// <param name="allDeviceManagers"></param>
        public SaveFileDetails(TestDetails testDetails, Dictionary<string, DeviceManager> allDeviceManagers)
        {
            _testDetails = testDetails;
            TaskNums = _testDetails.TaskNums;
            TasksString = _testDetails.TasksString;

            _imuNumber = allDeviceManagers.Count();
            _imuLocation = allDeviceManagers.Values.Select(c => c.Device.Location).ToList();
            _imuOrientation = allDeviceManagers.Values.Select(c => c.Device.Orientation).ToList();
            _allBluetoothConnectionNames = allDeviceManagers.Values.Select(c => c.DeviceName).ToList();
            _allPinsListNames = allDeviceManagers.Values.Select(c => c.Device.PinFuncList).ToList();
            _allAdcFreq = allDeviceManagers.Values.Select(d => d.AdcFreq).ToList();
            _allImuFreq = allDeviceManagers.Values.Select(d => d.ImuFreq).ToList();
            _allDeviceFirmware = allDeviceManagers.Values.Select(d => d.DeviceFirmware).ToList();

            HeaderSetUp();
        }

        /// <summary>
        /// Header set up method is called in constructor and creates strings for all of the header section of the csv file and the filepath
        /// </summary>
        private void HeaderSetUp()
        {
            FullFilePath = _testDetails.FilePath + _testDetails.TrialType + _testDetails.TrialNo + ".csv";

            //*************** File Header Line **********************//
            _fileHeader.Append(_testDetails.TrialType + ",");
            _fileHeader.Append(_testDetails.TrialNo + ",");

            _fileHeader.Append("Patient UID" + ",");
            if (_testDetails.TrialType == "Patient Trial")
            {
                _fileHeader.Append((Int32.Parse(_testDetails.TrialNo) + 2) + ",");
            }
            else
            {
                _fileHeader.Append(0 + ",");
            }

            _fileHeader.Append("No. IMUs" + ",");
            _fileHeader.Append(_imuNumber + ",");

            _fileHeader.Append("Tasks Recorded" + ",");
            _fileHeader.Append(_testDetails.TaskSet + ",");

            _fileHeader.Append("No. Tasks" + ",");
            _fileHeader.Append(_testDetails.NoTasks + ",");

            _fileHeader.Append("True Time Stamp" + ",");
            _fileHeader.Append("Yes" + ",");

            _fileHeader.Append("Counter" + ",");
            _fileHeader.Append("Yes" + ",");

            _fileHeader.Append("Side" + ",");
            _fileHeader.Append(_testDetails.InstrumentedSide + ",");

            _fileHeader.Append("Sitting Inclination" + ",");
            _fileHeader.Append(_testDetails.SittingIncl + ",");

            _fileHeader.Append("Firmware" + ",");
            if (_allDeviceFirmware.Any(o => o != _allDeviceFirmware[0]))
            {
                _fileHeader.Append(0);
            }
            else
            {
                _fileHeader.Append(_allDeviceFirmware[0].ToString());
            }

            FileHeader = _fileHeader.ToString();

            //******************* IMU Config Lines *********************//
            for (int order = 0; order < _imuNumber; order++)
            {
                string btName = _allBluetoothConnectionNames[order].Replace("NU_BT_0", "");
                _imuConfigLineSb.Append(btName + ",");

                _imuConfigLineSb.Append((order) + ",");

                _imuConfigLineSb.Append(_allAdcFreq[order] + ",");
                _imuConfigLineSb.Append(_allImuFreq[order] + ",");

                foreach (string pins in _allPinsListNames[order])
                {
                    _imuConfigLineSb.Append(pins + ",");
                }
                _imuConfigLineSb.Append(_imuLocation[order] + ",");
                _imuConfigLineSb.Append(_imuOrientation[order] + ",");

                ImuConfigLineString.Add(_imuConfigLineSb.ToString());
                _imuConfigLineSb.Clear();
            }
        }
    }
}
