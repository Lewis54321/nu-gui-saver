﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    public class BtConnectionEventArgs
    {
        public readonly string DeviceName;
        public readonly int UniqueNumber;

        public BtConnectionEventArgs(string deviceName, int uniqueNumber)
        {
            this.DeviceName = deviceName;
            this.UniqueNumber = uniqueNumber;
        }
    }
}
