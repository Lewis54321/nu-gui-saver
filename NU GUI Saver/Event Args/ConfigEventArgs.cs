﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    public class ConfigEventArgs
    {
        public readonly int ImuNum;
        public readonly List<string> PinList;
        public readonly string Location;
        public readonly string Orientation;

        public ConfigEventArgs(int imuNum, List<string> pinList, string location, string orientation)
        {
            ImuNum = imuNum;
            PinList = pinList;
            Location = location;
            Orientation = orientation;
        }
    }
}
