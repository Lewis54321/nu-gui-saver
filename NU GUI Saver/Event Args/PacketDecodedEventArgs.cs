﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    public class PacketDecodedEventArgs
    {
        /// <summary>
        /// Each packet is a dict type where the string is whether it is ADC or IMU data
        /// The following int array contains the data in order ADC1-8 for ADC data and GyrXYZMagXYZAccXYZ for IMU data
        /// </summary>
        public readonly Dictionary<string, int[]> DecodedPacket;

        /// <summary>
        /// Each packet is a dict type where the string is whether it is ADC or IMU data
        /// The following int array contains the data in order ADC1-8 for ADC data and GyrXYZMagXYZAccXYZ for IMU data
        /// </summary>
        /// <param name="decodedPacket"></param>
        public PacketDecodedEventArgs(Dictionary<string, int[]> decodedPacket)
        {
            this.DecodedPacket = decodedPacket;
        }
    }
}
