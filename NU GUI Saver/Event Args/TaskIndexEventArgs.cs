﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    public class TaskIndexEventArgs
    {
        public readonly int TaskIndex;
        public readonly int TaskState;

        public TaskIndexEventArgs(int taskIndex, int taskState)
        {
            this.TaskIndex = taskIndex;
            this.TaskState = taskState;
        }
    }
}
