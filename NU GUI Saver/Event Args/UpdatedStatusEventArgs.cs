﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    public class UpdatedStatusEventArgs : System.EventArgs
    {
        public readonly string Status;

        public UpdatedStatusEventArgs(string status)
        {
            this.Status = status;
        }
    }
}
