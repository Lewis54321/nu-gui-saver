﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    public class PacketSentEventArgs : System.EventArgs
    {
        public readonly byte[] Packet;
        public readonly int PacketLength;
        public readonly byte PacketCounter;
        public readonly int ImuNumber;

        public PacketSentEventArgs(byte[] packet, int packetLength, byte packetCounter, int imuNumber)
        {
            this.Packet = packet;
            this.PacketLength = packetLength;
            this.PacketCounter = packetCounter;
            this.ImuNumber = imuNumber;
        }
    }
}
