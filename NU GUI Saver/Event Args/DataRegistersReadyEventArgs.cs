﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NU_GUI_Saver
{
    public class DataRegistersReadyEventArgs
    {
        public readonly List<int> DeviceFreq;

        public DataRegistersReadyEventArgs(List<int> deviceFreq)
        {
            this.DeviceFreq = deviceFreq;
        }
    }
}
