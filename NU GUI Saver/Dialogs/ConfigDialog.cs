﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NU_GUI_Saver
{
    public partial class ConfigDialog : Form
    {
        private readonly List<string> _pinList = new List<string>();
        private string _location;
        private string _orientation;
        private readonly int _imuNum;
        private List<ComboBox> _userPinList;

        public ConfigDialog(int imuNum, int pinsActive)
        {
            InitializeComponent();
            _userPinList = new List<ComboBox>(){ comboBoxPin8, comboBoxPin7, comboBoxPin6, comboBoxPin5,
                                    comboBoxPin4, comboBoxPin3, comboBoxPin2, comboBoxPin1};
            char[] binArray = Convert.ToString(pinsActive, 2).PadLeft(8, '0').ToCharArray();
            for (int i = 0; i < 8; i++)
            {
                if (binArray[i] == '1')
                {
                    _userPinList[i].Enabled = true;
                }
            }

            _imuNum = imuNum;

            foreach (var c in tableLayoutPanel8.Controls.OfType<ComboBox>())
            {
                c.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// This method is triggered by the user button event to indicate the configuration values have been input
        /// If the pins are enabled on the IMU board then values will be saved as the user input string (preceded by order number if not "None")
        /// The remaining disabled pins are included as "None" at the end of this list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSetConfig_Click(object sender, EventArgs e)
        {
            int metricCount = 0;
            foreach (ComboBox userPin in _userPinList)
            {
                if (userPin.Enabled)
                {
                    metricCount++;
                    if (userPin.Text != "None")
                    {
                        _pinList.Add(metricCount.ToString() + userPin.Text);
                    }
                    else
                    {
                        _pinList.Add("None");
                    }                                    
                }                
            }
            while (_pinList.Count < 8)
            {
                _pinList.Add("None");
            }
            _location = comboBoxLocation.Text;
            _orientation = comboBoxOrientation.Text;
            OnConfigSet(new ConfigEventArgs(_imuNum, _pinList, _location, _orientation));
        }

        /// <summary>
        /// Event occurs whenever the button set configuration is set
        /// Used to notify the user that the values have been updated and also sets a bool to show that this IMU has been configured
        /// </summary>
        public event EventHandler<ConfigEventArgs> ConfigSet;
        protected virtual void OnConfigSet(ConfigEventArgs e)
        {
            ConfigSet?.Invoke(this, e);
        }
    }
}
