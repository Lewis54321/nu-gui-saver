﻿namespace NU_GUI_Saver
{
    partial class CalibrationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalibrationDialog));
            this.buttonUpload = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxBaseFreq = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxDivisor = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.checkBoxPin1 = new System.Windows.Forms.CheckBox();
            this.checkBoxPin2 = new System.Windows.Forms.CheckBox();
            this.checkBoxPin3 = new System.Windows.Forms.CheckBox();
            this.checkBoxPin4 = new System.Windows.Forms.CheckBox();
            this.checkBoxPin5 = new System.Windows.Forms.CheckBox();
            this.checkBoxPin6 = new System.Windows.Forms.CheckBox();
            this.checkBoxPin7 = new System.Windows.Forms.CheckBox();
            this.checkBoxPin8 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.comboBoxGyrScale = new System.Windows.Forms.ComboBox();
            this.comboBoxGyrOdr = new System.Windows.Forms.ComboBox();
            this.comboBoxAccScale = new System.Windows.Forms.ComboBox();
            this.comboBoxAccOdr = new System.Windows.Forms.ComboBox();
            this.comboBoxMagScale = new System.Windows.Forms.ComboBox();
            this.comboBoxMagOdr = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxGyrZ = new System.Windows.Forms.TextBox();
            this.textBoxGyrY = new System.Windows.Forms.TextBox();
            this.textBoxGyrX = new System.Windows.Forms.TextBox();
            this.textBoxAccZ = new System.Windows.Forms.TextBox();
            this.textBoxAccY = new System.Windows.Forms.TextBox();
            this.textBoxAccX = new System.Windows.Forms.TextBox();
            this.textBoxMagZ = new System.Windows.Forms.TextBox();
            this.textBoxMagY = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxMagX = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel9.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonUpload
            // 
            this.buttonUpload.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonUpload.Location = new System.Drawing.Point(3, 275);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(261, 23);
            this.buttonUpload.TabIndex = 1;
            this.buttonUpload.Text = "Upload Values";
            this.buttonUpload.UseVisualStyleBackColor = true;
            this.buttonUpload.Click += new System.EventHandler(this.ButtonUpload_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.buttonUpload, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.41916F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.580838F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(267, 301);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(261, 266);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(253, 240);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Basic";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel7, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(247, 234);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.63044F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.36956F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel4.Controls.Add(this.textBoxBaseFreq, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label10, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(241, 40);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // textBoxBaseFreq
            // 
            this.textBoxBaseFreq.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxBaseFreq.Location = new System.Drawing.Point(95, 10);
            this.textBoxBaseFreq.Name = "textBoxBaseFreq";
            this.textBoxBaseFreq.Size = new System.Drawing.Size(80, 20);
            this.textBoxBaseFreq.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 26);
            this.label8.TabIndex = 0;
            this.label8.Text = "Base Frequency: ";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(181, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Hz";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.15353F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.84647F));
            this.tableLayoutPanel5.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.comboBoxDivisor, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 95);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(241, 40);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(76, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Inertial Divisor:";
            // 
            // comboBoxDivisor
            // 
            this.comboBoxDivisor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxDivisor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDivisor.FormattingEnabled = true;
            this.comboBoxDivisor.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10",
            "20",
            "50",
            "100",
            "200",
            "500",
            "1000"});
            this.comboBoxDivisor.Location = new System.Drawing.Point(107, 9);
            this.comboBoxDivisor.Name = "comboBoxDivisor";
            this.comboBoxDivisor.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDivisor.TabIndex = 1;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 8;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label16, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label17, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.label18, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.label19, 4, 0);
            this.tableLayoutPanel6.Controls.Add(this.label20, 5, 0);
            this.tableLayoutPanel6.Controls.Add(this.label21, 6, 0);
            this.tableLayoutPanel6.Controls.Add(this.label22, 7, 0);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxPin1, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxPin2, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxPin3, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxPin4, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxPin5, 4, 1);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxPin6, 5, 1);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxPin7, 6, 1);
            this.tableLayoutPanel6.Controls.Add(this.checkBoxPin8, 8, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 187);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(241, 44);
            this.tableLayoutPanel6.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "(1)";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(35, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "(2)";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(65, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "(3)";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(95, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "(4)";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(125, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "(5)";
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(155, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "(6)";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(185, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "(7)";
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(216, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "(8)";
            // 
            // checkBoxPin1
            // 
            this.checkBoxPin1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxPin1.AutoSize = true;
            this.checkBoxPin1.Location = new System.Drawing.Point(7, 25);
            this.checkBoxPin1.Name = "checkBoxPin1";
            this.checkBoxPin1.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPin1.TabIndex = 8;
            this.checkBoxPin1.UseVisualStyleBackColor = true;
            // 
            // checkBoxPin2
            // 
            this.checkBoxPin2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxPin2.AutoSize = true;
            this.checkBoxPin2.Location = new System.Drawing.Point(37, 25);
            this.checkBoxPin2.Name = "checkBoxPin2";
            this.checkBoxPin2.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPin2.TabIndex = 9;
            this.checkBoxPin2.UseVisualStyleBackColor = true;
            // 
            // checkBoxPin3
            // 
            this.checkBoxPin3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxPin3.AutoSize = true;
            this.checkBoxPin3.Location = new System.Drawing.Point(67, 25);
            this.checkBoxPin3.Name = "checkBoxPin3";
            this.checkBoxPin3.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPin3.TabIndex = 10;
            this.checkBoxPin3.UseVisualStyleBackColor = true;
            // 
            // checkBoxPin4
            // 
            this.checkBoxPin4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxPin4.AutoSize = true;
            this.checkBoxPin4.Location = new System.Drawing.Point(97, 25);
            this.checkBoxPin4.Name = "checkBoxPin4";
            this.checkBoxPin4.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPin4.TabIndex = 11;
            this.checkBoxPin4.UseVisualStyleBackColor = true;
            // 
            // checkBoxPin5
            // 
            this.checkBoxPin5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxPin5.AutoSize = true;
            this.checkBoxPin5.Location = new System.Drawing.Point(127, 25);
            this.checkBoxPin5.Name = "checkBoxPin5";
            this.checkBoxPin5.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPin5.TabIndex = 12;
            this.checkBoxPin5.UseVisualStyleBackColor = true;
            // 
            // checkBoxPin6
            // 
            this.checkBoxPin6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxPin6.AutoSize = true;
            this.checkBoxPin6.Location = new System.Drawing.Point(157, 25);
            this.checkBoxPin6.Name = "checkBoxPin6";
            this.checkBoxPin6.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPin6.TabIndex = 13;
            this.checkBoxPin6.UseVisualStyleBackColor = true;
            // 
            // checkBoxPin7
            // 
            this.checkBoxPin7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxPin7.AutoSize = true;
            this.checkBoxPin7.Location = new System.Drawing.Point(187, 25);
            this.checkBoxPin7.Name = "checkBoxPin7";
            this.checkBoxPin7.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPin7.TabIndex = 14;
            this.checkBoxPin7.UseVisualStyleBackColor = true;
            // 
            // checkBoxPin8
            // 
            this.checkBoxPin8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.checkBoxPin8.AutoSize = true;
            this.checkBoxPin8.Location = new System.Drawing.Point(218, 25);
            this.checkBoxPin8.Name = "checkBoxPin8";
            this.checkBoxPin8.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPin8.TabIndex = 15;
            this.checkBoxPin8.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.07884F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.92116F));
            this.tableLayoutPanel7.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 141);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(241, 40);
            this.tableLayoutPanel7.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "ADC Channels:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImageLocation = "D:\\Box Sync\\IP 10-05-17\\SC- PhD\\IMU Saver\\NU GUI Saver\\Simulation\\Textures\\pin_di" +
    "agram.png";
            this.pictureBox1.Location = new System.Drawing.Point(102, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(136, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label24, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 49);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(241, 40);
            this.tableLayoutPanel9.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(235, 20);
            this.label11.TabIndex = 4;
            this.label11.Text = "Base freq must be between 1 and 1000 Hz";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Location = new System.Drawing.Point(3, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(235, 20);
            this.label24.TabIndex = 5;
            this.label24.Text = "Inertial freq will be rounded to nearest integer";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(253, 240);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Advanced";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.24696F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.75304F));
            this.tableLayoutPanel8.Controls.Add(this.label33, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.label31, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.label29, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.label27, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label25, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label23, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxGyrScale, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxGyrOdr, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxAccScale, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxAccOdr, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxMagScale, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxMagOdr, 1, 5);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 6;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(247, 234);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 205);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(48, 13);
            this.label33.TabIndex = 10;
            this.label33.Text = "mag_odr";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(3, 164);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(58, 13);
            this.label31.TabIndex = 8;
            this.label31.Text = "mag_scale";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 126);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 13);
            this.label29.TabIndex = 6;
            this.label29.Text = "accel_odr";
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(3, 88);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 13);
            this.label27.TabIndex = 4;
            this.label27.Text = "accel_scale";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 50);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(48, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "gyro_odr";
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(3, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "gyro_scale";
            // 
            // comboBoxGyrScale
            // 
            this.comboBoxGyrScale.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxGyrScale.AutoCompleteCustomSource.AddRange(new string[] {
            "245 DPS",
            "500 DPS",
            "2000 DPS"});
            this.comboBoxGyrScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGyrScale.FormattingEnabled = true;
            this.comboBoxGyrScale.Items.AddRange(new object[] {
            "245 DPS",
            "500 DPS",
            "2000 DPS"});
            this.comboBoxGyrScale.Location = new System.Drawing.Point(94, 8);
            this.comboBoxGyrScale.Name = "comboBoxGyrScale";
            this.comboBoxGyrScale.Size = new System.Drawing.Size(121, 21);
            this.comboBoxGyrScale.TabIndex = 11;
            // 
            // comboBoxGyrOdr
            // 
            this.comboBoxGyrOdr.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxGyrOdr.AutoCompleteCustomSource.AddRange(new string[] {
            "95 Hz BW 12.5 Hz",
            "95 Hz BW 25 Hz",
            "Not Implemented",
            "Not Implemented",
            "190 Hz BW 12.5 Hz",
            "190 Hz BW 25 Hz",
            "190 Hz BW 50 Hz",
            "190 Hz BW 70 Hz",
            "380 Hz BW 20 Hz",
            "380 Hz BW 25 Hz",
            "380 Hz BW 50 Hz",
            "380 Hz BW 100 Hz",
            "760 Hz BW 30 Hz",
            "760 Hz BW 35 Hz",
            "760 Hz BW 50 Hz",
            "760 Hz BW 100 Hz"});
            this.comboBoxGyrOdr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGyrOdr.FormattingEnabled = true;
            this.comboBoxGyrOdr.Items.AddRange(new object[] {
            "95 Hz BW 12.5 Hz",
            "95 Hz BW 25 Hz",
            "Not Implemented",
            "Not Implemented",
            "190 Hz BW 12.5 Hz",
            "190 Hz BW 25 Hz",
            "190 Hz BW 50 Hz",
            "190 Hz BW 70 Hz",
            "380 Hz BW 20 Hz",
            "380 Hz BW 25 Hz",
            "380 Hz BW 50 Hz",
            "380 Hz BW 100 Hz",
            "760 Hz BW 30 Hz",
            "760 Hz BW 35 Hz",
            "760 Hz BW 50 Hz",
            "760 Hz BW 100 Hz"});
            this.comboBoxGyrOdr.Location = new System.Drawing.Point(94, 46);
            this.comboBoxGyrOdr.Name = "comboBoxGyrOdr";
            this.comboBoxGyrOdr.Size = new System.Drawing.Size(121, 21);
            this.comboBoxGyrOdr.TabIndex = 12;
            // 
            // comboBoxAccScale
            // 
            this.comboBoxAccScale.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxAccScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAccScale.FormattingEnabled = true;
            this.comboBoxAccScale.Items.AddRange(new object[] {
            "2 G",
            "4 G",
            "6 G",
            "8 G",
            "16 G"});
            this.comboBoxAccScale.Location = new System.Drawing.Point(94, 84);
            this.comboBoxAccScale.Name = "comboBoxAccScale";
            this.comboBoxAccScale.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAccScale.TabIndex = 13;
            // 
            // comboBoxAccOdr
            // 
            this.comboBoxAccOdr.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxAccOdr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAccOdr.FormattingEnabled = true;
            this.comboBoxAccOdr.Items.AddRange(new object[] {
            "Power Down",
            "3.125 Hz",
            "6.25 Hz",
            "12.5 Hz",
            "25 Hz",
            "50 Hz",
            "100 Hz",
            "200 Hz",
            "400 Hz",
            "1600 Hz"});
            this.comboBoxAccOdr.Location = new System.Drawing.Point(94, 122);
            this.comboBoxAccOdr.Name = "comboBoxAccOdr";
            this.comboBoxAccOdr.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAccOdr.TabIndex = 14;
            // 
            // comboBoxMagScale
            // 
            this.comboBoxMagScale.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxMagScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMagScale.FormattingEnabled = true;
            this.comboBoxMagScale.Items.AddRange(new object[] {
            "2 Gs",
            "4 Gs",
            "8 Gs",
            "12 Gs"});
            this.comboBoxMagScale.Location = new System.Drawing.Point(94, 160);
            this.comboBoxMagScale.Name = "comboBoxMagScale";
            this.comboBoxMagScale.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMagScale.TabIndex = 15;
            // 
            // comboBoxMagOdr
            // 
            this.comboBoxMagOdr.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBoxMagOdr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMagOdr.FormattingEnabled = true;
            this.comboBoxMagOdr.Items.AddRange(new object[] {
            "3.125 Hz",
            "6.25 Hz",
            "12.5 Hz",
            "25 Hz",
            "50 Hz",
            "100 Hz"});
            this.comboBoxMagOdr.Location = new System.Drawing.Point(94, 201);
            this.comboBoxMagOdr.Name = "comboBoxMagOdr";
            this.comboBoxMagOdr.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMagOdr.TabIndex = 16;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(253, 240);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Calibration";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.textBoxGyrZ, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.textBoxGyrY, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.textBoxGyrX, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.textBoxAccZ, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.textBoxAccY, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.textBoxAccX, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.textBoxMagZ, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBoxMagY, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.label15, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBoxMagX, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 9;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(247, 234);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // textBoxGyrZ
            // 
            this.textBoxGyrZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxGyrZ.Location = new System.Drawing.Point(103, 211);
            this.textBoxGyrZ.Name = "textBoxGyrZ";
            this.textBoxGyrZ.Size = new System.Drawing.Size(141, 20);
            this.textBoxGyrZ.TabIndex = 25;
            // 
            // textBoxGyrY
            // 
            this.textBoxGyrY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxGyrY.Location = new System.Drawing.Point(103, 185);
            this.textBoxGyrY.Name = "textBoxGyrY";
            this.textBoxGyrY.Size = new System.Drawing.Size(141, 20);
            this.textBoxGyrY.TabIndex = 24;
            // 
            // textBoxGyrX
            // 
            this.textBoxGyrX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxGyrX.Location = new System.Drawing.Point(103, 159);
            this.textBoxGyrX.Name = "textBoxGyrX";
            this.textBoxGyrX.Size = new System.Drawing.Size(141, 20);
            this.textBoxGyrX.TabIndex = 23;
            // 
            // textBoxAccZ
            // 
            this.textBoxAccZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAccZ.Location = new System.Drawing.Point(103, 133);
            this.textBoxAccZ.Name = "textBoxAccZ";
            this.textBoxAccZ.Size = new System.Drawing.Size(141, 20);
            this.textBoxAccZ.TabIndex = 22;
            // 
            // textBoxAccY
            // 
            this.textBoxAccY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAccY.Location = new System.Drawing.Point(103, 107);
            this.textBoxAccY.Name = "textBoxAccY";
            this.textBoxAccY.Size = new System.Drawing.Size(141, 20);
            this.textBoxAccY.TabIndex = 21;
            // 
            // textBoxAccX
            // 
            this.textBoxAccX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxAccX.Location = new System.Drawing.Point(103, 81);
            this.textBoxAccX.Name = "textBoxAccX";
            this.textBoxAccX.Size = new System.Drawing.Size(141, 20);
            this.textBoxAccX.TabIndex = 20;
            // 
            // textBoxMagZ
            // 
            this.textBoxMagZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMagZ.Location = new System.Drawing.Point(103, 55);
            this.textBoxMagZ.Name = "textBoxMagZ";
            this.textBoxMagZ.Size = new System.Drawing.Size(141, 20);
            this.textBoxMagZ.TabIndex = 19;
            // 
            // textBoxMagY
            // 
            this.textBoxMagY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMagY.Location = new System.Drawing.Point(103, 29);
            this.textBoxMagY.Name = "textBoxMagY";
            this.textBoxMagY.Size = new System.Drawing.Size(141, 20);
            this.textBoxMagY.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 208);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(5);
            this.label9.Size = new System.Drawing.Size(94, 26);
            this.label9.TabIndex = 16;
            this.label9.Text = "GyroZ Offset:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 182);
            this.label15.Name = "label15";
            this.label15.Padding = new System.Windows.Forms.Padding(5);
            this.label15.Size = new System.Drawing.Size(94, 26);
            this.label15.TabIndex = 8;
            this.label15.Text = "GyroY Offset:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 156);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5);
            this.label7.Size = new System.Drawing.Size(94, 26);
            this.label7.TabIndex = 12;
            this.label7.Text = "GyroX Offset:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 130);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(5);
            this.label6.Size = new System.Drawing.Size(94, 26);
            this.label6.TabIndex = 10;
            this.label6.Text = "AccZ Offset:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 104);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5);
            this.label5.Size = new System.Drawing.Size(94, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "AccY Offset:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 78);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(5);
            this.label4.Size = new System.Drawing.Size(94, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "AccX Offset:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 52);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(5);
            this.label3.Size = new System.Drawing.Size(94, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "MagZ Offset:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 26);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(94, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "MagY Offset:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(94, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "MagX Offset:";
            // 
            // textBoxMagX
            // 
            this.textBoxMagX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxMagX.Location = new System.Drawing.Point(103, 3);
            this.textBoxMagX.Name = "textBoxMagX";
            this.textBoxMagX.Size = new System.Drawing.Size(141, 20);
            this.textBoxMagX.TabIndex = 17;
            // 
            // CalibrationDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 321);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "CalibrationDialog";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "CalibrationDialog";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox textBoxBaseFreq;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxDivisor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox checkBoxPin1;
        private System.Windows.Forms.CheckBox checkBoxPin2;
        private System.Windows.Forms.CheckBox checkBoxPin3;
        private System.Windows.Forms.CheckBox checkBoxPin4;
        private System.Windows.Forms.CheckBox checkBoxPin5;
        private System.Windows.Forms.CheckBox checkBoxPin6;
        private System.Windows.Forms.CheckBox checkBoxPin7;
        private System.Windows.Forms.CheckBox checkBoxPin8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox textBoxGyrZ;
        private System.Windows.Forms.TextBox textBoxGyrY;
        private System.Windows.Forms.TextBox textBoxGyrX;
        private System.Windows.Forms.TextBox textBoxAccZ;
        private System.Windows.Forms.TextBox textBoxAccY;
        private System.Windows.Forms.TextBox textBoxAccX;
        private System.Windows.Forms.TextBox textBoxMagZ;
        private System.Windows.Forms.TextBox textBoxMagY;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxMagX;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBoxGyrScale;
        private System.Windows.Forms.ComboBox comboBoxGyrOdr;
        private System.Windows.Forms.ComboBox comboBoxAccScale;
        private System.Windows.Forms.ComboBox comboBoxAccOdr;
        private System.Windows.Forms.ComboBox comboBoxMagScale;
        private System.Windows.Forms.ComboBox comboBoxMagOdr;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label24;
    }
}