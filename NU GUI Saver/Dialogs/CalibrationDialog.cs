﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NU_GUI_Saver
{
    public partial class CalibrationDialog : Form
    {
        /// <summary>
        /// Dictionary of user selected register values for all the registers than may be set
        /// </summary>
        public Dictionary<string, int[]> RegisterValues = new Dictionary<string, int[]>();

        /// <summary>
        /// Dictionary of inertial calibration values gathered from the device
        /// </summary>
        private Dictionary<string, Int16> _inertialCal;

        /// <summary>
        /// Dictionary of inertial advanced values gathered from the device
        /// </summary>
        private Dictionary<string, byte> _inertialAdv;

        /// <summary>
        /// Dictionary of sample settings gathered from the device
        /// </summary>
        private Dictionary<string, UInt16> _sampleSet;

        /// <summary>
        /// List of possible divisor values for calculating the inertial frequency relative to the base frequency
        /// </summary>
        private List<int> _divisorValues = new List<int> { 1, 2, 5, 10, 20, 50, 100, 200, 500, 1000 };

        public CalibrationDialog(Dictionary<string, Int16> inertialCal, Dictionary<string, byte> inertialAdv, Dictionary<string, UInt16> sampleSet)
        {
            InitializeComponent();

            _inertialCal = inertialCal;
            _inertialAdv = inertialAdv;
            _sampleSet = sampleSet;

            DisplayCurrentCalValues();
        }

        private void DisplayCurrentCalValues()
        {
            this.comboBoxDivisor.DataSource = _divisorValues;
            this.comboBoxDivisor.SelectedIndex = _divisorValues.IndexOf(_sampleSet["FrequencyDiv"] + 1);
            this.textBoxBaseFreq.Text = _sampleSet["AdcFreq"].ToString();
            SetAdcPins(_sampleSet["AdcPins"]);

            this.comboBoxGyrScale.SelectedIndex = _inertialAdv["GyrScaleIdx"];
            this.comboBoxGyrOdr.SelectedIndex = _inertialAdv["GyrRateIdx"];
            this.comboBoxAccScale.SelectedIndex = _inertialAdv["AccScaleIdx"];
            this.comboBoxAccOdr.SelectedIndex = _inertialAdv["AccRateIdx"];
            this.comboBoxMagScale.SelectedIndex = _inertialAdv["MagScaleIdx"];
            this.comboBoxMagOdr.SelectedIndex = _inertialAdv["MagRateIdx"];

            this.textBoxMagX.Text = _inertialCal["MagX"].ToString();
            this.textBoxMagY.Text = _inertialCal["MagY"].ToString();
            this.textBoxMagZ.Text = _inertialCal["MagZ"].ToString();
            this.textBoxAccX.Text = _inertialCal["AccX"].ToString();
            this.textBoxAccY.Text = _inertialCal["AccY"].ToString();
            this.textBoxAccZ.Text = _inertialCal["AccZ"].ToString();
            this.textBoxGyrX.Text = _inertialCal["GyrX"].ToString();
            this.textBoxGyrY.Text = _inertialCal["GyrY"].ToString();
            this.textBoxGyrZ.Text = _inertialCal["GyrZ"].ToString();
        }

        private void SetAdcPins(int adcPins)
        {
            char[] binArray = Convert.ToString(adcPins, 2).PadLeft(8,'0').ToCharArray();
            List<CheckBox> listPins = new List<CheckBox>() { checkBoxPin8, checkBoxPin7, checkBoxPin6, checkBoxPin5, checkBoxPin4, checkBoxPin3, checkBoxPin2, checkBoxPin1 };

            for (int i = 0; i < 8; i++)
            {
                if (binArray[i] == '1')
                {
                    listPins[i].Checked = true;
                }
            }
        }

        private int GetAdcPins()
        {
            int[] binArray = new bool[]{ checkBoxPin8.Checked, checkBoxPin7.Checked, checkBoxPin6.Checked, checkBoxPin5.Checked,
                checkBoxPin4.Checked, checkBoxPin3.Checked, checkBoxPin2.Checked, checkBoxPin1.Checked }.Select(b => Convert.ToInt32(b)).ToArray();
            int pins = Convert.ToInt16(string.Join("", binArray), 2);
            return pins;
        }

        private Dictionary<string, int> GetFrequencyParameters(Int16 baseFreq)
        {
            int clock = 30000000;
            int period = 65536;
            int preScalerIdx = -1;
            int[] preScalerValues = {1, 8, 64, 256};

            while (period > 65535)
            {
                preScalerIdx += 1;
                period = clock / (baseFreq * preScalerValues[preScalerIdx] * 2);
            }

            Dictionary<string, int> frequencyParameters = new Dictionary<string, int>() { { "Period", period }, { "PreScalerIdx", preScalerIdx } };
            return frequencyParameters;
        }

        /// <summary>
        /// Button click event for creating a dict of calibration values to be uploaded to the IMU
        /// Strings converted to either signed or unsigned ints depending on what format the IMU expects
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonUpload_Click(object sender, EventArgs e)
        {
            Dictionary<string, int> frequencyParameters =
                GetFrequencyParameters(Convert.ToInt16(this.textBoxBaseFreq.Text));

            RegisterValues.Add("period", new[] { 3, frequencyParameters["Period"] });
            RegisterValues.Add("Divisor", new[] { 5, _divisorValues[comboBoxDivisor.SelectedIndex] - 1 });
            RegisterValues.Add("AdcPins", new[] { 7, GetAdcPins() });
            RegisterValues.Add("Prescaler", new[] { 9, frequencyParameters["PreScalerIdx"] });
            RegisterValues.Add("GyroScale", new[] { 11, comboBoxGyrScale.SelectedIndex });
            RegisterValues.Add("GyroRate", new[] { 13, comboBoxGyrOdr.SelectedIndex });
            RegisterValues.Add("AccScale", new[] { 15, comboBoxAccScale.SelectedIndex });
            RegisterValues.Add("AccRate", new[] { 17, comboBoxAccOdr.SelectedIndex });
            RegisterValues.Add("MagScale", new[] { 19, comboBoxMagScale.SelectedIndex });
            RegisterValues.Add("MagRate", new[] { 21, comboBoxMagOdr.SelectedIndex });
            RegisterValues.Add("MagX", new []{23, Convert.ToInt16(textBoxMagX.Text)});
            RegisterValues.Add("MagY", new []{25, Convert.ToInt16(textBoxMagY.Text)});
            RegisterValues.Add("MagZ", new []{27, Convert.ToInt16(textBoxMagZ.Text)});
            RegisterValues.Add("AccX", new []{29, Convert.ToInt16(textBoxAccX.Text)});
            RegisterValues.Add("AccY", new []{31, Convert.ToInt16(textBoxAccY.Text)});
            RegisterValues.Add("AccZ", new []{33, Convert.ToInt16(textBoxAccZ.Text)});
            RegisterValues.Add("GyrX", new []{35, Convert.ToInt16(textBoxGyrX.Text)});
            RegisterValues.Add("GyrY", new []{37, Convert.ToInt16(textBoxGyrY.Text)});
            RegisterValues.Add("GyrZ", new []{39, Convert.ToInt16(textBoxGyrZ.Text)});
            //RegisterValues.Add("Firmware", new[] { 41, 6 });
            OnCalValues(new EventArgs());
        }

        public event EventHandler<EventArgs> CalValues;
        protected virtual void OnCalValues(EventArgs e)
        {
            CalValues?.Invoke(this, e);
        }
    }
}
