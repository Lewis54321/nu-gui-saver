﻿namespace NU_GUI_Saver
{
    partial class ConfigDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.comboBoxPin1 = new System.Windows.Forms.ComboBox();
            this.comboBoxPin2 = new System.Windows.Forms.ComboBox();
            this.comboBoxPin3 = new System.Windows.Forms.ComboBox();
            this.comboBoxPin4 = new System.Windows.Forms.ComboBox();
            this.comboBoxPin5 = new System.Windows.Forms.ComboBox();
            this.comboBoxPin6 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxPin7 = new System.Windows.Forms.ComboBox();
            this.comboBoxPin8 = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBoxLocation = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxOrientation = new System.Windows.Forms.ComboBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonSetConfig = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonSetConfig, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.75497F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.24503F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1284, 702);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 168F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.label33, 0, 6);
            this.tableLayoutPanel8.Controls.Add(this.label31, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.label29, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.label27, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.label25, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label23, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxPin1, 2, 1);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxPin2, 2, 2);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxPin3, 2, 3);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxPin4, 2, 4);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxPin5, 2, 5);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxPin6, 2, 6);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 7);
            this.tableLayoutPanel8.Controls.Add(this.label2, 0, 8);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxPin7, 2, 7);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxPin8, 2, 8);
            this.tableLayoutPanel8.Controls.Add(this.panel2, 0, 9);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxLocation, 2, 10);
            this.tableLayoutPanel8.Controls.Add(this.label4, 0, 11);
            this.tableLayoutPanel8.Controls.Add(this.comboBoxOrientation, 2, 11);
            this.tableLayoutPanel8.Controls.Add(this.panel6, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.panel1, 1, 10);
            this.tableLayoutPanel8.Controls.Add(this.panel3, 0, 12);
            this.tableLayoutPanel8.Controls.Add(this.label5, 0, 10);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 13;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.08064F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.08064F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.08064F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.08064F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.08064F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.08064F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.08064F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.08064F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.677416F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.677416F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1284, 609);
            this.tableLayoutPanel8.TabIndex = 7;
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(4, 316);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(79, 29);
            this.label33.TabIndex = 10;
            this.label33.Text = "Pin 6:";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(4, 257);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(79, 29);
            this.label31.TabIndex = 8;
            this.label31.Text = "Pin 5:";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(4, 198);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(79, 29);
            this.label29.TabIndex = 6;
            this.label29.Text = "Pin 4:";
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(4, 139);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(79, 29);
            this.label27.TabIndex = 4;
            this.label27.Text = "Pin 3:";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(4, 80);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 29);
            this.label25.TabIndex = 2;
            this.label25.Text = "Pin 2:";
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(4, 21);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 29);
            this.label23.TabIndex = 0;
            this.label23.Text = "Pin 1:";
            // 
            // comboBoxPin1
            // 
            this.comboBoxPin1.AutoCompleteCustomSource.AddRange(new string[] {
            "245 DPS",
            "500 DPS",
            "2000 DPS"});
            this.comboBoxPin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPin1.Enabled = false;
            this.comboBoxPin1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPin1.FormattingEnabled = true;
            this.comboBoxPin1.Items.AddRange(new object[] {
            "None",
            "MMG",
            "EMG",
            "Other"});
            this.comboBoxPin1.Location = new System.Drawing.Point(174, 6);
            this.comboBoxPin1.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPin1.Name = "comboBoxPin1";
            this.comboBoxPin1.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxPin1.TabIndex = 11;
            // 
            // comboBoxPin2
            // 
            this.comboBoxPin2.AutoCompleteCustomSource.AddRange(new string[] {
            "95 Hz BW 12.5 Hz",
            "95 Hz BW 25 Hz",
            "Not Implemented",
            "Not Implemented",
            "190 Hz BW 12.5 Hz",
            "190 Hz BW 25 Hz",
            "190 Hz BW 50 Hz",
            "190 Hz BW 70 Hz",
            "380 Hz BW 20 Hz",
            "380 Hz BW 25 Hz",
            "380 Hz BW 50 Hz",
            "380 Hz BW 100 Hz",
            "760 Hz BW 30 Hz",
            "760 Hz BW 35 Hz",
            "760 Hz BW 50 Hz",
            "760 Hz BW 100 Hz"});
            this.comboBoxPin2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPin2.Enabled = false;
            this.comboBoxPin2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPin2.FormattingEnabled = true;
            this.comboBoxPin2.Items.AddRange(new object[] {
            "None",
            "MMG",
            "EMG",
            "Other"});
            this.comboBoxPin2.Location = new System.Drawing.Point(174, 65);
            this.comboBoxPin2.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPin2.Name = "comboBoxPin2";
            this.comboBoxPin2.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxPin2.TabIndex = 12;
            // 
            // comboBoxPin3
            // 
            this.comboBoxPin3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPin3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPin3.Enabled = false;
            this.comboBoxPin3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPin3.FormattingEnabled = true;
            this.comboBoxPin3.Items.AddRange(new object[] {
            "None",
            "MMG",
            "EMG",
            "Other"});
            this.comboBoxPin3.Location = new System.Drawing.Point(174, 124);
            this.comboBoxPin3.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPin3.Name = "comboBoxPin3";
            this.comboBoxPin3.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxPin3.TabIndex = 13;
            // 
            // comboBoxPin4
            // 
            this.comboBoxPin4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPin4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPin4.Enabled = false;
            this.comboBoxPin4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPin4.FormattingEnabled = true;
            this.comboBoxPin4.Items.AddRange(new object[] {
            "None",
            "MMG",
            "EMG",
            "Other"});
            this.comboBoxPin4.Location = new System.Drawing.Point(174, 183);
            this.comboBoxPin4.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPin4.Name = "comboBoxPin4";
            this.comboBoxPin4.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxPin4.TabIndex = 14;
            // 
            // comboBoxPin5
            // 
            this.comboBoxPin5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPin5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPin5.Enabled = false;
            this.comboBoxPin5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPin5.FormattingEnabled = true;
            this.comboBoxPin5.Items.AddRange(new object[] {
            "None",
            "MMG",
            "EMG",
            "Other"});
            this.comboBoxPin5.Location = new System.Drawing.Point(174, 242);
            this.comboBoxPin5.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPin5.Name = "comboBoxPin5";
            this.comboBoxPin5.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxPin5.TabIndex = 15;
            // 
            // comboBoxPin6
            // 
            this.comboBoxPin6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPin6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPin6.Enabled = false;
            this.comboBoxPin6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPin6.FormattingEnabled = true;
            this.comboBoxPin6.Items.AddRange(new object[] {
            "None",
            "MMG",
            "EMG",
            "Other"});
            this.comboBoxPin6.Location = new System.Drawing.Point(174, 301);
            this.comboBoxPin6.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPin6.Name = "comboBoxPin6";
            this.comboBoxPin6.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxPin6.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 375);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 29);
            this.label1.TabIndex = 17;
            this.label1.Text = "Pin 7:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 434);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 29);
            this.label2.TabIndex = 18;
            this.label2.Text = "Pin 8:";
            // 
            // comboBoxPin7
            // 
            this.comboBoxPin7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPin7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPin7.Enabled = false;
            this.comboBoxPin7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPin7.FormattingEnabled = true;
            this.comboBoxPin7.Items.AddRange(new object[] {
            "None",
            "MMG",
            "EMG",
            "Other"});
            this.comboBoxPin7.Location = new System.Drawing.Point(174, 360);
            this.comboBoxPin7.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPin7.Name = "comboBoxPin7";
            this.comboBoxPin7.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxPin7.TabIndex = 19;
            // 
            // comboBoxPin8
            // 
            this.comboBoxPin8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPin8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPin8.Enabled = false;
            this.comboBoxPin8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPin8.FormattingEnabled = true;
            this.comboBoxPin8.Items.AddRange(new object[] {
            "None",
            "MMG",
            "EMG",
            "Other"});
            this.comboBoxPin8.Location = new System.Drawing.Point(174, 419);
            this.comboBoxPin8.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxPin8.Name = "comboBoxPin8";
            this.comboBoxPin8.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxPin8.TabIndex = 20;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.tableLayoutPanel8.SetColumnSpan(this.panel2, 3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 478);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1284, 6);
            this.panel2.TabIndex = 23;
            // 
            // comboBoxLocation
            // 
            this.comboBoxLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxLocation.FormattingEnabled = true;
            this.comboBoxLocation.Items.AddRange(new object[] {
            "Torso",
            "UpperArm",
            "LowerArm",
            "Wrist"});
            this.comboBoxLocation.Location = new System.Drawing.Point(174, 484);
            this.comboBoxLocation.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxLocation.Name = "comboBoxLocation";
            this.comboBoxLocation.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxLocation.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 555);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 29);
            this.label4.TabIndex = 27;
            this.label4.Text = "Orientation:";
            // 
            // comboBoxOrientation
            // 
            this.comboBoxOrientation.AutoCompleteCustomSource.AddRange(new string[] {
            "Up",
            "Down",
            "Left",
            "Right"});
            this.comboBoxOrientation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxOrientation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOrientation.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxOrientation.FormattingEnabled = true;
            this.comboBoxOrientation.Items.AddRange(new object[] {
            "Downwards",
            "Upwards",
            "Left",
            "Right"});
            this.comboBoxOrientation.Location = new System.Drawing.Point(174, 541);
            this.comboBoxOrientation.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxOrientation.Name = "comboBoxOrientation";
            this.comboBoxOrientation.Size = new System.Drawing.Size(1110, 45);
            this.comboBoxOrientation.TabIndex = 28;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(168, 6);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.tableLayoutPanel8.SetRowSpan(this.panel6, 8);
            this.panel6.Size = new System.Drawing.Size(6, 472);
            this.panel6.TabIndex = 29;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.tableLayoutPanel8.SetColumnSpan(this.panel5, 3);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1284, 6);
            this.panel5.TabIndex = 41;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(168, 484);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel8.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(6, 114);
            this.panel1.TabIndex = 42;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.tableLayoutPanel8.SetColumnSpan(this.panel3, 3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 598);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.MaximumSize = new System.Drawing.Size(0, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1284, 6);
            this.panel3.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 498);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 29);
            this.label5.TabIndex = 44;
            this.label5.Text = "Location:";
            // 
            // buttonSetConfig
            // 
            this.buttonSetConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonSetConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSetConfig.Location = new System.Drawing.Point(150, 615);
            this.buttonSetConfig.Margin = new System.Windows.Forms.Padding(150, 6, 150, 6);
            this.buttonSetConfig.Name = "buttonSetConfig";
            this.buttonSetConfig.Size = new System.Drawing.Size(984, 81);
            this.buttonSetConfig.TabIndex = 4;
            this.buttonSetConfig.Text = "Set Config Settings";
            this.buttonSetConfig.UseVisualStyleBackColor = true;
            this.buttonSetConfig.Click += new System.EventHandler(this.buttonSetConfig_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 314);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 20);
            this.label3.TabIndex = 21;
            this.label3.Text = "Location";
            // 
            // ConfigDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 702);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ConfigDialog";
            this.Text = "PinSetDialog";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonSetConfig;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBoxPin1;
        private System.Windows.Forms.ComboBox comboBoxPin2;
        private System.Windows.Forms.ComboBox comboBoxPin3;
        private System.Windows.Forms.ComboBox comboBoxPin4;
        private System.Windows.Forms.ComboBox comboBoxPin5;
        private System.Windows.Forms.ComboBox comboBoxPin6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxPin7;
        private System.Windows.Forms.ComboBox comboBoxPin8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBoxLocation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxOrientation;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
    }
}