﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.WindowsForms;

namespace NU_GUI_Saver
{
    public partial class LivePlot : Form
    {
        private PlotModel _myModel;
        private LineSeries _line;
        private List<LineSeries> _lineList = new List<LineSeries>();
        private int noSeries;
        private PlotView myPlot;

        public LivePlot(int noSeries)
        {
            InitializeComponent();

            this.noSeries = noSeries;

            CreatePlot();
        }

        private void CreatePlot()
        {
            //Create Plotview Object
            myPlot = new PlotView();

            //Create Plotmodel object
            _myModel = new PlotModel {Title = "Simple Plot"};

            //Assign the plot model to the plot view
            myPlot.Model = _myModel;

            //Set up plot for display
            myPlot.Dock = System.Windows.Forms.DockStyle.Bottom;
            myPlot.Location = new System.Drawing.Point(0, 0);
            myPlot.Size = new System.Drawing.Size(500, 500);
            myPlot.TabIndex = 0;

            //Add plot control to form
            Controls.Add(myPlot);

            // Add x and y axes to model
            LinearAxis x = new LinearAxis { Position = AxisPosition.Bottom, AbsoluteMinimum = 0.0, AbsoluteMaximum = 100.0 };
            LinearAxis y = new LinearAxis { Position = AxisPosition.Left, AbsoluteMinimum = 0.0, AbsoluteMaximum = 5.0 };
            y.Zoom(0, 5);
            x.Zoom(0, 100);

            // Create three new LineSeries
            for (int i = 0; i <= noSeries; i++)
            {
                _line = new LineSeries {Title = "LineSeries" + i.ToString(), StrokeThickness = 1};

                for (int j = 0; j <= 99; j++)
                {
                    _line.Points.Add(new DataPoint(i,0));
                }
                _lineList.Add(_line);
                myPlot.Model.Series.Add(_line);
            }
        }

        private void UpdateValues(int[] ADC_data)
        {
            int count = 0;
            foreach (LineSeries line in _lineList)
            {
                line.Points.Add(new DataPoint(99, ADC_data[count] + count));
                count++;
                for (int i = 0; i <= 99; i++)
                {
                    line.Points[i] = new DataPoint(line.Points[i].X - 1, line.Points[i].Y);
                }
                line.Points.RemoveAt(0);
            }
            myPlot.InvalidatePlot(true);
        }

        // ----------------------- EVENT HANDLERS ------------------------- //
        public void OnPacketDecoded(object sender, PacketDecodedEventArgs e)
        {
            // ADC data is always contained in the first index of the packet decoded list
            UpdateValues(e.decoded_packet[1]);
        }
    }
}
